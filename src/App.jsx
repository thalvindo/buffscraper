import React from "react";
import "./App.css";
import BuffProductDetailScraper from "./Services/BuffProductDetailScraper";
import useExportExcel from "./Services/useExportExcel";
import { filteredProduct1 } from "./Constants/FilteredProduct1";
import { filteredProduct2 } from "./Constants/FilteredProduct2";
import { filteredProduct3 } from "./Constants/FilteredProduct3";
import { RadioTypeEnum, SpeedTypeEnum } from "./Types/TypeEnum";

const allProducts = [...filteredProduct1, ...filteredProduct2, ...filteredProduct3];
const akProducts = allProducts.filter((product) => product.productName.includes("AK-47"));
const knifeProducts = allProducts.filter((product) => product.productName.includes("Knife"));
const m4A4Products = allProducts.filter((product) => product.productName.includes("M4A4"));
const m4A1Products = allProducts.filter((product) => product.productName.includes("M4A1-S"));
const awpProducts = allProducts.filter((product) => product.productName.includes("AWP"));
const deProducts = allProducts.filter((product) => product.productName.includes("Desert Eagle"));

const App = () => {
  const [isCurrentlyScraping, setIsCurrentlyScraping] = React.useState(false);
  const [timeout, setTimeout] = React.useState(SpeedTypeEnum.chill);
  const [dataUsed, setDataUsed] = React.useState(allProducts);

  const [latestData, setLatestData] = React.useState();
  const [selectedType, setSelectedType] = React.useState(RadioTypeEnum.all);
  const [selectedSpeed, setSelectedSpeed] = React.useState(SpeedTypeEnum.chill);

  const handleChange = (event) => {
    setSelectedType(event.target.value);

    let value = allProducts;
    if (event.target.value === RadioTypeEnum.ak) {
      value = akProducts;
    }
    if (event.target.value === RadioTypeEnum.awp) {
      value = awpProducts;
    }
    if (event.target.value === RadioTypeEnum.m4a4) {
      value = m4A4Products;
    }
    if (event.target.value === RadioTypeEnum.m4a1) {
      value = m4A1Products;
    }
    if (event.target.value === RadioTypeEnum.de) {
      value = deProducts;
    }
    if (event.target.value === RadioTypeEnum.knife) {
      value = knifeProducts;
    }
    if (event.target.value === RadioTypeEnum.rand) {
      const shuffled = [...allProducts].sort(() => 0.5 - Math.random());
      const selected = shuffled.slice(0, 20);
      value = selected;
    }
    setDataUsed(value);
  };

  const handleChangeSpeed = (event) => {
    let value = SpeedTypeEnum.chill;

    if (parseInt(event.target.value) === SpeedTypeEnum.medium) {
      value = SpeedTypeEnum.medium;
    }
    if (parseInt(event.target.value) === SpeedTypeEnum.fast) {
      value = SpeedTypeEnum.fast;
    }
    setSelectedSpeed(value);
    setTimeout(value);
  };

  const handleOnChangeCurrentlyScrapping = (isTrue) => {
    setIsCurrentlyScraping(isTrue);
  };

  const { data, loopProducts } = BuffProductDetailScraper(dataUsed, handleOnChangeCurrentlyScrapping, timeout);
  const { handleExport } = useExportExcel(latestData);

  React.useEffect(() => {
    setLatestData(data);
  }, [data]);

  return (
    <div style={{ textAlign: "center", padding: "30px", fontFamily: "Arial, sans-serif" }}>
      <h1 style={{ color: "#F0F", fontSize: "2.8rem", marginBottom: "0.5rem" }}>Tha's Buff 163 Auto Scrapper</h1>
      <h2 style={{ fontSize: "2rem", color: "#F0F", marginBottom: "1rem" }}>Money Making Machine</h2>
      <br />
      <h2 style={{ fontSize: "1.8rem", marginBottom: "1rem", color: "#FFF" }}>Select Product Type</h2>
      <div style={{ display: "flex", justifyContent: "center", gap: "10px", flexWrap: "wrap", marginBottom: "20px" }}>
        <label style={{ margin: "5px 10px" }}>
          <input
            type="radio"
            name="type"
            value={RadioTypeEnum.rand}
            checked={selectedType === RadioTypeEnum.rand}
            onChange={handleChange}
            disabled={isCurrentlyScraping}
          />
          Random 20 Data
        </label>
        <label style={{ margin: "5px 10px" }}>
          <input
            type="radio"
            name="type"
            value={RadioTypeEnum.all}
            checked={selectedType === RadioTypeEnum.all}
            onChange={handleChange}
            disabled={isCurrentlyScraping}
          />
          All
        </label>
        <label style={{ margin: "5px 10px" }}>
          <input
            type="radio"
            name="type"
            value={RadioTypeEnum.ak}
            checked={selectedType === RadioTypeEnum.ak}
            onChange={handleChange}
            disabled={isCurrentlyScraping}
          />
          AK
        </label>
        <label style={{ margin: "5px 10px" }}>
          <input
            type="radio"
            name="type"
            value={RadioTypeEnum.knife}
            checked={selectedType === RadioTypeEnum.knife}
            onChange={handleChange}
            disabled={isCurrentlyScraping}
          />
          Knife
        </label>
        <label style={{ margin: "5px 10px" }}>
          <input
            type="radio"
            name="type"
            value={RadioTypeEnum.m4a4}
            checked={selectedType === RadioTypeEnum.m4a4}
            onChange={handleChange}
            disabled={isCurrentlyScraping}
          />
          M4A4
        </label>
        <label style={{ margin: "5px 10px" }}>
          <input
            type="radio"
            name="type"
            value={RadioTypeEnum.m4a1}
            checked={selectedType === RadioTypeEnum.m4a1}
            onChange={handleChange}
            disabled={isCurrentlyScraping}
          />
          M4A1-S
        </label>
        <label style={{ margin: "5px 10px" }}>
          <input
            type="radio"
            name="type"
            value={RadioTypeEnum.awp}
            checked={selectedType === RadioTypeEnum.awp}
            onChange={handleChange}
            disabled={isCurrentlyScraping}
          />
          AWP
        </label>
        <label style={{ margin: "5px 10px" }}>
          <input
            type="radio"
            name="type"
            value={RadioTypeEnum.de}
            checked={selectedType === RadioTypeEnum.de}
            onChange={handleChange}
            disabled={isCurrentlyScraping}
          />
          Dual Eagle
        </label>
      </div>

      <br />

      <div>
        <h2 style={{ fontSize: "1.8rem", marginBottom: "1rem", color: "#FFF" }}>Select Speed</h2>
        <div style={{ display: "flex", justifyContent: "center", gap: "10px", flexWrap: "wrap", marginBottom: "20px" }}>
          <label style={{ margin: "5px 10px", color: "#6DE63E" }}>
            <input
              type="radio"
              name="speed"
              value={SpeedTypeEnum.chill}
              checked={selectedSpeed === SpeedTypeEnum.chill}
              onChange={handleChangeSpeed}
              disabled={isCurrentlyScraping}
            />
            Chill (1000ms)
          </label>
          <label style={{ margin: "5px 10px", color: "#0096FF" }}>
            <input
              type="radio"
              name="speed"
              value={SpeedTypeEnum.medium}
              checked={selectedSpeed === SpeedTypeEnum.medium}
              onChange={handleChangeSpeed}
              disabled={isCurrentlyScraping}
            />
            Medium (600ms)
          </label>
          <label style={{ margin: "5px 10px", color: "#E34234" }}>
            <input
              type="radio"
              name="speed"
              value={SpeedTypeEnum.fast}
              checked={selectedSpeed === SpeedTypeEnum.fast}
              onChange={handleChangeSpeed}
              disabled={isCurrentlyScraping}
            />
            Fast (300ms)
          </label>
        </div>
      </div>

      <br />
      <h3 style={{ fontSize: "1.6rem", marginBottom: "0.8rem", color: "#FFF" }}>
        Total Data: <span style={{ color: "#007BFF" }}>{dataUsed.length}</span>
      </h3>
      <h3 style={{ fontSize: "1.6rem", marginBottom: "2rem", color: "#FFF" }}>
        Collected Data Now: <span style={{ color: "#28A745" }}>{latestData?.length ?? 0}</span>
      </h3>

      <div style={{ display: "flex", justifyContent: "center", gap: "20px", marginBottom: "2rem" }}>
        <button
          disabled={isCurrentlyScraping}
          onClick={() => loopProducts()}
          style={{
            padding: "12px 25px",
            fontSize: "1.2rem",
            backgroundColor: isCurrentlyScraping ? "#6c757d" : "#007BFF",
            color: "#fff",
            border: "none",
            borderRadius: "6px",
            cursor: isCurrentlyScraping ? "not-allowed" : "pointer",
            transition: "background-color 0.3s ease",
          }}
        >
          {isCurrentlyScraping ? "Scraping..." : "Start Getting Data"}
        </button>
        <button
          onClick={() => handleExport()}
          style={{
            padding: "12px 25px",
            fontSize: "1.2rem",
            backgroundColor: "#28A745",
            color: "#fff",
            border: "none",
            borderRadius: "6px",
            cursor: "pointer",
            transition: "background-color 0.3s ease",
          }}
        >
          Export to Excel
        </button>
      </div>

      <button
        onClick={() => window.location.reload()}
        style={{
          padding: "12px 25px",
          fontSize: "1.2rem",
          backgroundColor: "#6C757D",
          color: "#fff",
          border: "none",
          borderRadius: "6px",
          cursor: "pointer",
          transition: "background-color 0.3s ease",
        }}
      >
        Refresh Page
      </button>
    </div>
  );
};

export default App;

import converter from "currency-exchanger-js";

export const steam_currencies = [
  "A$",
  "ARS$",
  "A$",
  "R$",
  "CDN$",
  "CHf",
  "CLP$",
  "¥",
  "COL$",
  "₡",
  "€",
  "£",
  "HK$",
  "₪",
  "Rp",
  "₹",
  "¥",
  "₩",
  "KD",
  "₸",
  "Mex$",
  "RM",
  "kr",
  "NZ$",
  "S/.",
  "P",
  "zł",
  "QR",
  "pуб",
  "SR",
  "S$",
  "฿",
  "TL",
  "NT$",
  "₴",
  "$",
  "$U",
  "₫",
  "R",
  "kr",
];
export const ISO4217_CurrencyCodes = [
  "AUD",
  "ARS",
  "AUD",
  "BRL",
  "CAD",
  "CHF",
  "CLP",
  "CNY",
  "COP",
  "CRC",
  "EUR",
  "GBP",
  "HKD",
  "ILS",
  "IDR",
  "INR",
  "JPY",
  "KRW",
  "KWD",
  "KZT",
  "MXN",
  "MYR",
  "NOK",
  "NZD",
  "PEN",
  "PHP",
  "PLN",
  "QAR",
  "RUB",
  "SAR",
  "SGD",
  "THB",
  "TRY",
  "TWD",
  "UAH",
  "USD",
  "UYU",
  "VND",
  "ZAR",
  "SEK",
];

export const extractCurrencySymbol = (response) => {
  // Use a regular expression to match currency symbols, tabs, and newlines
  const currencySymbolRegex = /[^\d.,\t\n]+/g;

  const matches = response.match(currencySymbolRegex);
  if (matches) {
    return matches[0];
  } else {
    return "";
  }
};

export const extractNumberInCurrency = (response) => {
  const findNumberCommasDotRegex = /[\d.,]+/g;

  const matches = response.match(findNumberCommasDotRegex);

  if (matches) {
    return matches[0];
  } else {
    return 0;
  }
};

function removeSpaces(inputString) {
  // Use the replace method with a regular expression to remove spaces
  return inputString.replace(/\s-\s/g, "");
}

export const symbolToCurrencyName = (symbol) => {
  const index = steam_currencies.indexOf(removeSpaces(symbol));
  if (index !== -1) {
    return ISO4217_CurrencyCodes[index];
  } else {
    return "none";
  }
};

export const formatCurrencyToIdr = async (currency, price) => {
  let convertedCurrency = "";
  const toIdr = await converter.convertOnDate(price, currency, "idr", new Date());
  convertedCurrency = toIdr;

  return convertedCurrency;
};

export const getValueBeforeComma = (value) => {
  const parts = value.split(",");
  const partss = parts[0].split(".");

  let res = "";
  for (let i = 0; i < partss.length; i++) {
    res += partss[i];
  }

  return parseInt(res);
};

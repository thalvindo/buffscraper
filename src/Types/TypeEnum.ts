export enum RadioTypeEnum {
  all = "all",
  ak = "ak",
  m4a4 = "m4a4",
  m4a1 = "m4a1",
  knife = "knife",
  awp = "awp",
  de = "de",
  rand = "random",
}

export enum SpeedTypeEnum {
  chill = 1000,
  medium = 600,
  fast = 400,
}

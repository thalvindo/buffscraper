import { useState } from "react";
import axios from "axios";
import cheerio from "cheerio";

const BuffProductDetailScraper = (products, handleOnChangeCurrentlyScrapping, timeout) => {
  const [data, setData] = useState([]);
  const [processedIds, setProcessedIds] = useState([]);

  const fetchEachData = async (eachProduct) => {
    const response = await axios.get("http://localhost:3001/get", { params: { id: eachProduct.id } });
    const $ = cheerio.load(response.data);

    const productNameHtml = $("div.detail-cont");
    const sellingPriceHtmlTopNav = $("a.i_Btn.i_Btn_trans_bule.active");

    const productNameElement = productNameHtml.find("h1");

    const priceReferenceHtml = $("div.detail-summ");
    const priceReferenceSpanElement = priceReferenceHtml.find("span");

    const tempProductName = productNameElement[0].children[0].data;
    const tempSellingPrice = sellingPriceHtmlTopNav[0]?.children[1]?.attribs["data-price"];
    const tempPriceReference = priceReferenceSpanElement[1].attribs["data-price"];

    //   // not sure why I need this at that time, lol. just gonna put it here then.
    //   // Probably I want to see the steam's price instantly in excel, but got a huge problem in between it,
    //   // and then I'm too lazy to continue

    // const steamUrlElement = priceReferenceHtml.find("a");
    // const steamMarketUrl = steamUrlElement[2].attribs.href;
    // const steamResponse = await axios.get("http://localhost:3001/getSteam", { params: { url: steamMarketUrl } });
    // const $s = cheerio.load(steamResponse.data);

    // const steamActualSellPrice = $s("span.market_listing_price_with_fee");
    // const insideData = steamActualSellPrice[0]?.children[0]?.data ? steamActualSellPrice[0]?.children[0]?.data : null;

    // let convertedCurrency;
    // if (insideData !== null) {
    //   const steamSellingData = {
    //     price: extractNumberInCurrency(insideData ? insideData : null),
    //     currency: symbolToCurrencyName(extractCurrencySymbol(insideData ? insideData : null)),
    //   };

    //   convertedCurrency = await formatCurrencyToIdr(
    //     steamSellingData.currency.toLowerCase(),
    //     getValueBeforeComma(steamSellingData.price)
    //   );
    // }

    const resultEachData = {
      productName: eachProduct.productName ? eachProduct.productName : tempProductName,
      sellingPrice: tempSellingPrice ? tempSellingPrice : -1,
      priceReference: tempPriceReference ? tempPriceReference : -1,
      url: `https://buff.163.com/goods/${eachProduct.id}`,
      // priceFromSteam: convertedCurrency,
    };

    return resultEachData;
  };
  const loopProducts = async () => {
    handleOnChangeCurrentlyScrapping(true);
    for (let i = 0; i < products.length; i++) {
      try {
        await new Promise((resolve) => setTimeout(resolve, timeout));
        const resultEachData = await fetchEachData(products[i]);
        console.log("get Result", resultEachData);

        if (resultEachData && !processedIds.includes(products[i].id)) {
          setData((prevData) => [resultEachData, ...prevData]);
          setProcessedIds((prevIds) => [products[i].id, ...prevIds]);
        }
      } catch (error) {
        console.log(`skipped id ${i}, `, error);
      }
    }

    handleOnChangeCurrentlyScrapping(false);
  };

  return { data, loopProducts };
};

export default BuffProductDetailScraper;

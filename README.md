# React + Vite

## To run :

1. create a local backend to enable cors when hitting api on client side.
2. npm install
3. npm run dev
4. let the magic happen.

## Screenshot on UI :

![](https://drive.google.com/uc?export=view&id=1bFTBPlgf9tBL7RhX1302n02df01rC6ML)

## Note :

- To change the amount of data you want to search, just comment / uncomment things you need in app.jsx, so that dataUsed equals to what you want to search.
- I've made it simple enough so 10 years from now, whenever I read this shit code again, I'll instantly understand and only need to clik on ui.

Express backend :

```
const express = require('express');
const axios = require('axios');
const cors = require('cors');
const app = express();
const PORT = process.env.PORT || 3001;

app.use(cors());

app.get('/get', async (req, res) => {
  const {id} = req.query;
  console.log(id);
  try {
    const response = await axios.get(`https://buff.163.com/goods/${id}`);
    res.json(response.data);
  } catch (error) {
    res.status(500).json({ error: 'error backend' });
  }
});

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
```

## Some Example on the excel :

- https://docs.google.com/spreadsheets/d/1yLFB5y0_FLy_BvSobMCgmP2vabgDPbkaAfR6GrurM1U/edit?usp=sharing
- https://docs.google.com/spreadsheets/d/1WSzPKFPAn45vLABr4BLn7mZ4UNmsVy-c4VwYEN1sIvM/edit?usp=sharing

## Note : If ever there's a problem when using this, it's probably because :

- some version of the currency converter is deprecated
  - examples are currency-exchanger-js, from version 1.0.2 to 1.0.4 changes the api because their dependency is deprecated.
- Buff changes the url
- cheerio is deprecated
- I'm just dumb for not using typescript.

### useful links :

- https://buff.163.com/goods/${id}

### Special Thanks :

- https://github.com/ModestSerhat/cs2-marketplace-ids

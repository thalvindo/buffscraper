import ExcelJS from "exceljs";
import { saveAs } from "file-saver";
import { formatDate } from "../Utils/DateFormatter";

const useExportExcel = (products) => {
  let profitData = [];

  const workbook = new ExcelJS.Workbook();
  const totalProductLength = products?.length;

  const handleExportFirstSheet = () => {
    const worksheet = workbook.addWorksheet();
    worksheet.name = "Buff Products";
    worksheet.columns = [
      { key: "A", width: 5 },
      { key: "B", width: 35 },
      { key: "C", width: 15 },
      { key: "D", width: 15 },
      { key: "E", width: 10 },
      { key: "F", width: 20 },
      { key: "G", width: 20 },
      { key: "H", width: 20 },
      { key: "I", width: 20 },
      { key: "J", width: 20 },
    ];
    worksheet.getRow(1).height = 30;

    const rowHeaderPosition = 1;
    const rowHeader = worksheet.getRow(rowHeaderPosition);

    rowHeader.getCell(1).value = "NO";
    rowHeader.getCell(2).value = "Item Name";
    rowHeader.getCell(3).value = "Item Price";
    rowHeader.getCell(4).value = "Reference Price";
    rowHeader.getCell(5).value = "Float";
    rowHeader.getCell(6).value = "Price (idr)";
    rowHeader.getCell(7).value = "Clean Steam Wallet";
    rowHeader.getCell(8).value = "Gross";
    rowHeader.getCell(9).value = "Profit";
    rowHeader.getCell(10).value = "Buff Links";

    for (let i = 1; i <= 10; i++) {
      rowHeader.getCell(i).font = {
        size: 12,
        bold: true,
      };
      rowHeader.getCell(i).alignment = {
        horizontal: "center",
      };
    }

    for (let i = 1; i <= 10; i++) {
      for (let j = 1; j <= totalProductLength + 1; j++) {
        const cell = worksheet.getCell(j, i);
        cell.border = {
          top: { style: "thin" },
          left: { style: "thin" },
          bottom: { style: "thin" },
          right: { style: "thin" },
        };
      }
    }

    for (let i = 0; i < totalProductLength; i++) {
      const row = worksheet.getRow(rowHeaderPosition + 1 + i);
      row.alignment = {
        wrapText: true,
        horizontal: "center",
        vertical: "middle",
      };

      const float = (parseFloat(products[i].sellingPrice) / parseFloat(products[i].priceReference)).toFixed(2);
      const modal = parseFloat(products[i].sellingPrice).toFixed(2) * 2200;
      const cleanSteamPrice = parseFloat(products[i].priceReference).toFixed(2) * 2200 * 0.86;
      const gross = cleanSteamPrice * 0.9;
      const profit = parseInt(gross) - parseInt(modal);

      if (
        parseInt(profit) > 300000 &&
        parseFloat(float) > 0.62 &&
        parseFloat(float) < 0.67 &&
        parseFloat(products[i].sellingPrice).toFixed(2) > 800 &&
        parseFloat(products[i].sellingPrice).toFixed(2) < 3500 &&
        modal < 3500000 &&
        modal > 1800000
      ) {
        const payload = {
          no: i + 1,
          name: products[i].productName,
          sellingPrice: products[i].sellingPrice,
          priceReference: products[i].priceReference,
          float: float,
          modal: modal,
          cleanSteamPrice: cleanSteamPrice,
          gross: gross,
          profit: profit,
          url: products[i].url,
        };
        profitData.push(payload);
      }

      row.getCell(1).value = i + 1;
      row.getCell(2).value = products[i].productName;
      row.getCell(3).value = products[i].sellingPrice;
      row.getCell(4).value = products[i].priceReference;
      row.getCell(5).value = float;
      row.getCell(6).value = modal;
      row.getCell(7).value = cleanSteamPrice;
      row.getCell(8).value = gross;
      row.getCell(9).value = profit;
      row.getCell(10).value = products[i].url;
    }
  };

  const handleExportSecondSheet = () => {
    const secondSheet = workbook.addWorksheet();
    secondSheet.name = "Profit Products";
    secondSheet.columns = [
      { key: "A", width: 5 },
      { key: "B", width: 35 },
      { key: "C", width: 15 },
      { key: "D", width: 15 },
      { key: "E", width: 10 },
      { key: "F", width: 20 },
      { key: "G", width: 20 },
      { key: "H", width: 20 },
      { key: "I", width: 20 },
      { key: "J", width: 20 },
    ];
    secondSheet.getRow(1).height = 30;

    const rowHeaderPosition = 1;
    const rowHeader = secondSheet.getRow(rowHeaderPosition);

    rowHeader.getCell(1).value = "NO";
    rowHeader.getCell(2).value = "Item Name";
    rowHeader.getCell(3).value = "Item Price";
    rowHeader.getCell(4).value = "Reference Price";
    rowHeader.getCell(5).value = "Float";
    rowHeader.getCell(6).value = "Price (idr)";
    rowHeader.getCell(7).value = "Clean Steam Wallet";
    rowHeader.getCell(8).value = "Gross";
    rowHeader.getCell(9).value = "Profit";
    rowHeader.getCell(10).value = "Buff Links";

    for (let i = 1; i <= 10; i++) {
      rowHeader.getCell(i).font = {
        size: 12,
        bold: true,
      };
      rowHeader.getCell(i).alignment = {
        horizontal: "center",
      };
    }

    for (let i = 1; i <= 10; i++) {
      for (let j = 1; j <= profitData?.length + 1; j++) {
        const cell = secondSheet.getCell(j, i);
        cell.border = {
          top: { style: "thin" },
          left: { style: "thin" },
          bottom: { style: "thin" },
          right: { style: "thin" },
        };
      }
    }
    for (let i = 0; i < profitData?.length; i++) {
      const row = secondSheet.getRow(rowHeaderPosition + 1 + i);
      row.alignment = {
        wrapText: true,
        horizontal: "center",
        vertical: "middle",
      };

      row.getCell(1).value = profitData[i].no;
      row.getCell(2).value = profitData[i].name;
      row.getCell(3).value = profitData[i].sellingPrice;
      row.getCell(4).value = profitData[i].priceReference;
      row.getCell(5).value = profitData[i].float;
      row.getCell(6).value = profitData[i].modal;
      row.getCell(7).value = profitData[i].cleanSteamPrice;
      row.getCell(8).value = profitData[i].gross;
      row.getCell(9).value = profitData[i].profit;
      row.getCell(10).value = profitData[i].url;
    }
  };
  const handleExport = async () => {
    console.log("fff clicked");
    handleExportFirstSheet();
    handleExportSecondSheet();

    const buf = await workbook.xlsx.writeBuffer();
    saveAs(new Blob([buf]), `Tha's Buff Scapper || ${formatDate(new Date())}.xlsx`);
  };

  return { handleExport };
};

export default useExportExcel;

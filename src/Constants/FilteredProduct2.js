export const filteredProduct2 = [
  {
  "productName": "Souvenir AK-47 | Safari Mesh (Factory New)",
  "id": 37514
  },
  {
  "productName": "Souvenir AK-47 | Safari Mesh (Field-Tested)",
  "id": 37515
  },
  {
  "productName": "Souvenir AK-47 | Safari Mesh (Minimal Wear)",
  "id": 37516
  },
  {
  "productName": "Souvenir AUG | Colony (Factory New)",
  "id": 37519
  },
  {
  "productName": "Souvenir AUG | Colony (Field-Tested)",
  "id": 37520
  },
  {
  "productName": "Souvenir AUG | Colony (Minimal Wear)",
  "id": 37521
  },
  {
  "productName": "Souvenir AUG | Condemned (Field-Tested)",
  "id": 37524
  },
  {
  "productName": "Souvenir AUG | Condemned (Minimal Wear)",
  "id": 37525
  },
  {
  "productName": "Souvenir AUG | Contractor (Field-Tested)",
  "id": 37528
  },
  {
  "productName": "Souvenir AUG | Contractor (Minimal Wear)",
  "id": 37529
  },
  {
  "productName": "Souvenir AUG | Radiation Hazard (Factory New)",
  "id": 37532
  },
  {
  "productName": "Souvenir AUG | Radiation Hazard (Field-Tested)",
  "id": 37533
  },
  {
  "productName": "Souvenir AUG | Radiation Hazard (Minimal Wear)",
  "id": 37534
  },
  {
  "productName": "Souvenir AUG | Storm (Factory New)",
  "id": 37537
  },
  {
  "productName": "Souvenir AUG | Storm (Field-Tested)",
  "id": 37538
  },
  {
  "productName": "Souvenir AUG | Storm (Minimal Wear)",
  "id": 37539
  },
  {
  "productName": "Souvenir AWP | Pink DDPAT (Field-Tested)",
  "id": 37542
  },
  {
  "productName": "Souvenir AWP | Pink DDPAT (Minimal Wear)",
  "id": 37543
  },
  {
  "productName": "Souvenir AWP | Pit Viper (Field-Tested)",
  "id": 37545
  },
  {
  "productName": "Souvenir AWP | Pit Viper (Minimal Wear)",
  "id": 37546
  },
  {
  "productName": "Souvenir AWP | Safari Mesh (Field-Tested)",
  "id": 37548
  },
  {
  "productName": "Souvenir AWP | Safari Mesh (Minimal Wear)",
  "id": 37549
  },
  {
  "productName": "Souvenir CZ75-Auto | Chalice (Factory New)",
  "id": 37551
  },
  {
  "productName": "Souvenir CZ75-Auto | Chalice (Minimal Wear)",
  "id": 37552
  },
  {
  "productName": "Souvenir CZ75-Auto | Nitro (Field-Tested)",
  "id": 37554
  },
  {
  "productName": "Souvenir CZ75-Auto | Nitro (Minimal Wear)",
  "id": 37555
  },
  {
  "productName": "Souvenir Desert Eagle | Hand Cannon (Factory New)",
  "id": 37558
  },
  {
  "productName": "Souvenir Desert Eagle | Hand Cannon (Field-Tested)",
  "id": 37559
  },
  {
  "productName": "Souvenir Desert Eagle | Hand Cannon (Minimal Wear)",
  "id": 37560
  },
  {
  "productName": "Souvenir Desert Eagle | Mudder (Field-Tested)",
  "id": 37563
  },
  {
  "productName": "Souvenir Desert Eagle | Mudder (Minimal Wear)",
  "id": 37564
  },
  {
  "productName": "Souvenir Desert Eagle | Urban DDPAT (Factory New)",
  "id": 37567
  },
  {
  "productName": "Souvenir Desert Eagle | Urban DDPAT (Field-Tested)",
  "id": 37568
  },
  {
  "productName": "Souvenir Desert Eagle | Urban DDPAT (Minimal Wear)",
  "id": 37569
  },
  {
  "productName": "Souvenir Desert Eagle | Urban Rubble (Factory New)",
  "id": 37572
  },
  {
  "productName": "Souvenir Desert Eagle | Urban Rubble (Field-Tested)",
  "id": 37573
  },
  {
  "productName": "Souvenir Desert Eagle | Urban Rubble (Minimal Wear)",
  "id": 37574
  },
  {
  "productName": "Souvenir Dual Berettas | Anodized Navy (Factory New)",
  "id": 37576
  },
  {
  "productName": "Souvenir Dual Berettas | Anodized Navy (Minimal Wear)",
  "id": 37577
  },
  {
  "productName": "Souvenir Dual Berettas | Briar (Factory New)",
  "id": 37578
  },
  {
  "productName": "Souvenir Dual Berettas | Briar (Field-Tested)",
  "id": 37579
  },
  {
  "productName": "Souvenir Dual Berettas | Briar (Minimal Wear)",
  "id": 37580
  },
  {
  "productName": "Souvenir Dual Berettas | Cobalt Quartz (Factory New)",
  "id": 37581
  },
  {
  "productName": "Souvenir Dual Berettas | Cobalt Quartz (Field-Tested)",
  "id": 37582
  },
  {
  "productName": "Souvenir Dual Berettas | Cobalt Quartz (Minimal Wear)",
  "id": 37583
  },
  {
  "productName": "Souvenir Dual Berettas | Colony (Factory New)",
  "id": 37585
  },
  {
  "productName": "Souvenir Dual Berettas | Colony (Field-Tested)",
  "id": 37586
  },
  {
  "productName": "Souvenir Dual Berettas | Colony (Minimal Wear)",
  "id": 37587
  },
  {
  "productName": "Souvenir Dual Berettas | Contractor (Factory New)",
  "id": 37590
  },
  {
  "productName": "Souvenir Dual Berettas | Contractor (Field-Tested)",
  "id": 37591
  },
  {
  "productName": "Souvenir Dual Berettas | Contractor (Minimal Wear)",
  "id": 37592
  },
  {
  "productName": "Souvenir Dual Berettas | Stained (Field-Tested)",
  "id": 37595
  },
  {
  "productName": "Souvenir Dual Berettas | Stained (Minimal Wear)",
  "id": 37596
  },
  {
  "productName": "Souvenir FAMAS | Colony (Factory New)",
  "id": 37599
  },
  {
  "productName": "Souvenir FAMAS | Colony (Field-Tested)",
  "id": 37600
  },
  {
  "productName": "Souvenir FAMAS | Colony (Minimal Wear)",
  "id": 37601
  },
  {
  "productName": "Souvenir FAMAS | Cyanospatter (Field-Tested)",
  "id": 37604
  },
  {
  "productName": "Souvenir FAMAS | Cyanospatter (Minimal Wear)",
  "id": 37605
  },
  {
  "productName": "Souvenir FAMAS | Styx (Factory New)",
  "id": 37608
  },
  {
  "productName": "Souvenir FAMAS | Styx (Field-Tested)",
  "id": 37609
  },
  {
  "productName": "Souvenir FAMAS | Styx (Minimal Wear)",
  "id": 37610
  },
  {
  "productName": "Souvenir FAMAS | Teardown (Factory New)",
  "id": 37612
  },
  {
  "productName": "Souvenir FAMAS | Teardown (Field-Tested)",
  "id": 37613
  },
  {
  "productName": "Souvenir FAMAS | Teardown (Minimal Wear)",
  "id": 37614
  },
  {
  "productName": "Souvenir Five-SeveN | Contractor (Factory New)",
  "id": 37616
  },
  {
  "productName": "Souvenir Five-SeveN | Contractor (Field-Tested)",
  "id": 37617
  },
  {
  "productName": "Souvenir Five-SeveN | Contractor (Minimal Wear)",
  "id": 37618
  },
  {
  "productName": "Souvenir Five-SeveN | Forest Night (Factory New)",
  "id": 37621
  },
  {
  "productName": "Souvenir Five-SeveN | Forest Night (Field-Tested)",
  "id": 37622
  },
  {
  "productName": "Souvenir Five-SeveN | Forest Night (Minimal Wear)",
  "id": 37623
  },
  {
  "productName": "Souvenir Five-SeveN | Hot Shot (Factory New)",
  "id": 37626
  },
  {
  "productName": "Souvenir Five-SeveN | Hot Shot (Field-Tested)",
  "id": 37627
  },
  {
  "productName": "Souvenir Five-SeveN | Hot Shot (Minimal Wear)",
  "id": 37628
  },
  {
  "productName": "Souvenir Five-SeveN | Orange Peel (Factory New)",
  "id": 37631
  },
  {
  "productName": "Souvenir Five-SeveN | Orange Peel (Field-Tested)",
  "id": 37632
  },
  {
  "productName": "Souvenir Five-SeveN | Orange Peel (Minimal Wear)",
  "id": 37633
  },
  {
  "productName": "Souvenir Five-SeveN | Silver Quartz (Factory New)",
  "id": 37635
  },
  {
  "productName": "Souvenir Five-SeveN | Silver Quartz (Field-Tested)",
  "id": 37636
  },
  {
  "productName": "Souvenir Five-SeveN | Silver Quartz (Minimal Wear)",
  "id": 37637
  },
  {
  "productName": "Souvenir G3SG1 | Desert Storm (Factory New)",
  "id": 37639
  },
  {
  "productName": "Souvenir G3SG1 | Desert Storm (Field-Tested)",
  "id": 37640
  },
  {
  "productName": "Souvenir G3SG1 | Desert Storm (Minimal Wear)",
  "id": 37641
  },
  {
  "productName": "Souvenir G3SG1 | Jungle Dashed (Factory New)",
  "id": 37644
  },
  {
  "productName": "Souvenir G3SG1 | Jungle Dashed (Field-Tested)",
  "id": 37645
  },
  {
  "productName": "Souvenir G3SG1 | Jungle Dashed (Minimal Wear)",
  "id": 37646
  },
  {
  "productName": "Souvenir G3SG1 | Polar Camo (Factory New)",
  "id": 37649
  },
  {
  "productName": "Souvenir G3SG1 | Polar Camo (Field-Tested)",
  "id": 37650
  },
  {
  "productName": "Souvenir G3SG1 | Polar Camo (Minimal Wear)",
  "id": 37651
  },
  {
  "productName": "Souvenir G3SG1 | Safari Mesh (Factory New)",
  "id": 37654
  },
  {
  "productName": "Souvenir G3SG1 | Safari Mesh (Field-Tested)",
  "id": 37655
  },
  {
  "productName": "Souvenir G3SG1 | Safari Mesh (Minimal Wear)",
  "id": 37656
  },
  {
  "productName": "Souvenir G3SG1 | VariCamo (Factory New)",
  "id": 37659
  },
  {
  "productName": "Souvenir G3SG1 | VariCamo (Field-Tested)",
  "id": 37660
  },
  {
  "productName": "Souvenir G3SG1 | VariCamo (Minimal Wear)",
  "id": 37661
  },
  {
  "productName": "Souvenir Galil AR | Cerberus (Factory New)",
  "id": 37664
  },
  {
  "productName": "Souvenir Galil AR | Cerberus (Field-Tested)",
  "id": 37665
  },
  {
  "productName": "Souvenir Galil AR | Cerberus (Minimal Wear)",
  "id": 37666
  },
  {
  "productName": "Souvenir Galil AR | Hunting Blind (Factory New)",
  "id": 37669
  },
  {
  "productName": "Souvenir Galil AR | Hunting Blind (Field-Tested)",
  "id": 37670
  },
  {
  "productName": "Souvenir Galil AR | Hunting Blind (Minimal Wear)",
  "id": 37671
  },
  {
  "productName": "Souvenir Galil AR | Sage Spray (Factory New)",
  "id": 37674
  },
  {
  "productName": "Souvenir Galil AR | Sage Spray (Field-Tested)",
  "id": 37675
  },
  {
  "productName": "Souvenir Galil AR | Sage Spray (Minimal Wear)",
  "id": 37676
  },
  {
  "productName": "Souvenir Galil AR | VariCamo (Factory New)",
  "id": 37679
  },
  {
  "productName": "Souvenir Galil AR | VariCamo (Field-Tested)",
  "id": 37680
  },
  {
  "productName": "Souvenir Galil AR | VariCamo (Minimal Wear)",
  "id": 37681
  },
  {
  "productName": "Souvenir Glock-18 | Candy Apple (Factory New)",
  "id": 37683
  },
  {
  "productName": "Souvenir Glock-18 | Candy Apple (Field-Tested)",
  "id": 37684
  },
  {
  "productName": "Souvenir Glock-18 | Candy Apple (Minimal Wear)",
  "id": 37685
  },
  {
  "productName": "Souvenir Glock-18 | Groundwater (Factory New)",
  "id": 37687
  },
  {
  "productName": "Souvenir Glock-18 | Groundwater (Field-Tested)",
  "id": 37688
  },
  {
  "productName": "Souvenir Glock-18 | Groundwater (Minimal Wear)",
  "id": 37689
  },
  {
  "productName": "Souvenir Glock-18 | Night (Factory New)",
  "id": 37692
  },
  {
  "productName": "Souvenir Glock-18 | Night (Field-Tested)",
  "id": 37693
  },
  {
  "productName": "Souvenir Glock-18 | Night (Minimal Wear)",
  "id": 37694
  },
  {
  "productName": "Souvenir Glock-18 | Reactor (Factory New)",
  "id": 37697
  },
  {
  "productName": "Souvenir Glock-18 | Reactor (Field-Tested)",
  "id": 37698
  },
  {
  "productName": "Souvenir Glock-18 | Reactor (Minimal Wear)",
  "id": 37699
  },
  {
  "productName": "Souvenir M249 | Contrast Spray (Factory New)",
  "id": 37702
  },
  {
  "productName": "Souvenir M249 | Contrast Spray (Field-Tested)",
  "id": 37703
  },
  {
  "productName": "Souvenir M249 | Contrast Spray (Minimal Wear)",
  "id": 37704
  },
  {
  "productName": "Souvenir M249 | Gator Mesh (Factory New)",
  "id": 37707
  },
  {
  "productName": "Souvenir M249 | Gator Mesh (Field-Tested)",
  "id": 37708
  },
  {
  "productName": "Souvenir M249 | Gator Mesh (Minimal Wear)",
  "id": 37709
  },
  {
  "productName": "Souvenir M4A1-S | Boreal Forest (Field-Tested)",
  "id": 37711
  },
  {
  "productName": "Souvenir M4A1-S | Boreal Forest (Minimal Wear)",
  "id": 37712
  },
  {
  "productName": "Souvenir M4A1-S | Knight (Factory New)",
  "id": 37714
  },
  {
  "productName": "Souvenir M4A1-S | Knight (Minimal Wear)",
  "id": 37715
  },
  {
  "productName": "Souvenir M4A1-S | Master Piece (Field-Tested)",
  "id": 37717
  },
  {
  "productName": "Souvenir M4A1-S | Master Piece (Minimal Wear)",
  "id": 37718
  },
  {
  "productName": "Souvenir M4A1-S | Nitro (Field-Tested)",
  "id": 37721
  },
  {
  "productName": "Souvenir M4A1-S | Nitro (Minimal Wear)",
  "id": 37722
  },
  {
  "productName": "Souvenir M4A1-S | VariCamo (Factory New)",
  "id": 37724
  },
  {
  "productName": "Souvenir M4A1-S | VariCamo (Field-Tested)",
  "id": 37725
  },
  {
  "productName": "Souvenir M4A1-S | VariCamo (Minimal Wear)",
  "id": 37726
  },
  {
  "productName": "Souvenir M4A4 | Radiation Hazard (Factory New)",
  "id": 37729
  },
  {
  "productName": "Souvenir M4A4 | Radiation Hazard (Field-Tested)",
  "id": 37730
  },
  {
  "productName": "Souvenir M4A4 | Radiation Hazard (Minimal Wear)",
  "id": 37731
  },
  {
  "productName": "Souvenir M4A4 | Tornado (Factory New)",
  "id": 37734
  },
  {
  "productName": "Souvenir M4A4 | Tornado (Field-Tested)",
  "id": 37735
  },
  {
  "productName": "Souvenir M4A4 | Tornado (Minimal Wear)",
  "id": 37736
  },
  {
  "productName": "Souvenir M4A4 | Urban DDPAT (Factory New)",
  "id": 37739
  },
  {
  "productName": "Souvenir M4A4 | Urban DDPAT (Field-Tested)",
  "id": 37740
  },
  {
  "productName": "Souvenir M4A4 | Urban DDPAT (Minimal Wear)",
  "id": 37741
  },
  {
  "productName": "Souvenir MAC-10 | Amber Fade (Factory New)",
  "id": 37743
  },
  {
  "productName": "Souvenir MAC-10 | Amber Fade (Field-Tested)",
  "id": 37744
  },
  {
  "productName": "Souvenir MAC-10 | Amber Fade (Minimal Wear)",
  "id": 37745
  },
  {
  "productName": "Souvenir MAC-10 | Candy Apple (Factory New)",
  "id": 37747
  },
  {
  "productName": "Souvenir MAC-10 | Candy Apple (Field-Tested)",
  "id": 37748
  },
  {
  "productName": "Souvenir MAC-10 | Candy Apple (Minimal Wear)",
  "id": 37749
  },
  {
  "productName": "Souvenir MAC-10 | Indigo (Factory New)",
  "id": 37751
  },
  {
  "productName": "Souvenir MAC-10 | Indigo (Field-Tested)",
  "id": 37752
  },
  {
  "productName": "Souvenir MAC-10 | Indigo (Minimal Wear)",
  "id": 37753
  },
  {
  "productName": "Souvenir MAC-10 | Nuclear Garden (Factory New)",
  "id": 37756
  },
  {
  "productName": "Souvenir MAC-10 | Nuclear Garden (Field-Tested)",
  "id": 37757
  },
  {
  "productName": "Souvenir MAC-10 | Nuclear Garden (Minimal Wear)",
  "id": 37758
  },
  {
  "productName": "Souvenir MAC-10 | Palm (Factory New)",
  "id": 37761
  },
  {
  "productName": "Souvenir MAC-10 | Palm (Field-Tested)",
  "id": 37762
  },
  {
  "productName": "Souvenir MAC-10 | Palm (Minimal Wear)",
  "id": 37763
  },
  {
  "productName": "Souvenir MAG-7 | Bulldozer (Field-Tested)",
  "id": 37766
  },
  {
  "productName": "Souvenir MAG-7 | Bulldozer (Minimal Wear)",
  "id": 37767
  },
  {
  "productName": "Souvenir MAG-7 | Irradiated Alert (Factory New)",
  "id": 37770
  },
  {
  "productName": "Souvenir MAG-7 | Irradiated Alert (Field-Tested)",
  "id": 37771
  },
  {
  "productName": "Souvenir MAG-7 | Irradiated Alert (Minimal Wear)",
  "id": 37772
  },
  {
  "productName": "Souvenir MAG-7 | Metallic DDPAT (Factory New)",
  "id": 37774
  },
  {
  "productName": "Souvenir MAG-7 | Metallic DDPAT (Minimal Wear)",
  "id": 37775
  },
  {
  "productName": "Souvenir MAG-7 | Sand Dune (Factory New)",
  "id": 37777
  },
  {
  "productName": "Souvenir MAG-7 | Sand Dune (Field-Tested)",
  "id": 37778
  },
  {
  "productName": "Souvenir MAG-7 | Sand Dune (Minimal Wear)",
  "id": 37779
  },
  {
  "productName": "Souvenir MAG-7 | Silver (Factory New)",
  "id": 37781
  },
  {
  "productName": "Souvenir MAG-7 | Silver (Minimal Wear)",
  "id": 37782
  },
  {
  "productName": "Souvenir MAG-7 | Storm (Factory New)",
  "id": 37784
  },
  {
  "productName": "Souvenir MAG-7 | Storm (Field-Tested)",
  "id": 37785
  },
  {
  "productName": "Souvenir MAG-7 | Storm (Minimal Wear)",
  "id": 37786
  },
  {
  "productName": "Souvenir MP7 | Anodized Navy (Factory New)",
  "id": 37788
  },
  {
  "productName": "Souvenir MP7 | Army Recon (Field-Tested)",
  "id": 37790
  },
  {
  "productName": "Souvenir MP7 | Army Recon (Minimal Wear)",
  "id": 37791
  },
  {
  "productName": "Souvenir MP7 | Gunsmoke (Factory New)",
  "id": 37794
  },
  {
  "productName": "Souvenir MP7 | Gunsmoke (Field-Tested)",
  "id": 37795
  },
  {
  "productName": "Souvenir MP7 | Gunsmoke (Minimal Wear)",
  "id": 37796
  },
  {
  "productName": "Souvenir MP7 | Orange Peel (Factory New)",
  "id": 37799
  },
  {
  "productName": "Souvenir MP7 | Orange Peel (Field-Tested)",
  "id": 37800
  },
  {
  "productName": "Souvenir MP7 | Orange Peel (Minimal Wear)",
  "id": 37801
  },
  {
  "productName": "Souvenir MP9 | Dark Age (Factory New)",
  "id": 37803
  },
  {
  "productName": "Souvenir MP9 | Dark Age (Field-Tested)",
  "id": 37804
  },
  {
  "productName": "Souvenir MP9 | Dark Age (Minimal Wear)",
  "id": 37805
  },
  {
  "productName": "Souvenir MP9 | Hot Rod (Factory New)",
  "id": 37806
  },
  {
  "productName": "Souvenir MP9 | Hot Rod (Minimal Wear)",
  "id": 37807
  },
  {
  "productName": "Souvenir MP9 | Orange Peel (Field-Tested)",
  "id": 37809
  },
  {
  "productName": "Souvenir MP9 | Orange Peel (Minimal Wear)",
  "id": 37810
  },
  {
  "productName": "Souvenir MP9 | Sand Dashed (Factory New)",
  "id": 37813
  },
  {
  "productName": "Souvenir MP9 | Sand Dashed (Field-Tested)",
  "id": 37814
  },
  {
  "productName": "Souvenir MP9 | Sand Dashed (Minimal Wear)",
  "id": 37815
  },
  {
  "productName": "Souvenir MP9 | Setting Sun (Factory New)",
  "id": 37818
  },
  {
  "productName": "Souvenir MP9 | Setting Sun (Field-Tested)",
  "id": 37819
  },
  {
  "productName": "Souvenir MP9 | Setting Sun (Minimal Wear)",
  "id": 37820
  },
  {
  "productName": "Souvenir MP9 | Storm (Factory New)",
  "id": 37823
  },
  {
  "productName": "Souvenir MP9 | Storm (Field-Tested)",
  "id": 37824
  },
  {
  "productName": "Souvenir MP9 | Storm (Minimal Wear)",
  "id": 37825
  },
  {
  "productName": "Souvenir Negev | CaliCamo (Factory New)",
  "id": 37828
  },
  {
  "productName": "Souvenir Negev | CaliCamo (Field-Tested)",
  "id": 37829
  },
  {
  "productName": "Souvenir Negev | CaliCamo (Minimal Wear)",
  "id": 37830
  },
  {
  "productName": "Souvenir Negev | Nuclear Waste (Factory New)",
  "id": 37832
  },
  {
  "productName": "Souvenir Negev | Nuclear Waste (Field-Tested)",
  "id": 37833
  },
  {
  "productName": "Souvenir Negev | Nuclear Waste (Minimal Wear)",
  "id": 37834
  },
  {
  "productName": "Souvenir Nova | Candy Apple (Factory New)",
  "id": 37836
  },
  {
  "productName": "Souvenir Nova | Candy Apple (Field-Tested)",
  "id": 37837
  },
  {
  "productName": "Souvenir Nova | Candy Apple (Minimal Wear)",
  "id": 37838
  },
  {
  "productName": "Souvenir Nova | Green Apple (Factory New)",
  "id": 37839
  },
  {
  "productName": "Souvenir Nova | Green Apple (Field-Tested)",
  "id": 37840
  },
  {
  "productName": "Souvenir Nova | Green Apple (Minimal Wear)",
  "id": 37841
  },
  {
  "productName": "Souvenir Nova | Polar Mesh (Factory New)",
  "id": 37843
  },
  {
  "productName": "Souvenir Nova | Polar Mesh (Field-Tested)",
  "id": 37844
  },
  {
  "productName": "Souvenir Nova | Polar Mesh (Minimal Wear)",
  "id": 37845
  },
  {
  "productName": "Souvenir Nova | Predator (Factory New)",
  "id": 37848
  },
  {
  "productName": "Souvenir Nova | Predator (Field-Tested)",
  "id": 37849
  },
  {
  "productName": "Souvenir Nova | Predator (Minimal Wear)",
  "id": 37850
  },
  {
  "productName": "Souvenir Nova | Sand Dune (Factory New)",
  "id": 37853
  },
  {
  "productName": "Souvenir Nova | Sand Dune (Field-Tested)",
  "id": 37854
  },
  {
  "productName": "Souvenir Nova | Sand Dune (Minimal Wear)",
  "id": 37855
  },
  {
  "productName": "Souvenir Nova | Walnut (Factory New)",
  "id": 37858
  },
  {
  "productName": "Souvenir Nova | Walnut (Field-Tested)",
  "id": 37859
  },
  {
  "productName": "Souvenir Nova | Walnut (Minimal Wear)",
  "id": 37860
  },
  {
  "productName": "Souvenir P2000 | Amber Fade (Factory New)",
  "id": 37862
  },
  {
  "productName": "Souvenir P2000 | Amber Fade (Field-Tested)",
  "id": 37863
  },
  {
  "productName": "Souvenir P2000 | Amber Fade (Minimal Wear)",
  "id": 37864
  },
  {
  "productName": "Souvenir P2000 | Chainmail (Factory New)",
  "id": 37866
  },
  {
  "productName": "Souvenir P2000 | Chainmail (Field-Tested)",
  "id": 37867
  },
  {
  "productName": "Souvenir P2000 | Chainmail (Minimal Wear)",
  "id": 37868
  },
  {
  "productName": "Souvenir P2000 | Granite Marbleized (Field-Tested)",
  "id": 37870
  },
  {
  "productName": "Souvenir P2000 | Granite Marbleized (Minimal Wear)",
  "id": 37871
  },
  {
  "productName": "Souvenir P2000 | Grassland (Factory New)",
  "id": 37874
  },
  {
  "productName": "Souvenir P2000 | Grassland (Field-Tested)",
  "id": 37875
  },
  {
  "productName": "Souvenir P2000 | Grassland (Minimal Wear)",
  "id": 37876
  },
  {
  "productName": "Souvenir P250 | Bone Mask (Factory New)",
  "id": 37879
  },
  {
  "productName": "Souvenir P250 | Bone Mask (Field-Tested)",
  "id": 37880
  },
  {
  "productName": "Souvenir P250 | Bone Mask (Minimal Wear)",
  "id": 37881
  },
  {
  "productName": "Souvenir P250 | Boreal Forest (Field-Tested)",
  "id": 37884
  },
  {
  "productName": "Souvenir P250 | Boreal Forest (Minimal Wear)",
  "id": 37885
  },
  {
  "productName": "Souvenir P250 | Contamination (Factory New)",
  "id": 37888
  },
  {
  "productName": "Souvenir P250 | Contamination (Field-Tested)",
  "id": 37889
  },
  {
  "productName": "Souvenir P250 | Contamination (Minimal Wear)",
  "id": 37890
  },
  {
  "productName": "Souvenir P250 | Gunsmoke (Factory New)",
  "id": 37893
  },
  {
  "productName": "Souvenir P250 | Gunsmoke (Field-Tested)",
  "id": 37894
  },
  {
  "productName": "Souvenir P250 | Gunsmoke (Minimal Wear)",
  "id": 37895
  },
  {
  "productName": "Souvenir P250 | Metallic DDPAT (Factory New)",
  "id": 37897
  },
  {
  "productName": "Souvenir P250 | Metallic DDPAT (Minimal Wear)",
  "id": 37898
  },
  {
  "productName": "Souvenir P250 | Nuclear Threat (Field-Tested)",
  "id": 37900
  },
  {
  "productName": "Souvenir P250 | Nuclear Threat (Minimal Wear)",
  "id": 37901
  },
  {
  "productName": "Souvenir P250 | Sand Dune (Factory New)",
  "id": 37904
  },
  {
  "productName": "Souvenir P250 | Sand Dune (Field-Tested)",
  "id": 37905
  },
  {
  "productName": "Souvenir P250 | Sand Dune (Minimal Wear)",
  "id": 37906
  },
  {
  "productName": "Souvenir P90 | Ash Wood (Factory New)",
  "id": 37908
  },
  {
  "productName": "Souvenir P90 | Ash Wood (Field-Tested)",
  "id": 37909
  },
  {
  "productName": "Souvenir P90 | Ash Wood (Minimal Wear)",
  "id": 37910
  },
  {
  "productName": "Souvenir P90 | Fallout Warning (Factory New)",
  "id": 37913
  },
  {
  "productName": "Souvenir P90 | Fallout Warning (Field-Tested)",
  "id": 37914
  },
  {
  "productName": "Souvenir P90 | Fallout Warning (Minimal Wear)",
  "id": 37915
  },
  {
  "productName": "Souvenir P90 | Sand Spray (Factory New)",
  "id": 37918
  },
  {
  "productName": "Souvenir P90 | Sand Spray (Field-Tested)",
  "id": 37919
  },
  {
  "productName": "Souvenir P90 | Sand Spray (Minimal Wear)",
  "id": 37920
  },
  {
  "productName": "Souvenir P90 | Scorched (Factory New)",
  "id": 37923
  },
  {
  "productName": "Souvenir P90 | Scorched (Field-Tested)",
  "id": 37924
  },
  {
  "productName": "Souvenir P90 | Scorched (Minimal Wear)",
  "id": 37925
  },
  {
  "productName": "Souvenir P90 | Storm (Factory New)",
  "id": 37928
  },
  {
  "productName": "Souvenir P90 | Storm (Field-Tested)",
  "id": 37929
  },
  {
  "productName": "Souvenir P90 | Storm (Minimal Wear)",
  "id": 37930
  },
  {
  "productName": "Souvenir P90 | Teardown (Factory New)",
  "id": 37933
  },
  {
  "productName": "Souvenir P90 | Teardown (Field-Tested)",
  "id": 37934
  },
  {
  "productName": "Souvenir P90 | Teardown (Minimal Wear)",
  "id": 37935
  },
  {
  "productName": "Souvenir PP-Bizon | Brass (Factory New)",
  "id": 37938
  },
  {
  "productName": "Souvenir PP-Bizon | Brass (Field-Tested)",
  "id": 37939
  },
  {
  "productName": "Souvenir PP-Bizon | Brass (Minimal Wear)",
  "id": 37940
  },
  {
  "productName": "Souvenir PP-Bizon | Chemical Green (Factory New)",
  "id": 37943
  },
  {
  "productName": "Souvenir PP-Bizon | Chemical Green (Field-Tested)",
  "id": 37944
  },
  {
  "productName": "Souvenir PP-Bizon | Chemical Green (Minimal Wear)",
  "id": 37945
  },
  {
  "productName": "Souvenir PP-Bizon | Irradiated Alert (Field-Tested)",
  "id": 37948
  },
  {
  "productName": "Souvenir PP-Bizon | Irradiated Alert (Minimal Wear)",
  "id": 37949
  },
  {
  "productName": "Souvenir PP-Bizon | Night Ops (Factory New)",
  "id": 37952
  },
  {
  "productName": "Souvenir PP-Bizon | Night Ops (Field-Tested)",
  "id": 37953
  },
  {
  "productName": "Souvenir PP-Bizon | Night Ops (Minimal Wear)",
  "id": 37954
  },
  {
  "productName": "Souvenir PP-Bizon | Sand Dashed (Factory New)",
  "id": 37957
  },
  {
  "productName": "Souvenir PP-Bizon | Sand Dashed (Field-Tested)",
  "id": 37958
  },
  {
  "productName": "Souvenir PP-Bizon | Sand Dashed (Minimal Wear)",
  "id": 37959
  },
  {
  "productName": "Souvenir PP-Bizon | Urban Dashed (Factory New)",
  "id": 37962
  },
  {
  "productName": "Souvenir PP-Bizon | Urban Dashed (Field-Tested)",
  "id": 37963
  },
  {
  "productName": "Souvenir PP-Bizon | Urban Dashed (Minimal Wear)",
  "id": 37964
  },
  {
  "productName": "Souvenir R8 Revolver | Amber Fade (Field-Tested)",
  "id": 37966
  },
  {
  "productName": "Souvenir R8 Revolver | Amber Fade (Minimal Wear)",
  "id": 37967
  },
  {
  "productName": "Souvenir Sawed-Off | Amber Fade (Factory New)",
  "id": 38019
  },
  {
  "productName": "Souvenir Sawed-Off | Amber Fade (Field-Tested)",
  "id": 38020
  },
  {
  "productName": "Souvenir Sawed-Off | Amber Fade (Minimal Wear)",
  "id": 38021
  },
  {
  "productName": "Souvenir Sawed-Off | Full Stop (Field-Tested)",
  "id": 38024
  },
  {
  "productName": "Souvenir Sawed-Off | Full Stop (Minimal Wear)",
  "id": 38025
  },
  {
  "productName": "Souvenir Sawed-Off | Irradiated Alert (Factory New)",
  "id": 38027
  },
  {
  "productName": "Souvenir Sawed-Off | Irradiated Alert (Field-Tested)",
  "id": 38028
  },
  {
  "productName": "Souvenir Sawed-Off | Irradiated Alert (Minimal Wear)",
  "id": 38029
  },
  {
  "productName": "Souvenir Sawed-Off | Rust Coat (Factory New)",
  "id": 38032
  },
  {
  "productName": "Souvenir Sawed-Off | Rust Coat (Field-Tested)",
  "id": 38033
  },
  {
  "productName": "Souvenir Sawed-Off | Rust Coat (Minimal Wear)",
  "id": 38034
  },
  {
  "productName": "Souvenir Sawed-Off | Sage Spray (Factory New)",
  "id": 38037
  },
  {
  "productName": "Souvenir Sawed-Off | Sage Spray (Field-Tested)",
  "id": 38038
  },
  {
  "productName": "Souvenir Sawed-Off | Sage Spray (Minimal Wear)",
  "id": 38039
  },
  {
  "productName": "Souvenir Sawed-Off | Snake Camo (Factory New)",
  "id": 38042
  },
  {
  "productName": "Souvenir Sawed-Off | Snake Camo (Field-Tested)",
  "id": 38043
  },
  {
  "productName": "Souvenir Sawed-Off | Snake Camo (Minimal Wear)",
  "id": 38044
  },
  {
  "productName": "Souvenir SCAR-20 | Carbon Fiber (Factory New)",
  "id": 37968
  },
  {
  "productName": "Souvenir SCAR-20 | Carbon Fiber (Minimal Wear)",
  "id": 37969
  },
  {
  "productName": "Souvenir SCAR-20 | Contractor (Factory New)",
  "id": 37971
  },
  {
  "productName": "Souvenir SCAR-20 | Contractor (Field-Tested)",
  "id": 37972
  },
  {
  "productName": "Souvenir SCAR-20 | Contractor (Minimal Wear)",
  "id": 37973
  },
  {
  "productName": "Souvenir SCAR-20 | Sand Mesh (Factory New)",
  "id": 37976
  },
  {
  "productName": "Souvenir SCAR-20 | Sand Mesh (Field-Tested)",
  "id": 37977
  },
  {
  "productName": "Souvenir SCAR-20 | Sand Mesh (Minimal Wear)",
  "id": 37978
  },
  {
  "productName": "Souvenir SCAR-20 | Storm (Factory New)",
  "id": 37981
  },
  {
  "productName": "Souvenir SCAR-20 | Storm (Field-Tested)",
  "id": 37982
  },
  {
  "productName": "Souvenir SCAR-20 | Storm (Minimal Wear)",
  "id": 37983
  },
  {
  "productName": "Souvenir SG 553 | Anodized Navy (Factory New)",
  "id": 37985
  },
  {
  "productName": "Souvenir SG 553 | Anodized Navy (Minimal Wear)",
  "id": 37986
  },
  {
  "productName": "Souvenir SG 553 | Damascus Steel (Factory New)",
  "id": 37988
  },
  {
  "productName": "Souvenir SG 553 | Damascus Steel (Field-Tested)",
  "id": 37989
  },
  {
  "productName": "Souvenir SG 553 | Damascus Steel (Minimal Wear)",
  "id": 37990
  },
  {
  "productName": "Souvenir SG 553 | Fallout Warning (Factory New)",
  "id": 37993
  },
  {
  "productName": "Souvenir SG 553 | Fallout Warning (Field-Tested)",
  "id": 37994
  },
  {
  "productName": "Souvenir SG 553 | Fallout Warning (Minimal Wear)",
  "id": 37995
  },
  {
  "productName": "Souvenir SG 553 | Gator Mesh (Factory New)",
  "id": 37998
  },
  {
  "productName": "Souvenir SG 553 | Gator Mesh (Field-Tested)",
  "id": 37999
  },
  {
  "productName": "Souvenir SG 553 | Gator Mesh (Minimal Wear)",
  "id": 38000
  },
  {
  "productName": "Souvenir SG 553 | Waves Perforated (Field-Tested)",
  "id": 38002
  },
  {
  "productName": "Souvenir SG 553 | Waves Perforated (Minimal Wear)",
  "id": 38003
  },
  {
  "productName": "Souvenir SSG 08 | Acid Fade (Factory New)",
  "id": 38005
  },
  {
  "productName": "Souvenir SSG 08 | Blue Spruce (Field-Tested)",
  "id": 38007
  },
  {
  "productName": "Souvenir SSG 08 | Blue Spruce (Minimal Wear)",
  "id": 38008
  },
  {
  "productName": "Souvenir SSG 08 | Detour (Factory New)",
  "id": 38010
  },
  {
  "productName": "Souvenir SSG 08 | Detour (Field-Tested)",
  "id": 38011
  },
  {
  "productName": "Souvenir SSG 08 | Detour (Minimal Wear)",
  "id": 38012
  },
  {
  "productName": "Souvenir SSG 08 | Tropical Storm (Factory New)",
  "id": 38015
  },
  {
  "productName": "Souvenir SSG 08 | Tropical Storm (Field-Tested)",
  "id": 38016
  },
  {
  "productName": "Souvenir SSG 08 | Tropical Storm (Minimal Wear)",
  "id": 38017
  },
  {
  "productName": "Souvenir Tec-9 | Army Mesh (Factory New)",
  "id": 38047
  },
  {
  "productName": "Souvenir Tec-9 | Army Mesh (Field-Tested)",
  "id": 38048
  },
  {
  "productName": "Souvenir Tec-9 | Army Mesh (Minimal Wear)",
  "id": 38049
  },
  {
  "productName": "Souvenir Tec-9 | Brass (Factory New)",
  "id": 38052
  },
  {
  "productName": "Souvenir Tec-9 | Brass (Field-Tested)",
  "id": 38053
  },
  {
  "productName": "Souvenir Tec-9 | Brass (Minimal Wear)",
  "id": 38054
  },
  {
  "productName": "Souvenir Tec-9 | Groundwater (Field-Tested)",
  "id": 38057
  },
  {
  "productName": "Souvenir Tec-9 | Groundwater (Minimal Wear)",
  "id": 38058
  },
  {
  "productName": "Souvenir Tec-9 | Nuclear Threat (Field-Tested)",
  "id": 38061
  },
  {
  "productName": "Souvenir Tec-9 | Nuclear Threat (Minimal Wear)",
  "id": 38062
  },
  {
  "productName": "Souvenir Tec-9 | Red Quartz (Factory New)",
  "id": 38064
  },
  {
  "productName": "Souvenir Tec-9 | Red Quartz (Field-Tested)",
  "id": 38065
  },
  {
  "productName": "Souvenir Tec-9 | Red Quartz (Minimal Wear)",
  "id": 38066
  },
  {
  "productName": "Souvenir Tec-9 | Toxic (Factory New)",
  "id": 38069
  },
  {
  "productName": "Souvenir Tec-9 | Toxic (Field-Tested)",
  "id": 38070
  },
  {
  "productName": "Souvenir Tec-9 | Toxic (Minimal Wear)",
  "id": 38071
  },
  {
  "productName": "Souvenir Tec-9 | VariCamo (Factory New)",
  "id": 38074
  },
  {
  "productName": "Souvenir Tec-9 | VariCamo (Field-Tested)",
  "id": 38075
  },
  {
  "productName": "Souvenir Tec-9 | VariCamo (Minimal Wear)",
  "id": 38076
  },
  {
  "productName": "Souvenir UMP-45 | Blaze (Factory New)",
  "id": 38078
  },
  {
  "productName": "Souvenir UMP-45 | Blaze (Minimal Wear)",
  "id": 38079
  },
  {
  "productName": "Souvenir UMP-45 | Fallout Warning (Factory New)",
  "id": 38081
  },
  {
  "productName": "Souvenir UMP-45 | Fallout Warning (Field-Tested)",
  "id": 38082
  },
  {
  "productName": "Souvenir UMP-45 | Fallout Warning (Minimal Wear)",
  "id": 38083
  },
  {
  "productName": "Souvenir UMP-45 | Gunsmoke (Field-Tested)",
  "id": 38086
  },
  {
  "productName": "Souvenir UMP-45 | Gunsmoke (Minimal Wear)",
  "id": 38087
  },
  {
  "productName": "Souvenir UMP-45 | Indigo (Factory New)",
  "id": 38090
  },
  {
  "productName": "Souvenir UMP-45 | Indigo (Field-Tested)",
  "id": 38091
  },
  {
  "productName": "Souvenir UMP-45 | Indigo (Minimal Wear)",
  "id": 38092
  },
  {
  "productName": "Souvenir UMP-45 | Scorched (Factory New)",
  "id": 38095
  },
  {
  "productName": "Souvenir UMP-45 | Scorched (Field-Tested)",
  "id": 38096
  },
  {
  "productName": "Souvenir UMP-45 | Scorched (Minimal Wear)",
  "id": 38097
  },
  {
  "productName": "Souvenir UMP-45 | Urban DDPAT (Factory New)",
  "id": 38100
  },
  {
  "productName": "Souvenir UMP-45 | Urban DDPAT (Field-Tested)",
  "id": 38101
  },
  {
  "productName": "Souvenir UMP-45 | Urban DDPAT (Minimal Wear)",
  "id": 38102
  },
  {
  "productName": "Souvenir USP-S | Forest Leaves (Field-Tested)",
  "id": 38104
  },
  {
  "productName": "Souvenir USP-S | Forest Leaves (Minimal Wear)",
  "id": 38105
  },
  {
  "productName": "Souvenir USP-S | Night Ops (Factory New)",
  "id": 38107
  },
  {
  "productName": "Souvenir USP-S | Night Ops (Field-Tested)",
  "id": 38108
  },
  {
  "productName": "Souvenir USP-S | Night Ops (Minimal Wear)",
  "id": 38109
  },
  {
  "productName": "Souvenir USP-S | Road Rash (Factory New)",
  "id": 38111
  },
  {
  "productName": "Souvenir USP-S | Road Rash (Field-Tested)",
  "id": 38112
  },
  {
  "productName": "Souvenir USP-S | Road Rash (Minimal Wear)",
  "id": 38113
  },
  {
  "productName": "Souvenir USP-S | Royal Blue (Factory New)",
  "id": 38116
  },
  {
  "productName": "Souvenir USP-S | Royal Blue (Field-Tested)",
  "id": 38117
  },
  {
  "productName": "Souvenir USP-S | Royal Blue (Minimal Wear)",
  "id": 38118
  },
  {
  "productName": "Souvenir XM1014 | Blue Spruce (Field-Tested)",
  "id": 38121
  },
  {
  "productName": "Souvenir XM1014 | Blue Spruce (Minimal Wear)",
  "id": 38122
  },
  {
  "productName": "Souvenir XM1014 | Blue Steel (Field-Tested)",
  "id": 38125
  },
  {
  "productName": "Souvenir XM1014 | Blue Steel (Minimal Wear)",
  "id": 38126
  },
  {
  "productName": "Souvenir XM1014 | Bone Machine (Factory New)",
  "id": 38129
  },
  {
  "productName": "Souvenir XM1014 | Bone Machine (Field-Tested)",
  "id": 38130
  },
  {
  "productName": "Souvenir XM1014 | Bone Machine (Minimal Wear)",
  "id": 38131
  },
  {
  "productName": "Souvenir XM1014 | CaliCamo (Factory New)",
  "id": 38134
  },
  {
  "productName": "Souvenir XM1014 | CaliCamo (Field-Tested)",
  "id": 38135
  },
  {
  "productName": "Souvenir XM1014 | CaliCamo (Minimal Wear)",
  "id": 38136
  },
  {
  "productName": "Souvenir XM1014 | Fallout Warning (Factory New)",
  "id": 38139
  },
  {
  "productName": "Souvenir XM1014 | Fallout Warning (Field-Tested)",
  "id": 38140
  },
  {
  "productName": "Souvenir XM1014 | Fallout Warning (Minimal Wear)",
  "id": 38141
  },
  {
  "productName": "Souvenir XM1014 | VariCamo Blue (Factory New)",
  "id": 38144
  },
  {
  "productName": "Souvenir XM1014 | VariCamo Blue (Field-Tested)",
  "id": 38145
  },
  {
  "productName": "Souvenir XM1014 | VariCamo Blue (Minimal Wear)",
  "id": 38146
  },
  {
  "productName": "StatTrak™ AK-47 | Aquamarine Revenge (Factory New)",
  "id": 38153
  },
  {
  "productName": "StatTrak™ AK-47 | Aquamarine Revenge (Field-Tested)",
  "id": 38154
  },
  {
  "productName": "StatTrak™ AK-47 | Aquamarine Revenge (Minimal Wear)",
  "id": 38155
  },
  {
  "productName": "StatTrak™ AK-47 | Bloodsport (Factory New)",
  "id": 38157
  },
  {
  "productName": "StatTrak™ AK-47 | Bloodsport (Field-Tested)",
  "id": 38158
  },
  {
  "productName": "StatTrak™ AK-47 | Bloodsport (Minimal Wear)",
  "id": 38159
  },
  {
  "productName": "StatTrak™ AK-47 | Blue Laminate (Factory New)",
  "id": 38161
  },
  {
  "productName": "StatTrak™ AK-47 | Blue Laminate (Field-Tested)",
  "id": 38162
  },
  {
  "productName": "StatTrak™ AK-47 | Blue Laminate (Minimal Wear)",
  "id": 38163
  },
  {
  "productName": "StatTrak™ AK-47 | Cartel (Factory New)",
  "id": 38166
  },
  {
  "productName": "StatTrak™ AK-47 | Cartel (Field-Tested)",
  "id": 38167
  },
  {
  "productName": "StatTrak™ AK-47 | Cartel (Minimal Wear)",
  "id": 38168
  },
  {
  "productName": "StatTrak™ AK-47 | Case Hardened (Factory New)",
  "id": 38171
  },
  {
  "productName": "StatTrak™ AK-47 | Case Hardened (Field-Tested)",
  "id": 38172
  },
  {
  "productName": "StatTrak™ AK-47 | Case Hardened (Minimal Wear)",
  "id": 38173
  },
  {
  "productName": "StatTrak™ AK-47 | Elite Build (Factory New)",
  "id": 38176
  },
  {
  "productName": "StatTrak™ AK-47 | Elite Build (Field-Tested)",
  "id": 38177
  },
  {
  "productName": "StatTrak™ AK-47 | Elite Build (Minimal Wear)",
  "id": 38178
  },
  {
  "productName": "StatTrak™ AK-47 | Fire Serpent (Field-Tested)",
  "id": 38181
  },
  {
  "productName": "StatTrak™ AK-47 | Fire Serpent (Minimal Wear)",
  "id": 38182
  },
  {
  "productName": "StatTrak™ AK-47 | Frontside Misty (Factory New)",
  "id": 38185
  },
  {
  "productName": "StatTrak™ AK-47 | Frontside Misty (Field-Tested)",
  "id": 38186
  },
  {
  "productName": "StatTrak™ AK-47 | Frontside Misty (Minimal Wear)",
  "id": 38187
  },
  {
  "productName": "StatTrak™ AK-47 | Fuel Injector (Factory New)",
  "id": 38190
  },
  {
  "productName": "StatTrak™ AK-47 | Fuel Injector (Field-Tested)",
  "id": 38191
  },
  {
  "productName": "StatTrak™ AK-47 | Fuel Injector (Minimal Wear)",
  "id": 38192
  },
  {
  "productName": "StatTrak™ AK-47 | Jaguar (Factory New)",
  "id": 38195
  },
  {
  "productName": "StatTrak™ AK-47 | Jaguar (Field-Tested)",
  "id": 38196
  },
  {
  "productName": "StatTrak™ AK-47 | Jaguar (Minimal Wear)",
  "id": 38197
  },
  {
  "productName": "StatTrak™ AK-47 | Neon Revolution (Factory New)",
  "id": 38200
  },
  {
  "productName": "StatTrak™ AK-47 | Neon Revolution (Field-Tested)",
  "id": 38201
  },
  {
  "productName": "StatTrak™ AK-47 | Neon Revolution (Minimal Wear)",
  "id": 38202
  },
  {
  "productName": "StatTrak™ AK-47 | Orbit Mk01 (Factory New)",
  "id": 38205
  },
  {
  "productName": "StatTrak™ AK-47 | Orbit Mk01 (Field-Tested)",
  "id": 38206
  },
  {
  "productName": "StatTrak™ AK-47 | Orbit Mk01 (Minimal Wear)",
  "id": 38207
  },
  {
  "productName": "StatTrak™ AK-47 | Point Disarray (Factory New)",
  "id": 38210
  },
  {
  "productName": "StatTrak™ AK-47 | Point Disarray (Field-Tested)",
  "id": 38211
  },
  {
  "productName": "StatTrak™ AK-47 | Point Disarray (Minimal Wear)",
  "id": 38212
  },
  {
  "productName": "StatTrak™ AK-47 | Red Laminate (Factory New)",
  "id": 38215
  },
  {
  "productName": "StatTrak™ AK-47 | Red Laminate (Field-Tested)",
  "id": 38216
  },
  {
  "productName": "StatTrak™ AK-47 | Red Laminate (Minimal Wear)",
  "id": 38217
  },
  {
  "productName": "StatTrak™ AK-47 | Redline (Field-Tested)",
  "id": 38220
  },
  {
  "productName": "StatTrak™ AK-47 | Redline (Minimal Wear)",
  "id": 38221
  },
  {
  "productName": "StatTrak™ AK-47 | The Empress (Factory New)",
  "id": 38224
  },
  {
  "productName": "StatTrak™ AK-47 | The Empress (Field-Tested)",
  "id": 38225
  },
  {
  "productName": "StatTrak™ AK-47 | The Empress (Minimal Wear)",
  "id": 38226
  },
  {
  "productName": "StatTrak™ AK-47 | Vulcan (Factory New)",
  "id": 38229
  },
  {
  "productName": "StatTrak™ AK-47 | Vulcan (Field-Tested)",
  "id": 38230
  },
  {
  "productName": "StatTrak™ AK-47 | Vulcan (Minimal Wear)",
  "id": 38231
  },
  {
  "productName": "StatTrak™ AK-47 | Wasteland Rebel (Factory New)",
  "id": 38234
  },
  {
  "productName": "StatTrak™ AK-47 | Wasteland Rebel (Field-Tested)",
  "id": 38235
  },
  {
  "productName": "StatTrak™ AK-47 | Wasteland Rebel (Minimal Wear)",
  "id": 38236
  },
  {
  "productName": "StatTrak™ AUG | Aristocrat (Factory New)",
  "id": 38239
  },
  {
  "productName": "StatTrak™ AUG | Aristocrat (Field-Tested)",
  "id": 38240
  },
  {
  "productName": "StatTrak™ AUG | Aristocrat (Minimal Wear)",
  "id": 38241
  },
  {
  "productName": "StatTrak™ AUG | Bengal Tiger (Factory New)",
  "id": 38244
  },
  {
  "productName": "StatTrak™ AUG | Bengal Tiger (Field-Tested)",
  "id": 38245
  },
  {
  "productName": "StatTrak™ AUG | Bengal Tiger (Minimal Wear)",
  "id": 38246
  },
  {
  "productName": "StatTrak™ AUG | Chameleon (Factory New)",
  "id": 38249
  },
  {
  "productName": "StatTrak™ AUG | Chameleon (Field-Tested)",
  "id": 38250
  },
  {
  "productName": "StatTrak™ AUG | Chameleon (Minimal Wear)",
  "id": 38251
  },
  {
  "productName": "StatTrak™ AUG | Fleet Flock (Factory New)",
  "id": 38254
  },
  {
  "productName": "StatTrak™ AUG | Fleet Flock (Field-Tested)",
  "id": 38255
  },
  {
  "productName": "StatTrak™ AUG | Fleet Flock (Minimal Wear)",
  "id": 38256
  },
  {
  "productName": "StatTrak™ AUG | Ricochet (Factory New)",
  "id": 38259
  },
  {
  "productName": "StatTrak™ AUG | Ricochet (Field-Tested)",
  "id": 38260
  },
  {
  "productName": "StatTrak™ AUG | Ricochet (Minimal Wear)",
  "id": 38261
  },
  {
  "productName": "StatTrak™ AUG | Syd Mead (Factory New)",
  "id": 38264
  },
  {
  "productName": "StatTrak™ AUG | Syd Mead (Field-Tested)",
  "id": 38265
  },
  {
  "productName": "StatTrak™ AUG | Syd Mead (Minimal Wear)",
  "id": 38266
  },
  {
  "productName": "StatTrak™ AUG | Torque (Factory New)",
  "id": 38269
  },
  {
  "productName": "StatTrak™ AUG | Torque (Field-Tested)",
  "id": 38270
  },
  {
  "productName": "StatTrak™ AUG | Torque (Minimal Wear)",
  "id": 38271
  },
  {
  "productName": "StatTrak™ AUG | Triqua (Factory New)",
  "id": 38274
  },
  {
  "productName": "StatTrak™ AUG | Triqua (Field-Tested)",
  "id": 38275
  },
  {
  "productName": "StatTrak™ AUG | Triqua (Minimal Wear)",
  "id": 38276
  },
  {
  "productName": "StatTrak™ AUG | Wings (Factory New)",
  "id": 38278
  },
  {
  "productName": "StatTrak™ AUG | Wings (Minimal Wear)",
  "id": 38279
  },
  {
  "productName": "StatTrak™ AWP | Asiimov (Field-Tested)",
  "id": 38281
  },
  {
  "productName": "StatTrak™ AWP | BOOM (Factory New)",
  "id": 38283
  },
  {
  "productName": "StatTrak™ AWP | BOOM (Field-Tested)",
  "id": 38284
  },
  {
  "productName": "StatTrak™ AWP | BOOM (Minimal Wear)",
  "id": 38285
  },
  {
  "productName": "StatTrak™ AWP | Corticera (Factory New)",
  "id": 38286
  },
  {
  "productName": "StatTrak™ AWP | Corticera (Field-Tested)",
  "id": 38287
  },
  {
  "productName": "StatTrak™ AWP | Corticera (Minimal Wear)",
  "id": 38288
  },
  {
  "productName": "StatTrak™ AWP | Electric Hive (Factory New)",
  "id": 38289
  },
  {
  "productName": "StatTrak™ AWP | Electric Hive (Field-Tested)",
  "id": 38290
  },
  {
  "productName": "StatTrak™ AWP | Electric Hive (Minimal Wear)",
  "id": 38291
  },
  {
  "productName": "StatTrak™ AWP | Elite Build (Factory New)",
  "id": 38294
  },
  {
  "productName": "StatTrak™ AWP | Elite Build (Field-Tested)",
  "id": 38295
  },
  {
  "productName": "StatTrak™ AWP | Elite Build (Minimal Wear)",
  "id": 38296
  },
  {
  "productName": "StatTrak™ AWP | Fever Dream (Factory New)",
  "id": 38299
  },
  {
  "productName": "StatTrak™ AWP | Fever Dream (Field-Tested)",
  "id": 38300
  },
  {
  "productName": "StatTrak™ AWP | Fever Dream (Minimal Wear)",
  "id": 38301
  },
  {
  "productName": "StatTrak™ AWP | Graphite (Factory New)",
  "id": 38303
  },
  {
  "productName": "StatTrak™ AWP | Graphite (Minimal Wear)",
  "id": 38304
  },
  {
  "productName": "StatTrak™ AWP | Hyper Beast (Factory New)",
  "id": 38306
  },
  {
  "productName": "StatTrak™ AWP | Hyper Beast (Field-Tested)",
  "id": 38307
  },
  {
  "productName": "StatTrak™ AWP | Hyper Beast (Minimal Wear)",
  "id": 38308
  },
  {
  "productName": "StatTrak™ AWP | Lightning Strike (Factory New)",
  "id": 38310
  },
  {
  "productName": "StatTrak™ AWP | Lightning Strike (Minimal Wear)",
  "id": 38311
  },
  {
  "productName": "StatTrak™ AWP | Man-o'-war (Minimal Wear)",
  "id": 38312
  },
  {
  "productName": "StatTrak™ AWP | Oni Taiji (Factory New)",
  "id": 38313
  },
  {
  "productName": "StatTrak™ AWP | Oni Taiji (Field-Tested)",
  "id": 38314
  },
  {
  "productName": "StatTrak™ AWP | Oni Taiji (Minimal Wear)",
  "id": 38315
  },
  {
  "productName": "StatTrak™ AWP | Phobos (Factory New)",
  "id": 38316
  },
  {
  "productName": "StatTrak™ AWP | Phobos (Field-Tested)",
  "id": 38317
  },
  {
  "productName": "StatTrak™ AWP | Phobos (Minimal Wear)",
  "id": 38318
  },
  {
  "productName": "StatTrak™ AWP | Redline (Field-Tested)",
  "id": 38320
  },
  {
  "productName": "StatTrak™ AWP | Redline (Minimal Wear)",
  "id": 38321
  },
  {
  "productName": "StatTrak™ AWP | Worm God (Factory New)",
  "id": 38323
  },
  {
  "productName": "StatTrak™ AWP | Worm God (Field-Tested)",
  "id": 38324
  },
  {
  "productName": "StatTrak™ AWP | Worm God (Minimal Wear)",
  "id": 38325
  },
  {
  "productName": "StatTrak™ CZ75-Auto | Crimson Web (Field-Tested)",
  "id": 38328
  },
  {
  "productName": "StatTrak™ CZ75-Auto | Crimson Web (Minimal Wear)",
  "id": 38329
  },
  {
  "productName": "StatTrak™ CZ75-Auto | Hexane (Factory New)",
  "id": 38331
  },
  {
  "productName": "StatTrak™ CZ75-Auto | Hexane (Field-Tested)",
  "id": 38332
  },
  {
  "productName": "StatTrak™ CZ75-Auto | Hexane (Minimal Wear)",
  "id": 38333
  },
  {
  "productName": "StatTrak™ CZ75-Auto | Imprint (Factory New)",
  "id": 38336
  },
  {
  "productName": "StatTrak™ CZ75-Auto | Imprint (Field-Tested)",
  "id": 38337
  },
  {
  "productName": "StatTrak™ CZ75-Auto | Imprint (Minimal Wear)",
  "id": 38338
  },
  {
  "productName": "StatTrak™ CZ75-Auto | Poison Dart (Factory New)",
  "id": 38341
  },
  {
  "productName": "StatTrak™ CZ75-Auto | Poison Dart (Field-Tested)",
  "id": 38342
  },
  {
  "productName": "StatTrak™ CZ75-Auto | Poison Dart (Minimal Wear)",
  "id": 38343
  },
  {
  "productName": "StatTrak™ CZ75-Auto | Pole Position (Factory New)",
  "id": 38346
  },
  {
  "productName": "StatTrak™ CZ75-Auto | Pole Position (Field-Tested)",
  "id": 38347
  },
  {
  "productName": "StatTrak™ CZ75-Auto | Pole Position (Minimal Wear)",
  "id": 38348
  },
  {
  "productName": "StatTrak™ CZ75-Auto | Polymer (Factory New)",
  "id": 38351
  },
  {
  "productName": "StatTrak™ CZ75-Auto | Polymer (Field-Tested)",
  "id": 38352
  },
  {
  "productName": "StatTrak™ CZ75-Auto | Polymer (Minimal Wear)",
  "id": 38353
  },
  {
  "productName": "StatTrak™ CZ75-Auto | Red Astor (Factory New)",
  "id": 38356
  },
  {
  "productName": "StatTrak™ CZ75-Auto | Red Astor (Field-Tested)",
  "id": 38357
  },
  {
  "productName": "StatTrak™ CZ75-Auto | Red Astor (Minimal Wear)",
  "id": 38358
  },
  {
  "productName": "StatTrak™ CZ75-Auto | Tacticat (Factory New)",
  "id": 38361
  },
  {
  "productName": "StatTrak™ CZ75-Auto | Tacticat (Field-Tested)",
  "id": 38362
  },
  {
  "productName": "StatTrak™ CZ75-Auto | Tacticat (Minimal Wear)",
  "id": 38363
  },
  {
  "productName": "StatTrak™ CZ75-Auto | The Fuschia Is Now (Factory New)",
  "id": 38365
  },
  {
  "productName": "StatTrak™ CZ75-Auto | The Fuschia Is Now (Field-Tested)",
  "id": 38366
  },
  {
  "productName": "StatTrak™ CZ75-Auto | The Fuschia Is Now (Minimal Wear)",
  "id": 38367
  },
  {
  "productName": "StatTrak™ CZ75-Auto | Tigris (Factory New)",
  "id": 38370
  },
  {
  "productName": "StatTrak™ CZ75-Auto | Tigris (Field-Tested)",
  "id": 38371
  },
  {
  "productName": "StatTrak™ CZ75-Auto | Tigris (Minimal Wear)",
  "id": 38372
  },
  {
  "productName": "StatTrak™ CZ75-Auto | Tread Plate (Factory New)",
  "id": 38374
  },
  {
  "productName": "StatTrak™ CZ75-Auto | Tread Plate (Field-Tested)",
  "id": 38375
  },
  {
  "productName": "StatTrak™ CZ75-Auto | Tread Plate (Minimal Wear)",
  "id": 38376
  },
  {
  "productName": "StatTrak™ CZ75-Auto | Twist (Factory New)",
  "id": 38378
  },
  {
  "productName": "StatTrak™ CZ75-Auto | Twist (Field-Tested)",
  "id": 38379
  },
  {
  "productName": "StatTrak™ CZ75-Auto | Twist (Minimal Wear)",
  "id": 38380
  },
  {
  "productName": "StatTrak™ CZ75-Auto | Victoria (Factory New)",
  "id": 38383
  },
  {
  "productName": "StatTrak™ CZ75-Auto | Victoria (Field-Tested)",
  "id": 38384
  },
  {
  "productName": "StatTrak™ CZ75-Auto | Victoria (Minimal Wear)",
  "id": 38385
  },
  {
  "productName": "StatTrak™ CZ75-Auto | Xiangliu (Factory New)",
  "id": 38388
  },
  {
  "productName": "StatTrak™ CZ75-Auto | Xiangliu (Field-Tested)",
  "id": 38389
  },
  {
  "productName": "StatTrak™ CZ75-Auto | Xiangliu (Minimal Wear)",
  "id": 38390
  },
  {
  "productName": "StatTrak™ CZ75-Auto | Yellow Jacket (Factory New)",
  "id": 38393
  },
  {
  "productName": "StatTrak™ CZ75-Auto | Yellow Jacket (Field-Tested)",
  "id": 38394
  },
  {
  "productName": "StatTrak™ CZ75-Auto | Yellow Jacket (Minimal Wear)",
  "id": 38395
  },
  {
  "productName": "StatTrak™ Desert Eagle | Bronze Deco (Factory New)",
  "id": 38398
  },
  {
  "productName": "StatTrak™ Desert Eagle | Bronze Deco (Field-Tested)",
  "id": 38399
  },
  {
  "productName": "StatTrak™ Desert Eagle | Bronze Deco (Minimal Wear)",
  "id": 38400
  },
  {
  "productName": "StatTrak™ Desert Eagle | Cobalt Disruption (Factory New)",
  "id": 38402
  },
  {
  "productName": "StatTrak™ Desert Eagle | Cobalt Disruption (Field-Tested)",
  "id": 38403
  },
  {
  "productName": "StatTrak™ Desert Eagle | Cobalt Disruption (Minimal Wear)",
  "id": 38404
  },
  {
  "productName": "StatTrak™ Desert Eagle | Conspiracy (Factory New)",
  "id": 38405
  },
  {
  "productName": "StatTrak™ Desert Eagle | Conspiracy (Field-Tested)",
  "id": 38406
  },
  {
  "productName": "StatTrak™ Desert Eagle | Conspiracy (Minimal Wear)",
  "id": 38407
  },
  {
  "productName": "StatTrak™ Desert Eagle | Corinthian (Factory New)",
  "id": 38408
  },
  {
  "productName": "StatTrak™ Desert Eagle | Corinthian (Field-Tested)",
  "id": 38409
  },
  {
  "productName": "StatTrak™ Desert Eagle | Corinthian (Minimal Wear)",
  "id": 38410
  },
  {
  "productName": "StatTrak™ Desert Eagle | Crimson Web (Factory New)",
  "id": 38413
  },
  {
  "productName": "StatTrak™ Desert Eagle | Crimson Web (Field-Tested)",
  "id": 38414
  },
  {
  "productName": "StatTrak™ Desert Eagle | Crimson Web (Minimal Wear)",
  "id": 38415
  },
  {
  "productName": "StatTrak™ Desert Eagle | Directive (Factory New)",
  "id": 38418
  },
  {
  "productName": "StatTrak™ Desert Eagle | Directive (Field-Tested)",
  "id": 38419
  },
  {
  "productName": "StatTrak™ Desert Eagle | Directive (Minimal Wear)",
  "id": 38420
  },
  {
  "productName": "StatTrak™ Desert Eagle | Golden Koi (Factory New)",
  "id": 38422
  },
  {
  "productName": "StatTrak™ Desert Eagle | Golden Koi (Minimal Wear)",
  "id": 38423
  },
  {
  "productName": "StatTrak™ Desert Eagle | Heirloom (Factory New)",
  "id": 38425
  },
  {
  "productName": "StatTrak™ Desert Eagle | Heirloom (Field-Tested)",
  "id": 38426
  },
  {
  "productName": "StatTrak™ Desert Eagle | Heirloom (Minimal Wear)",
  "id": 38427
  },
  {
  "productName": "StatTrak™ Desert Eagle | Hypnotic (Factory New)",
  "id": 38429
  },
  {
  "productName": "StatTrak™ Desert Eagle | Hypnotic (Minimal Wear)",
  "id": 38430
  },
  {
  "productName": "StatTrak™ Desert Eagle | Kumicho Dragon (Factory New)",
  "id": 38432
  },
  {
  "productName": "StatTrak™ Desert Eagle | Kumicho Dragon (Field-Tested)",
  "id": 38433
  },
  {
  "productName": "StatTrak™ Desert Eagle | Kumicho Dragon (Minimal Wear)",
  "id": 38434
  },
  {
  "productName": "StatTrak™ Desert Eagle | Naga (Factory New)",
  "id": 38437
  },
  {
  "productName": "StatTrak™ Desert Eagle | Naga (Field-Tested)",
  "id": 38438
  },
  {
  "productName": "StatTrak™ Desert Eagle | Naga (Minimal Wear)",
  "id": 38439
  },
  {
  "productName": "StatTrak™ Desert Eagle | Oxide Blaze (Factory New)",
  "id": 38442
  },
  {
  "productName": "StatTrak™ Desert Eagle | Oxide Blaze (Field-Tested)",
  "id": 38443
  },
  {
  "productName": "StatTrak™ Desert Eagle | Oxide Blaze (Minimal Wear)",
  "id": 38444
  },
  {
  "productName": "StatTrak™ Dual Berettas | Black Limba (Factory New)",
  "id": 38447
  },
  {
  "productName": "StatTrak™ Dual Berettas | Black Limba (Field-Tested)",
  "id": 38448
  },
  {
  "productName": "StatTrak™ Dual Berettas | Black Limba (Minimal Wear)",
  "id": 38449
  },
  {
  "productName": "StatTrak™ Dual Berettas | Cartel (Factory New)",
  "id": 38452
  },
  {
  "productName": "StatTrak™ Dual Berettas | Cartel (Field-Tested)",
  "id": 38453
  },
  {
  "productName": "StatTrak™ Dual Berettas | Cartel (Minimal Wear)",
  "id": 38454
  },
  {
  "productName": "StatTrak™ Dual Berettas | Cobra Strike (Factory New)",
  "id": 38457
  },
  {
  "productName": "StatTrak™ Dual Berettas | Cobra Strike (Field-Tested)",
  "id": 38458
  },
  {
  "productName": "StatTrak™ Dual Berettas | Cobra Strike (Minimal Wear)",
  "id": 38459
  },
  {
  "productName": "StatTrak™ Dual Berettas | Dualing Dragons (Factory New)",
  "id": 38462
  },
  {
  "productName": "StatTrak™ Dual Berettas | Dualing Dragons (Field-Tested)",
  "id": 38463
  },
  {
  "productName": "StatTrak™ Dual Berettas | Dualing Dragons (Minimal Wear)",
  "id": 38464
  },
  {
  "productName": "StatTrak™ Dual Berettas | Hemoglobin (Factory New)",
  "id": 38466
  },
  {
  "productName": "StatTrak™ Dual Berettas | Hemoglobin (Field-Tested)",
  "id": 38467
  },
  {
  "productName": "StatTrak™ Dual Berettas | Hemoglobin (Minimal Wear)",
  "id": 38468
  },
  {
  "productName": "StatTrak™ Dual Berettas | Marina (Factory New)",
  "id": 38470
  },
  {
  "productName": "StatTrak™ Dual Berettas | Marina (Field-Tested)",
  "id": 38471
  },
  {
  "productName": "StatTrak™ Dual Berettas | Marina (Minimal Wear)",
  "id": 38472
  },
  {
  "productName": "StatTrak™ Dual Berettas | Panther (Field-Tested)",
  "id": 38475
  },
  {
  "productName": "StatTrak™ Dual Berettas | Panther (Minimal Wear)",
  "id": 38476
  },
  {
  "productName": "StatTrak™ Dual Berettas | Retribution (Factory New)",
  "id": 38478
  },
  {
  "productName": "StatTrak™ Dual Berettas | Retribution (Field-Tested)",
  "id": 38479
  },
  {
  "productName": "StatTrak™ Dual Berettas | Retribution (Minimal Wear)",
  "id": 38480
  },
  {
  "productName": "StatTrak™ Dual Berettas | Royal Consorts (Factory New)",
  "id": 38482
  },
  {
  "productName": "StatTrak™ Dual Berettas | Royal Consorts (Field-Tested)",
  "id": 38483
  },
  {
  "productName": "StatTrak™ Dual Berettas | Royal Consorts (Minimal Wear)",
  "id": 38484
  },
  {
  "productName": "StatTrak™ Dual Berettas | Urban Shock (Factory New)",
  "id": 38487
  },
  {
  "productName": "StatTrak™ Dual Berettas | Urban Shock (Field-Tested)",
  "id": 38488
  },
  {
  "productName": "StatTrak™ Dual Berettas | Urban Shock (Minimal Wear)",
  "id": 38489
  },
  {
  "productName": "StatTrak™ Dual Berettas | Ventilators (Factory New)",
  "id": 38491
  },
  {
  "productName": "StatTrak™ Dual Berettas | Ventilators (Field-Tested)",
  "id": 38492
  },
  {
  "productName": "StatTrak™ Dual Berettas | Ventilators (Minimal Wear)",
  "id": 38493
  },
  {
  "productName": "StatTrak™ FAMAS | Afterimage (Factory New)",
  "id": 38495
  },
  {
  "productName": "StatTrak™ FAMAS | Afterimage (Field-Tested)",
  "id": 38496
  },
  {
  "productName": "StatTrak™ FAMAS | Afterimage (Minimal Wear)",
  "id": 38497
  },
  {
  "productName": "StatTrak™ FAMAS | Djinn (Factory New)",
  "id": 38500
  },
  {
  "productName": "StatTrak™ FAMAS | Djinn (Field-Tested)",
  "id": 38501
  },
  {
  "productName": "StatTrak™ FAMAS | Djinn (Minimal Wear)",
  "id": 38502
  },
  {
  "productName": "StatTrak™ FAMAS | Doomkitty (Field-Tested)",
  "id": 38504
  },
  {
  "productName": "StatTrak™ FAMAS | Doomkitty (Minimal Wear)",
  "id": 38505
  },
  {
  "productName": "StatTrak™ FAMAS | Hexane (Factory New)",
  "id": 38506
  },
  {
  "productName": "StatTrak™ FAMAS | Hexane (Field-Tested)",
  "id": 38507
  },
  {
  "productName": "StatTrak™ FAMAS | Hexane (Minimal Wear)",
  "id": 38508
  },
  {
  "productName": "StatTrak™ FAMAS | Macabre (Factory New)",
  "id": 38511
  },
  {
  "productName": "StatTrak™ FAMAS | Macabre (Field-Tested)",
  "id": 38512
  },
  {
  "productName": "StatTrak™ FAMAS | Macabre (Minimal Wear)",
  "id": 38513
  },
  {
  "productName": "StatTrak™ FAMAS | Mecha Industries (Factory New)",
  "id": 38516
  },
  {
  "productName": "StatTrak™ FAMAS | Mecha Industries (Field-Tested)",
  "id": 38517
  },
  {
  "productName": "StatTrak™ FAMAS | Mecha Industries (Minimal Wear)",
  "id": 38518
  },
  {
  "productName": "StatTrak™ FAMAS | Neural Net (Factory New)",
  "id": 38521
  },
  {
  "productName": "StatTrak™ FAMAS | Neural Net (Field-Tested)",
  "id": 38522
  },
  {
  "productName": "StatTrak™ FAMAS | Neural Net (Minimal Wear)",
  "id": 38523
  },
  {
  "productName": "StatTrak™ FAMAS | Pulse (Factory New)",
  "id": 38524
  },
  {
  "productName": "StatTrak™ FAMAS | Pulse (Field-Tested)",
  "id": 38525
  },
  {
  "productName": "StatTrak™ FAMAS | Pulse (Minimal Wear)",
  "id": 38526
  },
  {
  "productName": "StatTrak™ FAMAS | Roll Cage (Factory New)",
  "id": 38529
  },
  {
  "productName": "StatTrak™ FAMAS | Roll Cage (Field-Tested)",
  "id": 38530
  },
  {
  "productName": "StatTrak™ FAMAS | Roll Cage (Minimal Wear)",
  "id": 38531
  },
  {
  "productName": "StatTrak™ FAMAS | Sergeant (Field-Tested)",
  "id": 38534
  },
  {
  "productName": "StatTrak™ FAMAS | Sergeant (Minimal Wear)",
  "id": 38535
  },
  {
  "productName": "StatTrak™ FAMAS | Survivor Z (Factory New)",
  "id": 38538
  },
  {
  "productName": "StatTrak™ FAMAS | Survivor Z (Field-Tested)",
  "id": 38539
  },
  {
  "productName": "StatTrak™ FAMAS | Survivor Z (Minimal Wear)",
  "id": 38540
  },
  {
  "productName": "StatTrak™ FAMAS | Valence (Factory New)",
  "id": 38543
  },
  {
  "productName": "StatTrak™ FAMAS | Valence (Field-Tested)",
  "id": 38544
  },
  {
  "productName": "StatTrak™ FAMAS | Valence (Minimal Wear)",
  "id": 38545
  },
  {
  "productName": "StatTrak™ Five-SeveN | Capillary (Factory New)",
  "id": 38548
  },
  {
  "productName": "StatTrak™ Five-SeveN | Capillary (Field-Tested)",
  "id": 38549
  },
  {
  "productName": "StatTrak™ Five-SeveN | Capillary (Minimal Wear)",
  "id": 38550
  },
  {
  "productName": "StatTrak™ Five-SeveN | Case Hardened (Factory New)",
  "id": 38553
  },
  {
  "productName": "StatTrak™ Five-SeveN | Case Hardened (Field-Tested)",
  "id": 38554
  },
  {
  "productName": "StatTrak™ Five-SeveN | Case Hardened (Minimal Wear)",
  "id": 38555
  },
  {
  "productName": "StatTrak™ Five-SeveN | Copper Galaxy (Factory New)",
  "id": 38557
  },
  {
  "productName": "StatTrak™ Five-SeveN | Copper Galaxy (Field-Tested)",
  "id": 38558
  },
  {
  "productName": "StatTrak™ Five-SeveN | Copper Galaxy (Minimal Wear)",
  "id": 38559
  },
  {
  "productName": "StatTrak™ Five-SeveN | Fowl Play (Factory New)",
  "id": 38561
  },
  {
  "productName": "StatTrak™ Five-SeveN | Fowl Play (Field-Tested)",
  "id": 38562
  },
  {
  "productName": "StatTrak™ Five-SeveN | Fowl Play (Minimal Wear)",
  "id": 38563
  },
  {
  "productName": "StatTrak™ Five-SeveN | Hyper Beast (Factory New)",
  "id": 38566
  },
  {
  "productName": "StatTrak™ Five-SeveN | Hyper Beast (Field-Tested)",
  "id": 38567
  },
  {
  "productName": "StatTrak™ Five-SeveN | Hyper Beast (Minimal Wear)",
  "id": 38568
  },
  {
  "productName": "StatTrak™ Five-SeveN | Kami (Factory New)",
  "id": 38570
  },
  {
  "productName": "StatTrak™ Five-SeveN | Kami (Field-Tested)",
  "id": 38571
  },
  {
  "productName": "StatTrak™ Five-SeveN | Kami (Minimal Wear)",
  "id": 38572
  },
  {
  "productName": "StatTrak™ Five-SeveN | Monkey Business (Field-Tested)",
  "id": 38574
  },
  {
  "productName": "StatTrak™ Five-SeveN | Monkey Business (Minimal Wear)",
  "id": 38575
  },
  {
  "productName": "StatTrak™ Five-SeveN | Nightshade (Factory New)",
  "id": 38577
  },
  {
  "productName": "StatTrak™ Five-SeveN | Nightshade (Field-Tested)",
  "id": 38578
  },
  {
  "productName": "StatTrak™ Five-SeveN | Nightshade (Minimal Wear)",
  "id": 38579
  },
  {
  "productName": "StatTrak™ Five-SeveN | Retrobution (Factory New)",
  "id": 38582
  },
  {
  "productName": "StatTrak™ Five-SeveN | Retrobution (Field-Tested)",
  "id": 38583
  },
  {
  "productName": "StatTrak™ Five-SeveN | Retrobution (Minimal Wear)",
  "id": 38584
  },
  {
  "productName": "StatTrak™ Five-SeveN | Scumbria (Factory New)",
  "id": 38587
  },
  {
  "productName": "StatTrak™ Five-SeveN | Scumbria (Field-Tested)",
  "id": 38588
  },
  {
  "productName": "StatTrak™ Five-SeveN | Scumbria (Minimal Wear)",
  "id": 38589
  },
  {
  "productName": "StatTrak™ Five-SeveN | Triumvirate (Factory New)",
  "id": 38592
  },
  {
  "productName": "StatTrak™ Five-SeveN | Triumvirate (Field-Tested)",
  "id": 38593
  },
  {
  "productName": "StatTrak™ Five-SeveN | Triumvirate (Minimal Wear)",
  "id": 38594
  },
  {
  "productName": "StatTrak™ Five-SeveN | Urban Hazard (Factory New)",
  "id": 38596
  },
  {
  "productName": "StatTrak™ Five-SeveN | Urban Hazard (Field-Tested)",
  "id": 38597
  },
  {
  "productName": "StatTrak™ Five-SeveN | Urban Hazard (Minimal Wear)",
  "id": 38598
  },
  {
  "productName": "StatTrak™ Five-SeveN | Violent Daimyo (Factory New)",
  "id": 38600
  },
  {
  "productName": "StatTrak™ Five-SeveN | Violent Daimyo (Field-Tested)",
  "id": 38601
  },
  {
  "productName": "StatTrak™ Five-SeveN | Violent Daimyo (Minimal Wear)",
  "id": 38602
  },
  {
  "productName": "StatTrak™ G3SG1 | Azure Zebra (Factory New)",
  "id": 38604
  },
  {
  "productName": "StatTrak™ G3SG1 | Azure Zebra (Field-Tested)",
  "id": 38605
  },
  {
  "productName": "StatTrak™ G3SG1 | Azure Zebra (Minimal Wear)",
  "id": 38606
  },
  {
  "productName": "StatTrak™ G3SG1 | Demeter (Factory New)",
  "id": 38608
  },
  {
  "productName": "StatTrak™ G3SG1 | Demeter (Field-Tested)",
  "id": 38609
  },
  {
  "productName": "StatTrak™ G3SG1 | Demeter (Minimal Wear)",
  "id": 38610
  },
  {
  "productName": "StatTrak™ G3SG1 | Flux (Factory New)",
  "id": 38613
  },
  {
  "productName": "StatTrak™ G3SG1 | Flux (Field-Tested)",
  "id": 38614
  },
  {
  "productName": "StatTrak™ G3SG1 | Flux (Minimal Wear)",
  "id": 38615
  },
  {
  "productName": "StatTrak™ G3SG1 | Hunter (Factory New)",
  "id": 38618
  },
  {
  "productName": "StatTrak™ G3SG1 | Hunter (Field-Tested)",
  "id": 38619
  },
  {
  "productName": "StatTrak™ G3SG1 | Hunter (Minimal Wear)",
  "id": 38620
  },
  {
  "productName": "StatTrak™ G3SG1 | Murky (Factory New)",
  "id": 38622
  },
  {
  "productName": "StatTrak™ G3SG1 | Murky (Field-Tested)",
  "id": 38623
  },
  {
  "productName": "StatTrak™ G3SG1 | Murky (Minimal Wear)",
  "id": 38624
  },
  {
  "productName": "StatTrak™ G3SG1 | Orange Crash (Factory New)",
  "id": 38626
  },
  {
  "productName": "StatTrak™ G3SG1 | Orange Crash (Field-Tested)",
  "id": 38627
  },
  {
  "productName": "StatTrak™ G3SG1 | Orange Crash (Minimal Wear)",
  "id": 38628
  },
  {
  "productName": "StatTrak™ G3SG1 | Stinger (Factory New)",
  "id": 38631
  },
  {
  "productName": "StatTrak™ G3SG1 | Stinger (Field-Tested)",
  "id": 38632
  },
  {
  "productName": "StatTrak™ G3SG1 | Stinger (Minimal Wear)",
  "id": 38633
  },
  {
  "productName": "StatTrak™ G3SG1 | The Executioner (Field-Tested)",
  "id": 38636
  },
  {
  "productName": "StatTrak™ G3SG1 | The Executioner (Minimal Wear)",
  "id": 38637
  },
  {
  "productName": "StatTrak™ G3SG1 | Ventilator (Factory New)",
  "id": 38638
  },
  {
  "productName": "StatTrak™ G3SG1 | Ventilator (Field-Tested)",
  "id": 38639
  },
  {
  "productName": "StatTrak™ G3SG1 | Ventilator (Minimal Wear)",
  "id": 38640
  },
  {
  "productName": "StatTrak™ Galil AR | Black Sand (Factory New)",
  "id": 38643
  },
  {
  "productName": "StatTrak™ Galil AR | Black Sand (Field-Tested)",
  "id": 38644
  },
  {
  "productName": "StatTrak™ Galil AR | Black Sand (Minimal Wear)",
  "id": 38645
  },
  {
  "productName": "StatTrak™ Galil AR | Blue Titanium (Factory New)",
  "id": 38647
  },
  {
  "productName": "StatTrak™ Galil AR | Chatterbox (Field-Tested)",
  "id": 38649
  },
  {
  "productName": "StatTrak™ Galil AR | Crimson Tsunami (Factory New)",
  "id": 38652
  },
  {
  "productName": "StatTrak™ Galil AR | Crimson Tsunami (Field-Tested)",
  "id": 38653
  },
  {
  "productName": "StatTrak™ Galil AR | Crimson Tsunami (Minimal Wear)",
  "id": 38654
  },
  {
  "productName": "StatTrak™ Galil AR | Eco (Field-Tested)",
  "id": 38657
  },
  {
  "productName": "StatTrak™ Galil AR | Eco (Minimal Wear)",
  "id": 38658
  },
  {
  "productName": "StatTrak™ Galil AR | Firefight (Factory New)",
  "id": 38661
  },
  {
  "productName": "StatTrak™ Galil AR | Firefight (Field-Tested)",
  "id": 38662
  },
  {
  "productName": "StatTrak™ Galil AR | Firefight (Minimal Wear)",
  "id": 38663
  },
  {
  "productName": "StatTrak™ Galil AR | Kami (Factory New)",
  "id": 38666
  },
  {
  "productName": "StatTrak™ Galil AR | Kami (Field-Tested)",
  "id": 38667
  },
  {
  "productName": "StatTrak™ Galil AR | Kami (Minimal Wear)",
  "id": 38668
  },
  {
  "productName": "StatTrak™ Galil AR | Orange DDPAT (Factory New)",
  "id": 38671
  },
  {
  "productName": "StatTrak™ Galil AR | Orange DDPAT (Field-Tested)",
  "id": 38672
  },
  {
  "productName": "StatTrak™ Galil AR | Orange DDPAT (Minimal Wear)",
  "id": 38673
  },
  {
  "productName": "StatTrak™ Galil AR | Rocket Pop (Factory New)",
  "id": 38676
  },
  {
  "productName": "StatTrak™ Galil AR | Rocket Pop (Field-Tested)",
  "id": 38677
  },
  {
  "productName": "StatTrak™ Galil AR | Rocket Pop (Minimal Wear)",
  "id": 38678
  },
  {
  "productName": "StatTrak™ Galil AR | Sandstorm (Field-Tested)",
  "id": 38681
  },
  {
  "productName": "StatTrak™ Galil AR | Sandstorm (Minimal Wear)",
  "id": 38682
  },
  {
  "productName": "StatTrak™ Galil AR | Shattered (Factory New)",
  "id": 38685
  },
  {
  "productName": "StatTrak™ Galil AR | Shattered (Field-Tested)",
  "id": 38686
  },
  {
  "productName": "StatTrak™ Galil AR | Shattered (Minimal Wear)",
  "id": 38687
  },
  {
  "productName": "StatTrak™ Galil AR | Stone Cold (Factory New)",
  "id": 38690
  },
  {
  "productName": "StatTrak™ Galil AR | Stone Cold (Field-Tested)",
  "id": 38691
  },
  {
  "productName": "StatTrak™ Galil AR | Stone Cold (Minimal Wear)",
  "id": 38692
  },
  {
  "productName": "StatTrak™ Galil AR | Sugar Rush (Factory New)",
  "id": 38695
  },
  {
  "productName": "StatTrak™ Galil AR | Sugar Rush (Field-Tested)",
  "id": 38696
  },
  {
  "productName": "StatTrak™ Galil AR | Sugar Rush (Minimal Wear)",
  "id": 38697
  },
  {
  "productName": "StatTrak™ Glock-18 | Blue Fissure (Factory New)",
  "id": 38700
  },
  {
  "productName": "StatTrak™ Glock-18 | Blue Fissure (Minimal Wear)",
  "id": 38701
  },
  {
  "productName": "StatTrak™ Glock-18 | Bunsen Burner (Factory New)",
  "id": 38704
  },
  {
  "productName": "StatTrak™ Glock-18 | Bunsen Burner (Field-Tested)",
  "id": 38705
  },
  {
  "productName": "StatTrak™ Glock-18 | Bunsen Burner (Minimal Wear)",
  "id": 38706
  },
  {
  "productName": "StatTrak™ Glock-18 | Catacombs (Factory New)",
  "id": 38709
  },
  {
  "productName": "StatTrak™ Glock-18 | Catacombs (Field-Tested)",
  "id": 38710
  },
  {
  "productName": "StatTrak™ Glock-18 | Catacombs (Minimal Wear)",
  "id": 38711
  },
  {
  "productName": "StatTrak™ Glock-18 | Dragon Tattoo (Factory New)",
  "id": 38713
  },
  {
  "productName": "StatTrak™ Glock-18 | Dragon Tattoo (Minimal Wear)",
  "id": 38714
  },
  {
  "productName": "StatTrak™ Glock-18 | Grinder (Factory New)",
  "id": 38715
  },
  {
  "productName": "StatTrak™ Glock-18 | Grinder (Field-Tested)",
  "id": 38716
  },
  {
  "productName": "StatTrak™ Glock-18 | Grinder (Minimal Wear)",
  "id": 38717
  },
  {
  "productName": "StatTrak™ Glock-18 | Ironwork (Factory New)",
  "id": 38719
  },
  {
  "productName": "StatTrak™ Glock-18 | Ironwork (Field-Tested)",
  "id": 38720
  },
  {
  "productName": "StatTrak™ Glock-18 | Ironwork (Minimal Wear)",
  "id": 38721
  },
  {
  "productName": "StatTrak™ Glock-18 | Off World (Factory New)",
  "id": 38724
  },
  {
  "productName": "StatTrak™ Glock-18 | Off World (Field-Tested)",
  "id": 38725
  },
  {
  "productName": "StatTrak™ Glock-18 | Off World (Minimal Wear)",
  "id": 38726
  },
  {
  "productName": "StatTrak™ Glock-18 | Royal Legion (Factory New)",
  "id": 38729
  },
  {
  "productName": "StatTrak™ Glock-18 | Royal Legion (Field-Tested)",
  "id": 38730
  },
  {
  "productName": "StatTrak™ Glock-18 | Royal Legion (Minimal Wear)",
  "id": 38731
  },
  {
  "productName": "StatTrak™ Glock-18 | Steel Disruption (Factory New)",
  "id": 38733
  },
  {
  "productName": "StatTrak™ Glock-18 | Steel Disruption (Field-Tested)",
  "id": 38734
  },
  {
  "productName": "StatTrak™ Glock-18 | Steel Disruption (Minimal Wear)",
  "id": 38735
  },
  {
  "productName": "StatTrak™ Glock-18 | Wasteland Rebel (Factory New)",
  "id": 38737
  },
  {
  "productName": "StatTrak™ Glock-18 | Wasteland Rebel (Field-Tested)",
  "id": 38738
  },
  {
  "productName": "StatTrak™ Glock-18 | Wasteland Rebel (Minimal Wear)",
  "id": 38739
  },
  {
  "productName": "StatTrak™ Glock-18 | Water Elemental (Factory New)",
  "id": 38742
  },
  {
  "productName": "StatTrak™ Glock-18 | Water Elemental (Field-Tested)",
  "id": 38743
  },
  {
  "productName": "StatTrak™ Glock-18 | Water Elemental (Minimal Wear)",
  "id": 38744
  },
  {
  "productName": "StatTrak™ Glock-18 | Weasel (Factory New)",
  "id": 38747
  },
  {
  "productName": "StatTrak™ Glock-18 | Weasel (Field-Tested)",
  "id": 38748
  },
  {
  "productName": "StatTrak™ Glock-18 | Weasel (Minimal Wear)",
  "id": 38749
  },
  {
  "productName": "StatTrak™ Glock-18 | Wraiths (Factory New)",
  "id": 38752
  },
  {
  "productName": "StatTrak™ Glock-18 | Wraiths (Field-Tested)",
  "id": 38753
  },
  {
  "productName": "StatTrak™ Glock-18 | Wraiths (Minimal Wear)",
  "id": 38754
  },
  {
  "productName": "StatTrak™ M249 | Emerald Poison Dart (Factory New)",
  "id": 38756
  },
  {
  "productName": "StatTrak™ M249 | Emerald Poison Dart (Field-Tested)",
  "id": 38757
  },
  {
  "productName": "StatTrak™ M249 | Emerald Poison Dart (Minimal Wear)",
  "id": 38758
  },
  {
  "productName": "StatTrak™ M249 | Magma (Factory New)",
  "id": 38761
  },
  {
  "productName": "StatTrak™ M249 | Magma (Field-Tested)",
  "id": 38762
  },
  {
  "productName": "StatTrak™ M249 | Magma (Minimal Wear)",
  "id": 38763
  },
  {
  "productName": "StatTrak™ M249 | Nebula Crusader (Factory New)",
  "id": 38766
  },
  {
  "productName": "StatTrak™ M249 | Nebula Crusader (Field-Tested)",
  "id": 38767
  },
  {
  "productName": "StatTrak™ M249 | Nebula Crusader (Minimal Wear)",
  "id": 38768
  },
  {
  "productName": "StatTrak™ M249 | Spectre (Factory New)",
  "id": 38771
  },
  {
  "productName": "StatTrak™ M249 | Spectre (Field-Tested)",
  "id": 38772
  },
  {
  "productName": "StatTrak™ M249 | Spectre (Minimal Wear)",
  "id": 38773
  },
  {
  "productName": "StatTrak™ M249 | System Lock (Factory New)",
  "id": 38776
  },
  {
  "productName": "StatTrak™ M249 | System Lock (Field-Tested)",
  "id": 38777
  },
  {
  "productName": "StatTrak™ M249 | System Lock (Minimal Wear)",
  "id": 38778
  },
  {
  "productName": "StatTrak™ M4A1-S | Atomic Alloy (Factory New)",
  "id": 38781
  },
  {
  "productName": "StatTrak™ M4A1-S | Atomic Alloy (Field-Tested)",
  "id": 38782
  },
  {
  "productName": "StatTrak™ M4A1-S | Atomic Alloy (Minimal Wear)",
  "id": 38783
  },
  {
  "productName": "StatTrak™ M4A1-S | Basilisk (Factory New)",
  "id": 38786
  },
  {
  "productName": "StatTrak™ M4A1-S | Basilisk (Field-Tested)",
  "id": 38787
  },
  {
  "productName": "StatTrak™ M4A1-S | Basilisk (Minimal Wear)",
  "id": 38788
  },
  {
  "productName": "StatTrak™ M4A1-S | Blood Tiger (Factory New)",
  "id": 38790
  },
  {
  "productName": "StatTrak™ M4A1-S | Blood Tiger (Field-Tested)",
  "id": 38791
  },
  {
  "productName": "StatTrak™ M4A1-S | Blood Tiger (Minimal Wear)",
  "id": 38792
  },
  {
  "productName": "StatTrak™ M4A1-S | Briefing (Factory New)",
  "id": 38793
  },
  {
  "productName": "StatTrak™ M4A1-S | Briefing (Field-Tested)",
  "id": 38794
  },
  {
  "productName": "StatTrak™ M4A1-S | Briefing (Minimal Wear)",
  "id": 38795
  },
  {
  "productName": "StatTrak™ M4A1-S | Bright Water (Field-Tested)",
  "id": 38797
  },
  {
  "productName": "StatTrak™ M4A1-S | Bright Water (Minimal Wear)",
  "id": 38798
  },
  {
  "productName": "StatTrak™ M4A1-S | Chantico's Fire (Factory New)",
  "id": 38800
  },
  {
  "productName": "StatTrak™ M4A1-S | Chantico's Fire (Field-Tested)",
  "id": 38801
  },
  {
  "productName": "StatTrak™ M4A1-S | Chantico's Fire (Minimal Wear)",
  "id": 38802
  },
  {
  "productName": "StatTrak™ M4A1-S | Cyrex (Minimal Wear)",
  "id": 38805
  },
  {
  "productName": "StatTrak™ M4A1-S | Dark Water (Field-Tested)",
  "id": 38807
  },
  {
  "productName": "StatTrak™ M4A1-S | Dark Water (Minimal Wear)",
  "id": 38808
  },
  {
  "productName": "StatTrak™ M4A1-S | Decimator (Factory New)",
  "id": 38810
  },
  {
  "productName": "StatTrak™ M4A1-S | Decimator (Field-Tested)",
  "id": 38811
  },
  {
  "productName": "StatTrak™ M4A1-S | Decimator (Minimal Wear)",
  "id": 38812
  },
  {
  "productName": "StatTrak™ M4A1-S | Flashback (Factory New)",
  "id": 38815
  },
  {
  "productName": "StatTrak™ M4A1-S | Flashback (Field-Tested)",
  "id": 38816
  },
  {
  "productName": "StatTrak™ M4A1-S | Flashback (Minimal Wear)",
  "id": 38817
  },
  {
  "productName": "StatTrak™ M4A1-S | Golden Coil (Factory New)",
  "id": 38820
  },
  {
  "productName": "StatTrak™ M4A1-S | Golden Coil (Field-Tested)",
  "id": 38821
  },
  {
  "productName": "StatTrak™ M4A1-S | Golden Coil (Minimal Wear)",
  "id": 38822
  },
  {
  "productName": "StatTrak™ M4A1-S | Guardian (Factory New)",
  "id": 38825
  },
  {
  "productName": "StatTrak™ M4A1-S | Guardian (Minimal Wear)",
  "id": 38826
  },
  {
  "productName": "StatTrak™ M4A1-S | Hyper Beast (Factory New)",
  "id": 38829
  },
  {
  "productName": "StatTrak™ M4A1-S | Hyper Beast (Field-Tested)",
  "id": 38830
  },
  {
  "productName": "StatTrak™ M4A1-S | Hyper Beast (Minimal Wear)",
  "id": 38831
  },
  {
  "productName": "StatTrak™ M4A1-S | Leaded Glass (Factory New)",
  "id": 38834
  },
  {
  "productName": "StatTrak™ M4A1-S | Leaded Glass (Field-Tested)",
  "id": 38835
  },
  {
  "productName": "StatTrak™ M4A1-S | Leaded Glass (Minimal Wear)",
  "id": 38836
  },
  {
  "productName": "StatTrak™ M4A1-S | Mecha Industries (Factory New)",
  "id": 38839
  },
  {
  "productName": "StatTrak™ M4A1-S | Mecha Industries (Field-Tested)",
  "id": 38840
  },
  {
  "productName": "StatTrak™ M4A1-S | Mecha Industries (Minimal Wear)",
  "id": 38841
  },
  {
  "productName": "StatTrak™ M4A4 | Asiimov (Field-Tested)",
  "id": 38844
  },
  {
  "productName": "StatTrak™ M4A4 | Bullet Rain (Factory New)",
  "id": 38847
  },
  {
  "productName": "StatTrak™ M4A4 | Bullet Rain (Field-Tested)",
  "id": 38848
  },
  {
  "productName": "StatTrak™ M4A4 | Bullet Rain (Minimal Wear)",
  "id": 38849
  },
  {
  "productName": "StatTrak™ M4A4 | Buzz Kill (Factory New)",
  "id": 38852
  },
  {
  "productName": "StatTrak™ M4A4 | Buzz Kill (Field-Tested)",
  "id": 38853
  },
  {
  "productName": "StatTrak™ M4A4 | Buzz Kill (Minimal Wear)",
  "id": 38854
  },
  {
  "productName": "StatTrak™ M4A4 | Desert-Strike (Factory New)",
  "id": 38857
  },
  {
  "productName": "StatTrak™ M4A4 | Desert-Strike (Field-Tested)",
  "id": 38858
  },
  {
  "productName": "StatTrak™ M4A4 | Desert-Strike (Minimal Wear)",
  "id": 38859
  },
  {
  "productName": "StatTrak™ M4A4 | Desolate Space (Factory New)",
  "id": 38862
  },
  {
  "productName": "StatTrak™ M4A4 | Desolate Space (Field-Tested)",
  "id": 38863
  },
  {
  "productName": "StatTrak™ M4A4 | Desolate Space (Minimal Wear)",
  "id": 38864
  },
  {
  "productName": "StatTrak™ M4A4 | Evil Daimyo (Factory New)",
  "id": 38867
  },
  {
  "productName": "StatTrak™ M4A4 | Evil Daimyo (Field-Tested)",
  "id": 38868
  },
  {
  "productName": "StatTrak™ M4A4 | Evil Daimyo (Minimal Wear)",
  "id": 38869
  },
  {
  "productName": "StatTrak™ M4A4 | Faded Zebra (Factory New)",
  "id": 38872
  },
  {
  "productName": "StatTrak™ M4A4 | Faded Zebra (Field-Tested)",
  "id": 38873
  },
  {
  "productName": "StatTrak™ M4A4 | Faded Zebra (Minimal Wear)",
  "id": 38874
  },
  {
  "productName": "StatTrak™ M4A4 | Griffin (Factory New)",
  "id": 38876
  },
  {
  "productName": "StatTrak™ M4A4 | Griffin (Field-Tested)",
  "id": 38877
  },
  {
  "productName": "StatTrak™ M4A4 | Griffin (Minimal Wear)",
  "id": 38878
  },
  {
  "productName": "StatTrak™ M4A4 | Hellfire (Factory New)",
  "id": 38881
  },
  {
  "productName": "StatTrak™ M4A4 | Hellfire (Field-Tested)",
  "id": 38882
  },
  {
  "productName": "StatTrak™ M4A4 | Hellfire (Minimal Wear)",
  "id": 38883
  },
  {
  "productName": "StatTrak™ M4A4 | Howl (Field-Tested)",
  "id": 38885
  },
  {
  "productName": "StatTrak™ M4A4 | Royal Paladin (Factory New)",
  "id": 38887
  },
  {
  "productName": "StatTrak™ M4A4 | Royal Paladin (Field-Tested)",
  "id": 38888
  },
  {
  "productName": "StatTrak™ M4A4 | Royal Paladin (Minimal Wear)",
  "id": 38889
  },
  {
  "productName": "StatTrak™ M4A4 | The Battlestar (Factory New)",
  "id": 38892
  },
  {
  "productName": "StatTrak™ M4A4 | The Battlestar (Field-Tested)",
  "id": 38893
  },
  {
  "productName": "StatTrak™ M4A4 | The Battlestar (Minimal Wear)",
  "id": 38894
  },
  {
  "productName": "StatTrak™ M4A4 | X-Ray (Factory New)",
  "id": 38896
  },
  {
  "productName": "StatTrak™ M4A4 | X-Ray (Field-Tested)",
  "id": 38897
  },
  {
  "productName": "StatTrak™ M4A4 | X-Ray (Minimal Wear)",
  "id": 38898
  },
  {
  "productName": "StatTrak™ M4A4 | Zirka (Factory New)",
  "id": 38899
  },
  {
  "productName": "StatTrak™ M4A4 | Zirka (Field-Tested)",
  "id": 38900
  },
  {
  "productName": "StatTrak™ M4A4 | Zirka (Minimal Wear)",
  "id": 38901
  },
  {
  "productName": "StatTrak™ M4A4 | 龍王 (Dragon King) (Factory New)",
  "id": 38904
  },
  {
  "productName": "StatTrak™ M4A4 | 龍王 (Dragon King) (Field-Tested)",
  "id": 38905
  },
  {
  "productName": "StatTrak™ M4A4 | 龍王 (Dragon King) (Minimal Wear)",
  "id": 38906
  },
  {
  "productName": "StatTrak™ MAC-10 | Aloha (Factory New)",
  "id": 38909
  },
  {
  "productName": "StatTrak™ MAC-10 | Aloha (Field-Tested)",
  "id": 38910
  },
  {
  "productName": "StatTrak™ MAC-10 | Aloha (Minimal Wear)",
  "id": 38911
  },
  {
  "productName": "StatTrak™ MAC-10 | Carnivore (Factory New)",
  "id": 38914
  },
  {
  "productName": "StatTrak™ MAC-10 | Carnivore (Field-Tested)",
  "id": 38915
  },
  {
  "productName": "StatTrak™ MAC-10 | Carnivore (Minimal Wear)",
  "id": 38916
  },
  {
  "productName": "StatTrak™ MAC-10 | Curse (Factory New)",
  "id": 38919
  },
  {
  "productName": "StatTrak™ MAC-10 | Curse (Field-Tested)",
  "id": 38920
  },
  {
  "productName": "StatTrak™ MAC-10 | Curse (Minimal Wear)",
  "id": 38921
  },
  {
  "productName": "StatTrak™ MAC-10 | Graven (Factory New)",
  "id": 38924
  },
  {
  "productName": "StatTrak™ MAC-10 | Graven (Field-Tested)",
  "id": 38925
  },
  {
  "productName": "StatTrak™ MAC-10 | Graven (Minimal Wear)",
  "id": 38926
  },
  {
  "productName": "StatTrak™ MAC-10 | Heat (Field-Tested)",
  "id": 38929
  },
  {
  "productName": "StatTrak™ MAC-10 | Heat (Minimal Wear)",
  "id": 38930
  },
  {
  "productName": "StatTrak™ MAC-10 | Lapis Gator (Factory New)",
  "id": 38933
  },
  {
  "productName": "StatTrak™ MAC-10 | Lapis Gator (Field-Tested)",
  "id": 38934
  },
  {
  "productName": "StatTrak™ MAC-10 | Lapis Gator (Minimal Wear)",
  "id": 38935
  },
  {
  "productName": "StatTrak™ MAC-10 | Last Dive (Factory New)",
  "id": 38938
  },
  {
  "productName": "StatTrak™ MAC-10 | Last Dive (Field-Tested)",
  "id": 38939
  },
  {
  "productName": "StatTrak™ MAC-10 | Last Dive (Minimal Wear)",
  "id": 38940
  },
  {
  "productName": "StatTrak™ MAC-10 | Malachite (Factory New)",
  "id": 38943
  },
  {
  "productName": "StatTrak™ MAC-10 | Malachite (Field-Tested)",
  "id": 38944
  },
  {
  "productName": "StatTrak™ MAC-10 | Malachite (Minimal Wear)",
  "id": 38945
  },
  {
  "productName": "StatTrak™ MAC-10 | Neon Rider (Factory New)",
  "id": 38947
  },
  {
  "productName": "StatTrak™ MAC-10 | Neon Rider (Field-Tested)",
  "id": 38948
  },
  {
  "productName": "StatTrak™ MAC-10 | Neon Rider (Minimal Wear)",
  "id": 38949
  },
  {
  "productName": "StatTrak™ MAC-10 | Oceanic (Factory New)",
  "id": 38952
  },
  {
  "productName": "StatTrak™ MAC-10 | Oceanic (Field-Tested)",
  "id": 38953
  },
  {
  "productName": "StatTrak™ MAC-10 | Oceanic (Minimal Wear)",
  "id": 38954
  },
  {
  "productName": "StatTrak™ MAC-10 | Rangeen (Factory New)",
  "id": 38957
  },
  {
  "productName": "StatTrak™ MAC-10 | Rangeen (Field-Tested)",
  "id": 38958
  },
  {
  "productName": "StatTrak™ MAC-10 | Rangeen (Minimal Wear)",
  "id": 38959
  },
  {
  "productName": "StatTrak™ MAC-10 | Tatter (Factory New)",
  "id": 38962
  },
  {
  "productName": "StatTrak™ MAC-10 | Tatter (Field-Tested)",
  "id": 38963
  },
  {
  "productName": "StatTrak™ MAC-10 | Tatter (Minimal Wear)",
  "id": 38964
  },
  {
  "productName": "StatTrak™ MAC-10 | Ultraviolet (Factory New)",
  "id": 38967
  },
  {
  "productName": "StatTrak™ MAC-10 | Ultraviolet (Field-Tested)",
  "id": 38968
  },
  {
  "productName": "StatTrak™ MAC-10 | Ultraviolet (Minimal Wear)",
  "id": 38969
  },
  {
  "productName": "StatTrak™ MAG-7 | Cobalt Core (Factory New)",
  "id": 38972
  },
  {
  "productName": "StatTrak™ MAG-7 | Cobalt Core (Field-Tested)",
  "id": 38973
  },
  {
  "productName": "StatTrak™ MAG-7 | Cobalt Core (Minimal Wear)",
  "id": 38974
  },
  {
  "productName": "StatTrak™ MAG-7 | Firestarter (Factory New)",
  "id": 38977
  },
  {
  "productName": "StatTrak™ MAG-7 | Firestarter (Field-Tested)",
  "id": 38978
  },
  {
  "productName": "StatTrak™ MAG-7 | Firestarter (Minimal Wear)",
  "id": 38979
  },
  {
  "productName": "StatTrak™ MAG-7 | Hard Water (Factory New)",
  "id": 38981
  },
  {
  "productName": "StatTrak™ MAG-7 | Hard Water (Field-Tested)",
  "id": 38982
  },
  {
  "productName": "StatTrak™ MAG-7 | Hard Water (Minimal Wear)",
  "id": 38983
  },
  {
  "productName": "StatTrak™ MAG-7 | Heat (Factory New)",
  "id": 38986
  },
  {
  "productName": "StatTrak™ MAG-7 | Heat (Field-Tested)",
  "id": 38987
  },
  {
  "productName": "StatTrak™ MAG-7 | Heat (Minimal Wear)",
  "id": 38988
  },
  {
  "productName": "StatTrak™ MAG-7 | Heaven Guard (Factory New)",
  "id": 38990
  },
  {
  "productName": "StatTrak™ MAG-7 | Heaven Guard (Field-Tested)",
  "id": 38991
  },
  {
  "productName": "StatTrak™ MAG-7 | Heaven Guard (Minimal Wear)",
  "id": 38992
  },
  {
  "productName": "StatTrak™ MAG-7 | Memento (Factory New)",
  "id": 38994
  },
  {
  "productName": "StatTrak™ MAG-7 | Memento (Field-Tested)",
  "id": 38995
  },
  {
  "productName": "StatTrak™ MAG-7 | Memento (Minimal Wear)",
  "id": 38996
  },
  {
  "productName": "StatTrak™ MAG-7 | Petroglyph (Factory New)",
  "id": 38997
  },
  {
  "productName": "StatTrak™ MAG-7 | Petroglyph (Field-Tested)",
  "id": 38998
  },
  {
  "productName": "StatTrak™ MAG-7 | Petroglyph (Minimal Wear)",
  "id": 38999
  },
  {
  "productName": "StatTrak™ MAG-7 | Praetorian (Factory New)",
  "id": 39001
  },
  {
  "productName": "StatTrak™ MAG-7 | Praetorian (Field-Tested)",
  "id": 39002
  },
  {
  "productName": "StatTrak™ MAG-7 | Praetorian (Minimal Wear)",
  "id": 39003
  },
  {
  "productName": "StatTrak™ MAG-7 | Sonar (Factory New)",
  "id": 39005
  },
  {
  "productName": "StatTrak™ MAG-7 | Sonar (Field-Tested)",
  "id": 39006
  },
  {
  "productName": "StatTrak™ MAG-7 | Sonar (Minimal Wear)",
  "id": 39007
  },
  {
  "productName": "StatTrak™ MP7 | Akoben (Factory New)",
  "id": 39009
  },
  {
  "productName": "StatTrak™ MP7 | Akoben (Field-Tested)",
  "id": 39010
  },
  {
  "productName": "StatTrak™ MP7 | Akoben (Minimal Wear)",
  "id": 39011
  },
  {
  "productName": "StatTrak™ MP7 | Armor Core (Factory New)",
  "id": 39014
  },
  {
  "productName": "StatTrak™ MP7 | Armor Core (Field-Tested)",
  "id": 39015
  },
  {
  "productName": "StatTrak™ MP7 | Armor Core (Minimal Wear)",
  "id": 39016
  },
  {
  "productName": "StatTrak™ MP7 | Cirrus (Factory New)",
  "id": 39019
  },
  {
  "productName": "StatTrak™ MP7 | Cirrus (Field-Tested)",
  "id": 39020
  },
  {
  "productName": "StatTrak™ MP7 | Cirrus (Minimal Wear)",
  "id": 39021
  },
  {
  "productName": "StatTrak™ MP7 | Impire (Factory New)",
  "id": 39023
  },
  {
  "productName": "StatTrak™ MP7 | Impire (Field-Tested)",
  "id": 39024
  },
  {
  "productName": "StatTrak™ MP7 | Impire (Minimal Wear)",
  "id": 39025
  },
  {
  "productName": "StatTrak™ MP7 | Nemesis (Factory New)",
  "id": 39027
  },
  {
  "productName": "StatTrak™ MP7 | Nemesis (Field-Tested)",
  "id": 39028
  },
  {
  "productName": "StatTrak™ MP7 | Nemesis (Minimal Wear)",
  "id": 39029
  },
  {
  "productName": "StatTrak™ MP7 | Ocean Foam (Factory New)",
  "id": 39030
  },
  {
  "productName": "StatTrak™ MP7 | Ocean Foam (Minimal Wear)",
  "id": 39031
  },
  {
  "productName": "StatTrak™ MP7 | Skulls (Field-Tested)",
  "id": 39032
  },
  {
  "productName": "StatTrak™ MP7 | Skulls (Minimal Wear)",
  "id": 39033
  },
  {
  "productName": "StatTrak™ MP7 | Special Delivery (Factory New)",
  "id": 39035
  },
  {
  "productName": "StatTrak™ MP7 | Special Delivery (Field-Tested)",
  "id": 39036
  },
  {
  "productName": "StatTrak™ MP7 | Special Delivery (Minimal Wear)",
  "id": 39037
  },
  {
  "productName": "StatTrak™ MP7 | Urban Hazard (Factory New)",
  "id": 39040
  },
  {
  "productName": "StatTrak™ MP7 | Urban Hazard (Field-Tested)",
  "id": 39041
  },
  {
  "productName": "StatTrak™ MP7 | Urban Hazard (Minimal Wear)",
  "id": 39042
  },
  {
  "productName": "StatTrak™ MP9 | Airlock (Factory New)",
  "id": 39045
  },
  {
  "productName": "StatTrak™ MP9 | Airlock (Field-Tested)",
  "id": 39046
  },
  {
  "productName": "StatTrak™ MP9 | Airlock (Minimal Wear)",
  "id": 39047
  },
  {
  "productName": "StatTrak™ MP9 | Bioleak (Factory New)",
  "id": 39050
  },
  {
  "productName": "StatTrak™ MP9 | Bioleak (Field-Tested)",
  "id": 39051
  },
  {
  "productName": "StatTrak™ MP9 | Bioleak (Minimal Wear)",
  "id": 39052
  },
  {
  "productName": "StatTrak™ MP9 | Dart (Factory New)",
  "id": 39055
  },
  {
  "productName": "StatTrak™ MP9 | Dart (Field-Tested)",
  "id": 39056
  },
  {
  "productName": "StatTrak™ MP9 | Dart (Minimal Wear)",
  "id": 39057
  },
  {
  "productName": "StatTrak™ MP9 | Deadly Poison (Factory New)",
  "id": 39060
  },
  {
  "productName": "StatTrak™ MP9 | Deadly Poison (Field-Tested)",
  "id": 39061
  },
  {
  "productName": "StatTrak™ MP9 | Deadly Poison (Minimal Wear)",
  "id": 39062
  },
  {
  "productName": "StatTrak™ MP9 | Goo (Factory New)",
  "id": 39065
  },
  {
  "productName": "StatTrak™ MP9 | Goo (Field-Tested)",
  "id": 39066
  },
  {
  "productName": "StatTrak™ MP9 | Goo (Minimal Wear)",
  "id": 39067
  },
  {
  "productName": "StatTrak™ MP9 | Hypnotic (Factory New)",
  "id": 39069
  },
  {
  "productName": "StatTrak™ MP9 | Hypnotic (Minimal Wear)",
  "id": 39070
  },
  {
  "productName": "StatTrak™ MP9 | Rose Iron (Factory New)",
  "id": 39071
  },
  {
  "productName": "StatTrak™ MP9 | Rose Iron (Field-Tested)",
  "id": 39072
  },
  {
  "productName": "StatTrak™ MP9 | Rose Iron (Minimal Wear)",
  "id": 39073
  },
  {
  "productName": "StatTrak™ MP9 | Ruby Poison Dart (Factory New)",
  "id": 39075
  },
  {
  "productName": "StatTrak™ MP9 | Ruby Poison Dart (Field-Tested)",
  "id": 39076
  },
  {
  "productName": "StatTrak™ MP9 | Ruby Poison Dart (Minimal Wear)",
  "id": 39077
  },
  {
  "productName": "StatTrak™ MP9 | Sand Scale (Factory New)",
  "id": 39079
  },
  {
  "productName": "StatTrak™ MP9 | Sand Scale (Minimal Wear)",
  "id": 39080
  },
  {
  "productName": "StatTrak™ Negev | Bratatat (Factory New)",
  "id": 39119
  },
  {
  "productName": "StatTrak™ Negev | Bratatat (Field-Tested)",
  "id": 39120
  },
  {
  "productName": "StatTrak™ Negev | Bratatat (Minimal Wear)",
  "id": 39121
  },
  {
  "productName": "StatTrak™ Negev | Dazzle (Field-Tested)",
  "id": 39124
  },
  {
  "productName": "StatTrak™ Negev | Dazzle (Minimal Wear)",
  "id": 39125
  },
  {
  "productName": "StatTrak™ Negev | Desert-Strike (Factory New)",
  "id": 39128
  },
  {
  "productName": "StatTrak™ Negev | Desert-Strike (Field-Tested)",
  "id": 39129
  },
  {
  "productName": "StatTrak™ Negev | Desert-Strike (Minimal Wear)",
  "id": 39130
  },
  {
  "productName": "StatTrak™ Negev | Loudmouth (Field-Tested)",
  "id": 39133
  },
  {
  "productName": "StatTrak™ Negev | Loudmouth (Minimal Wear)",
  "id": 39134
  },
  {
  "productName": "StatTrak™ Negev | Man-o'-war (Field-Tested)",
  "id": 39135
  },
  {
  "productName": "StatTrak™ Negev | Man-o'-war (Minimal Wear)",
  "id": 39136
  },
  {
  "productName": "StatTrak™ Negev | Power Loader (Factory New)",
  "id": 39138
  },
  {
  "productName": "StatTrak™ Negev | Power Loader (Field-Tested)",
  "id": 39139
  },
  {
  "productName": "StatTrak™ Negev | Power Loader (Minimal Wear)",
  "id": 39140
  },
  {
  "productName": "StatTrak™ Negev | Terrain (Factory New)",
  "id": 39142
  },
  {
  "productName": "StatTrak™ Negev | Terrain (Field-Tested)",
  "id": 39143
  },
  {
  "productName": "StatTrak™ Negev | Terrain (Minimal Wear)",
  "id": 39144
  },
  {
  "productName": "StatTrak™ Nova | Antique (Factory New)",
  "id": 39146
  },
  {
  "productName": "StatTrak™ Nova | Antique (Field-Tested)",
  "id": 39147
  },
  {
  "productName": "StatTrak™ Nova | Antique (Minimal Wear)",
  "id": 39148
  },
  {
  "productName": "StatTrak™ Nova | Bloomstick (Factory New)",
  "id": 39150
  },
  {
  "productName": "StatTrak™ Nova | Bloomstick (Field-Tested)",
  "id": 39151
  },
  {
  "productName": "StatTrak™ Nova | Bloomstick (Minimal Wear)",
  "id": 39152
  },
  {
  "productName": "StatTrak™ Nova | Exo (Factory New)",
  "id": 39155
  },
  {
  "productName": "StatTrak™ Nova | Exo (Field-Tested)",
  "id": 39156
  },
  {
  "productName": "StatTrak™ Nova | Exo (Minimal Wear)",
  "id": 39157
  },
  {
  "productName": "StatTrak™ Nova | Ghost Camo (Factory New)",
  "id": 39159
  },
  {
  "productName": "StatTrak™ Nova | Ghost Camo (Field-Tested)",
  "id": 39160
  },
  {
  "productName": "StatTrak™ Nova | Ghost Camo (Minimal Wear)",
  "id": 39161
  },
  {
  "productName": "StatTrak™ Nova | Gila (Factory New)",
  "id": 39163
  },
  {
  "productName": "StatTrak™ Nova | Gila (Field-Tested)",
  "id": 39164
  },
  {
  "productName": "StatTrak™ Nova | Gila (Minimal Wear)",
  "id": 39165
  },
  {
  "productName": "StatTrak™ Nova | Graphite (Factory New)",
  "id": 39166
  },
  {
  "productName": "StatTrak™ Nova | Graphite (Minimal Wear)",
  "id": 39167
  },
  {
  "productName": "StatTrak™ Nova | Hyper Beast (Factory New)",
  "id": 39169
  },
  {
  "productName": "StatTrak™ Nova | Hyper Beast (Field-Tested)",
  "id": 39170
  },
  {
  "productName": "StatTrak™ Nova | Hyper Beast (Minimal Wear)",
  "id": 39171
  },
  {
  "productName": "StatTrak™ Nova | Koi (Factory New)",
  "id": 39173
  },
  {
  "productName": "StatTrak™ Nova | Koi (Field-Tested)",
  "id": 39174
  },
  {
  "productName": "StatTrak™ Nova | Koi (Minimal Wear)",
  "id": 39175
  },
  {
  "productName": "StatTrak™ Nova | Ranger (Factory New)",
  "id": 39176
  },
  {
  "productName": "StatTrak™ Nova | Ranger (Field-Tested)",
  "id": 39177
  },
  {
  "productName": "StatTrak™ Nova | Ranger (Minimal Wear)",
  "id": 39178
  },
  {
  "productName": "StatTrak™ Nova | Rising Skull (Factory New)",
  "id": 39181
  },
  {
  "productName": "StatTrak™ Nova | Rising Skull (Field-Tested)",
  "id": 39182
  },
  {
  "productName": "StatTrak™ Nova | Rising Skull (Minimal Wear)",
  "id": 39183
  },
  {
  "productName": "StatTrak™ Nova | Tempest (Factory New)",
  "id": 39185
  },
  {
  "productName": "StatTrak™ Nova | Tempest (Field-Tested)",
  "id": 39186
  },
  {
  "productName": "StatTrak™ Nova | Tempest (Minimal Wear)",
  "id": 39187
  },
  {
  "productName": "StatTrak™ P2000 | Corticera (Factory New)",
  "id": 39188
  },
  {
  "productName": "StatTrak™ P2000 | Corticera (Field-Tested)",
  "id": 39189
  },
  {
  "productName": "StatTrak™ P2000 | Corticera (Minimal Wear)",
  "id": 39190
  },
  {
  "productName": "StatTrak™ P2000 | Fire Elemental (Factory New)",
  "id": 39192
  },
  {
  "productName": "StatTrak™ P2000 | Fire Elemental (Field-Tested)",
  "id": 39193
  },
  {
  "productName": "StatTrak™ P2000 | Fire Elemental (Minimal Wear)",
  "id": 39194
  },
  {
  "productName": "StatTrak™ P2000 | Handgun (Factory New)",
  "id": 39197
  },
  {
  "productName": "StatTrak™ P2000 | Handgun (Field-Tested)",
  "id": 39198
  },
  {
  "productName": "StatTrak™ P2000 | Handgun (Minimal Wear)",
  "id": 39199
  },
  {
  "productName": "StatTrak™ P2000 | Imperial (Factory New)",
  "id": 39201
  },
  {
  "productName": "StatTrak™ P2000 | Imperial (Field-Tested)",
  "id": 39202
  },
  {
  "productName": "StatTrak™ P2000 | Imperial (Minimal Wear)",
  "id": 39203
  },
  {
  "productName": "StatTrak™ P2000 | Imperial Dragon (Factory New)",
  "id": 39205
  },
  {
  "productName": "StatTrak™ P2000 | Imperial Dragon (Field-Tested)",
  "id": 39206
  },
  {
  "productName": "StatTrak™ P2000 | Imperial Dragon (Minimal Wear)",
  "id": 39207
  },
  {
  "productName": "StatTrak™ P2000 | Ivory (Factory New)",
  "id": 39210
  },
  {
  "productName": "StatTrak™ P2000 | Ivory (Field-Tested)",
  "id": 39211
  },
  {
  "productName": "StatTrak™ P2000 | Ivory (Minimal Wear)",
  "id": 39212
  },
  {
  "productName": "StatTrak™ P2000 | Ocean Foam (Factory New)",
  "id": 39214
  },
  {
  "productName": "StatTrak™ P2000 | Ocean Foam (Minimal Wear)",
  "id": 39215
  },
  {
  "productName": "StatTrak™ P2000 | Oceanic (Factory New)",
  "id": 39217
  },
  {
  "productName": "StatTrak™ P2000 | Oceanic (Field-Tested)",
  "id": 39218
  },
  {
  "productName": "StatTrak™ P2000 | Oceanic (Minimal Wear)",
  "id": 39219
  },
  {
  "productName": "StatTrak™ P2000 | Pulse (Field-Tested)",
  "id": 39222
  },
  {
  "productName": "StatTrak™ P2000 | Pulse (Minimal Wear)",
  "id": 39223
  },
  {
  "productName": "StatTrak™ P2000 | Red FragCam (Factory New)",
  "id": 39226
  },
  {
  "productName": "StatTrak™ P2000 | Red FragCam (Field-Tested)",
  "id": 39227
  },
  {
  "productName": "StatTrak™ P2000 | Red FragCam (Minimal Wear)",
  "id": 39228
  },
  {
  "productName": "StatTrak™ P2000 | Turf (Factory New)",
  "id": 39231
  },
  {
  "productName": "StatTrak™ P2000 | Turf (Field-Tested)",
  "id": 39232
  },
  {
  "productName": "StatTrak™ P2000 | Turf (Minimal Wear)",
  "id": 39233
  },
  {
  "productName": "StatTrak™ P2000 | Woodsman (Factory New)",
  "id": 39236
  },
  {
  "productName": "StatTrak™ P2000 | Woodsman (Field-Tested)",
  "id": 39237
  },
  {
  "productName": "StatTrak™ P2000 | Woodsman (Minimal Wear)",
  "id": 39238
  },
  {
  "productName": "StatTrak™ P250 | Asiimov (Field-Tested)",
  "id": 39241
  },
  {
  "productName": "StatTrak™ P250 | Asiimov (Minimal Wear)",
  "id": 39242
  },
  {
  "productName": "StatTrak™ P250 | Cartel (Factory New)",
  "id": 39245
  },
  {
  "productName": "StatTrak™ P250 | Cartel (Field-Tested)",
  "id": 39246
  },
  {
  "productName": "StatTrak™ P250 | Cartel (Minimal Wear)",
  "id": 39247
  },
  {
  "productName": "StatTrak™ P250 | Hive (Factory New)",
  "id": 39249
  },
  {
  "productName": "StatTrak™ P250 | Hive (Field-Tested)",
  "id": 39250
  },
  {
  "productName": "StatTrak™ P250 | Hive (Minimal Wear)",
  "id": 39251
  },
  {
  "productName": "StatTrak™ P250 | Iron Clad (Factory New)",
  "id": 39253
  },
  {
  "productName": "StatTrak™ P250 | Iron Clad (Field-Tested)",
  "id": 39254
  },
  {
  "productName": "StatTrak™ P250 | Iron Clad (Minimal Wear)",
  "id": 39255
  },
  {
  "productName": "StatTrak™ P250 | Mehndi (Factory New)",
  "id": 39258
  },
  {
  "productName": "StatTrak™ P250 | Mehndi (Field-Tested)",
  "id": 39259
  },
  {
  "productName": "StatTrak™ P250 | Mehndi (Minimal Wear)",
  "id": 39260
  },
  {
  "productName": "StatTrak™ P250 | Muertos (Factory New)",
  "id": 39263
  },
  {
  "productName": "StatTrak™ P250 | Muertos (Field-Tested)",
  "id": 39264
  },
  {
  "productName": "StatTrak™ P250 | Muertos (Minimal Wear)",
  "id": 39265
  },
  {
  "productName": "StatTrak™ P250 | Red Rock (Factory New)",
  "id": 39268
  },
  {
  "productName": "StatTrak™ P250 | Red Rock (Minimal Wear)",
  "id": 39269
  },
  {
  "productName": "StatTrak™ P250 | Ripple (Factory New)",
  "id": 39272
  },
  {
  "productName": "StatTrak™ P250 | Ripple (Field-Tested)",
  "id": 39273
  },
  {
  "productName": "StatTrak™ P250 | Ripple (Minimal Wear)",
  "id": 39274
  },
  {
  "productName": "StatTrak™ P250 | See Ya Later (Factory New)",
  "id": 39277
  },
  {
  "productName": "StatTrak™ P250 | See Ya Later (Field-Tested)",
  "id": 39278
  },
  {
  "productName": "StatTrak™ P250 | See Ya Later (Minimal Wear)",
  "id": 39279
  },
  {
  "productName": "StatTrak™ P250 | Splash (Factory New)",
  "id": 39281
  },
  {
  "productName": "StatTrak™ P250 | Splash (Field-Tested)",
  "id": 39282
  },
  {
  "productName": "StatTrak™ P250 | Splash (Minimal Wear)",
  "id": 39283
  },
  {
  "productName": "StatTrak™ P250 | Steel Disruption (Factory New)",
  "id": 39284
  },
  {
  "productName": "StatTrak™ P250 | Steel Disruption (Field-Tested)",
  "id": 39285
  },
  {
  "productName": "StatTrak™ P250 | Steel Disruption (Minimal Wear)",
  "id": 39286
  },
  {
  "productName": "StatTrak™ P250 | Supernova (Factory New)",
  "id": 39287
  },
  {
  "productName": "StatTrak™ P250 | Supernova (Field-Tested)",
  "id": 39288
  },
  {
  "productName": "StatTrak™ P250 | Supernova (Minimal Wear)",
  "id": 39289
  },
  {
  "productName": "StatTrak™ P250 | Undertow (Factory New)",
  "id": 39291
  },
  {
  "productName": "StatTrak™ P250 | Undertow (Field-Tested)",
  "id": 39292
  },
  {
  "productName": "StatTrak™ P250 | Undertow (Minimal Wear)",
  "id": 39293
  },
  {
  "productName": "StatTrak™ P250 | Valence (Factory New)",
  "id": 39295
  },
  {
  "productName": "StatTrak™ P250 | Valence (Field-Tested)",
  "id": 39296
  },
  {
  "productName": "StatTrak™ P250 | Valence (Minimal Wear)",
  "id": 39297
  },
  {
  "productName": "StatTrak™ P250 | Wingshot (Factory New)",
  "id": 39300
  },
  {
  "productName": "StatTrak™ P250 | Wingshot (Field-Tested)",
  "id": 39301
  },
  {
  "productName": "StatTrak™ P250 | Wingshot (Minimal Wear)",
  "id": 39302
  },
  {
  "productName": "StatTrak™ P90 | Asiimov (Factory New)",
  "id": 39305
  },
  {
  "productName": "StatTrak™ P90 | Asiimov (Field-Tested)",
  "id": 39306
  },
  {
  "productName": "StatTrak™ P90 | Asiimov (Minimal Wear)",
  "id": 39307
  },
  {
  "productName": "StatTrak™ P90 | Blind Spot (Factory New)",
  "id": 39310
  },
  {
  "productName": "StatTrak™ P90 | Blind Spot (Field-Tested)",
  "id": 39311
  },
  {
  "productName": "StatTrak™ P90 | Blind Spot (Minimal Wear)",
  "id": 39312
  },
  {
  "productName": "StatTrak™ P90 | Chopper (Factory New)",
  "id": 39315
  },
  {
  "productName": "StatTrak™ P90 | Chopper (Field-Tested)",
  "id": 39316
  },
  {
  "productName": "StatTrak™ P90 | Chopper (Minimal Wear)",
  "id": 39317
  },
  {
  "productName": "StatTrak™ P90 | Cold Blooded (Factory New)",
  "id": 39319
  },
  {
  "productName": "StatTrak™ P90 | Cold Blooded (Minimal Wear)",
  "id": 39320
  },
  {
  "productName": "StatTrak™ P90 | Death by Kitty (Field-Tested)",
  "id": 39326
  },
  {
  "productName": "StatTrak™ P90 | Death by Kitty (Minimal Wear)",
  "id": 39327
  },
  {
  "productName": "StatTrak™ P90 | Death Grip (Factory New)",
  "id": 39322
  },
  {
  "productName": "StatTrak™ P90 | Death Grip (Field-Tested)",
  "id": 39323
  },
  {
  "productName": "StatTrak™ P90 | Death Grip (Minimal Wear)",
  "id": 39324
  },
  {
  "productName": "StatTrak™ P90 | Desert Warfare (Factory New)",
  "id": 39329
  },
  {
  "productName": "StatTrak™ P90 | Desert Warfare (Field-Tested)",
  "id": 39330
  },
  {
  "productName": "StatTrak™ P90 | Desert Warfare (Minimal Wear)",
  "id": 39331
  },
  {
  "productName": "StatTrak™ P90 | Elite Build (Factory New)",
  "id": 39334
  },
  {
  "productName": "StatTrak™ P90 | Elite Build (Field-Tested)",
  "id": 39335
  },
  {
  "productName": "StatTrak™ P90 | Elite Build (Minimal Wear)",
  "id": 39336
  },
  {
  "productName": "StatTrak™ P90 | Emerald Dragon (Factory New)",
  "id": 39339
  },
  {
  "productName": "StatTrak™ P90 | Emerald Dragon (Field-Tested)",
  "id": 39340
  },
  {
  "productName": "StatTrak™ P90 | Emerald Dragon (Minimal Wear)",
  "id": 39341
  },
  {
  "productName": "StatTrak™ P90 | Grim (Factory New)",
  "id": 39344
  },
  {
  "productName": "StatTrak™ P90 | Grim (Field-Tested)",
  "id": 39345
  },
  {
  "productName": "StatTrak™ P90 | Grim (Minimal Wear)",
  "id": 39346
  },
  {
  "productName": "StatTrak™ P90 | Module (Factory New)",
  "id": 39348
  },
  {
  "productName": "StatTrak™ P90 | Module (Field-Tested)",
  "id": 39349
  },
  {
  "productName": "StatTrak™ P90 | Module (Minimal Wear)",
  "id": 39350
  },
  {
  "productName": "StatTrak™ P90 | Shallow Grave (Factory New)",
  "id": 39352
  },
  {
  "productName": "StatTrak™ P90 | Shallow Grave (Field-Tested)",
  "id": 39353
  },
  {
  "productName": "StatTrak™ P90 | Shallow Grave (Minimal Wear)",
  "id": 39354
  },
  {
  "productName": "StatTrak™ P90 | Shapewood (Factory New)",
  "id": 39357
  },
  {
  "productName": "StatTrak™ P90 | Shapewood (Field-Tested)",
  "id": 39358
  },
  {
  "productName": "StatTrak™ P90 | Shapewood (Minimal Wear)",
  "id": 39359
  },
  {
  "productName": "StatTrak™ P90 | Trigon (Field-Tested)",
  "id": 39362
  },
  {
  "productName": "StatTrak™ P90 | Trigon (Minimal Wear)",
  "id": 39363
  },
  {
  "productName": "StatTrak™ P90 | Virus (Factory New)",
  "id": 39366
  },
  {
  "productName": "StatTrak™ P90 | Virus (Field-Tested)",
  "id": 39367
  },
  {
  "productName": "StatTrak™ P90 | Virus (Minimal Wear)",
  "id": 39368
  },
  {
  "productName": "StatTrak™ PP-Bizon | Antique (Factory New)",
  "id": 39371
  },
  {
  "productName": "StatTrak™ PP-Bizon | Antique (Field-Tested)",
  "id": 39372
  },
  {
  "productName": "StatTrak™ PP-Bizon | Antique (Minimal Wear)",
  "id": 39373
  },
  {
  "productName": "StatTrak™ PP-Bizon | Blue Streak (Factory New)",
  "id": 39376
  },
  {
  "productName": "StatTrak™ PP-Bizon | Blue Streak (Field-Tested)",
  "id": 39377
  },
  {
  "productName": "StatTrak™ PP-Bizon | Blue Streak (Minimal Wear)",
  "id": 39378
  },
  {
  "productName": "StatTrak™ PP-Bizon | Cobalt Halftone (Factory New)",
  "id": 39380
  },
  {
  "productName": "StatTrak™ PP-Bizon | Cobalt Halftone (Field-Tested)",
  "id": 39381
  },
  {
  "productName": "StatTrak™ PP-Bizon | Cobalt Halftone (Minimal Wear)",
  "id": 39382
  },
  {
  "productName": "StatTrak™ PP-Bizon | Fuel Rod (Factory New)",
  "id": 39385
  },
  {
  "productName": "StatTrak™ PP-Bizon | Fuel Rod (Field-Tested)",
  "id": 39386
  },
  {
  "productName": "StatTrak™ PP-Bizon | Fuel Rod (Minimal Wear)",
  "id": 39387
  },
  {
  "productName": "StatTrak™ PP-Bizon | Harvester (Factory New)",
  "id": 39390
  },
  {
  "productName": "StatTrak™ PP-Bizon | Harvester (Field-Tested)",
  "id": 39391
  },
  {
  "productName": "StatTrak™ PP-Bizon | Harvester (Minimal Wear)",
  "id": 39392
  },
  {
  "productName": "StatTrak™ PP-Bizon | High Roller (Factory New)",
  "id": 39395
  },
  {
  "productName": "StatTrak™ PP-Bizon | High Roller (Field-Tested)",
  "id": 39396
  },
  {
  "productName": "StatTrak™ PP-Bizon | High Roller (Minimal Wear)",
  "id": 39397
  },
  {
  "productName": "StatTrak™ PP-Bizon | Judgement of Anubis (Factory New)",
  "id": 39400
  },
  {
  "productName": "StatTrak™ PP-Bizon | Judgement of Anubis (Field-Tested)",
  "id": 39401
  },
  {
  "productName": "StatTrak™ PP-Bizon | Judgement of Anubis (Minimal Wear)",
  "id": 39402
  },
  {
  "productName": "StatTrak™ PP-Bizon | Jungle Slipstream (Factory New)",
  "id": 39405
  },
  {
  "productName": "StatTrak™ PP-Bizon | Jungle Slipstream (Field-Tested)",
  "id": 39406
  },
  {
  "productName": "StatTrak™ PP-Bizon | Jungle Slipstream (Minimal Wear)",
  "id": 39407
  },
  {
  "productName": "StatTrak™ PP-Bizon | Osiris (Factory New)",
  "id": 39410
  },
  {
  "productName": "StatTrak™ PP-Bizon | Osiris (Field-Tested)",
  "id": 39411
  },
  {
  "productName": "StatTrak™ PP-Bizon | Osiris (Minimal Wear)",
  "id": 39412
  },
  {
  "productName": "StatTrak™ PP-Bizon | Photic Zone (Factory New)",
  "id": 39415
  },
  {
  "productName": "StatTrak™ PP-Bizon | Photic Zone (Field-Tested)",
  "id": 39416
  },
  {
  "productName": "StatTrak™ PP-Bizon | Photic Zone (Minimal Wear)",
  "id": 39417
  },
  {
  "productName": "StatTrak™ PP-Bizon | Water Sigil (Factory New)",
  "id": 39420
  },
  {
  "productName": "StatTrak™ PP-Bizon | Water Sigil (Field-Tested)",
  "id": 39421
  },
  {
  "productName": "StatTrak™ PP-Bizon | Water Sigil (Minimal Wear)",
  "id": 39422
  },
  {
  "productName": "StatTrak™ R8 Revolver | Crimson Web (Factory New)",
  "id": 39425
  },
  {
  "productName": "StatTrak™ R8 Revolver | Crimson Web (Field-Tested)",
  "id": 39426
  },
  {
  "productName": "StatTrak™ R8 Revolver | Crimson Web (Minimal Wear)",
  "id": 39427
  },
  {
  "productName": "StatTrak™ R8 Revolver | Fade (Factory New)",
  "id": 39429
  },
  {
  "productName": "StatTrak™ R8 Revolver | Fade (Field-Tested)",
  "id": 39430
  },
  {
  "productName": "StatTrak™ R8 Revolver | Fade (Minimal Wear)",
  "id": 39431
  },
  {
  "productName": "StatTrak™ R8 Revolver | Llama Cannon (Factory New)",
  "id": 39434
  },
  {
  "productName": "StatTrak™ R8 Revolver | Llama Cannon (Field-Tested)",
  "id": 39435
  },
  {
  "productName": "StatTrak™ R8 Revolver | Llama Cannon (Minimal Wear)",
  "id": 39436
  },
  {
  "productName": "StatTrak™ R8 Revolver | Reboot (Factory New)",
  "id": 39439
  },
  {
  "productName": "StatTrak™ R8 Revolver | Reboot (Field-Tested)",
  "id": 39440
  },
  {
  "productName": "StatTrak™ R8 Revolver | Reboot (Minimal Wear)",
  "id": 39441
  },
  {
  "productName": "StatTrak™ Sawed-Off | Highwayman (Factory New)",
  "id": 39575
  },
  {
  "productName": "StatTrak™ Sawed-Off | Highwayman (Field-Tested)",
  "id": 39576
  },
  {
  "productName": "StatTrak™ Sawed-Off | Highwayman (Minimal Wear)",
  "id": 39577
  },
  {
  "productName": "StatTrak™ Sawed-Off | Limelight (Factory New)",
  "id": 39580
  },
  {
  "productName": "StatTrak™ Sawed-Off | Limelight (Field-Tested)",
  "id": 39581
  },
  {
  "productName": "StatTrak™ Sawed-Off | Limelight (Minimal Wear)",
  "id": 39582
  },
  {
  "productName": "StatTrak™ Sawed-Off | Morris (Factory New)",
  "id": 39585
  },
  {
  "productName": "StatTrak™ Sawed-Off | Morris (Field-Tested)",
  "id": 39586
  },
  {
  "productName": "StatTrak™ Sawed-Off | Morris (Minimal Wear)",
  "id": 39587
  },
  {
  "productName": "StatTrak™ Sawed-Off | Orange DDPAT (Factory New)",
  "id": 39590
  },
  {
  "productName": "StatTrak™ Sawed-Off | Orange DDPAT (Field-Tested)",
  "id": 39591
  },
  {
  "productName": "StatTrak™ Sawed-Off | Orange DDPAT (Minimal Wear)",
  "id": 39592
  },
  {
  "productName": "StatTrak™ Sawed-Off | Origami (Factory New)",
  "id": 39594
  },
  {
  "productName": "StatTrak™ Sawed-Off | Origami (Field-Tested)",
  "id": 39595
  },
  {
  "productName": "StatTrak™ Sawed-Off | Origami (Minimal Wear)",
  "id": 39596
  },
  {
  "productName": "StatTrak™ Sawed-Off | Serenity (Factory New)",
  "id": 39599
  },
  {
  "productName": "StatTrak™ Sawed-Off | Serenity (Field-Tested)",
  "id": 39600
  },
  {
  "productName": "StatTrak™ Sawed-Off | Serenity (Minimal Wear)",
  "id": 39601
  },
  {
  "productName": "StatTrak™ Sawed-Off | The Kraken (Factory New)",
  "id": 39603
  },
  {
  "productName": "StatTrak™ Sawed-Off | The Kraken (Field-Tested)",
  "id": 39604
  },
  {
  "productName": "StatTrak™ Sawed-Off | The Kraken (Minimal Wear)",
  "id": 39605
  },
  {
  "productName": "StatTrak™ Sawed-Off | Wasteland Princess (Factory New)",
  "id": 39608
  },
  {
  "productName": "StatTrak™ Sawed-Off | Wasteland Princess (Field-Tested)",
  "id": 39609
  },
  {
  "productName": "StatTrak™ Sawed-Off | Wasteland Princess (Minimal Wear)",
  "id": 39610
  },
  {
  "productName": "StatTrak™ Sawed-Off | Yorick (Factory New)",
  "id": 39613
  },
  {
  "productName": "StatTrak™ Sawed-Off | Yorick (Field-Tested)",
  "id": 39614
  },
  {
  "productName": "StatTrak™ Sawed-Off | Yorick (Minimal Wear)",
  "id": 39615
  },
  {
  "productName": "StatTrak™ Sawed-Off | Zander (Factory New)",
  "id": 39618
  },
  {
  "productName": "StatTrak™ Sawed-Off | Zander (Field-Tested)",
  "id": 39619
  },
  {
  "productName": "StatTrak™ Sawed-Off | Zander (Minimal Wear)",
  "id": 39620
  },
  {
  "productName": "StatTrak™ SCAR-20 | Bloodsport (Factory New)",
  "id": 39444
  },
  {
  "productName": "StatTrak™ SCAR-20 | Bloodsport (Field-Tested)",
  "id": 39445
  },
  {
  "productName": "StatTrak™ SCAR-20 | Bloodsport (Minimal Wear)",
  "id": 39446
  },
  {
  "productName": "StatTrak™ SCAR-20 | Blueprint (Factory New)",
  "id": 39449
  },
  {
  "productName": "StatTrak™ SCAR-20 | Blueprint (Field-Tested)",
  "id": 39450
  },
  {
  "productName": "StatTrak™ SCAR-20 | Blueprint (Minimal Wear)",
  "id": 39451
  },
  {
  "productName": "StatTrak™ SCAR-20 | Cardiac (Factory New)",
  "id": 39454
  },
  {
  "productName": "StatTrak™ SCAR-20 | Cardiac (Field-Tested)",
  "id": 39455
  },
  {
  "productName": "StatTrak™ SCAR-20 | Cardiac (Minimal Wear)",
  "id": 39456
  },
  {
  "productName": "StatTrak™ SCAR-20 | Crimson Web (Factory New)",
  "id": 39459
  },
  {
  "productName": "StatTrak™ SCAR-20 | Crimson Web (Field-Tested)",
  "id": 39460
  },
  {
  "productName": "StatTrak™ SCAR-20 | Crimson Web (Minimal Wear)",
  "id": 39461
  },
  {
  "productName": "StatTrak™ SCAR-20 | Cyrex (Factory New)",
  "id": 39464
  },
  {
  "productName": "StatTrak™ SCAR-20 | Cyrex (Field-Tested)",
  "id": 39465
  },
  {
  "productName": "StatTrak™ SCAR-20 | Cyrex (Minimal Wear)",
  "id": 39466
  },
  {
  "productName": "StatTrak™ SCAR-20 | Green Marine (Factory New)",
  "id": 39469
  },
  {
  "productName": "StatTrak™ SCAR-20 | Green Marine (Field-Tested)",
  "id": 39470
  },
  {
  "productName": "StatTrak™ SCAR-20 | Green Marine (Minimal Wear)",
  "id": 39471
  },
  {
  "productName": "StatTrak™ SCAR-20 | Grotto (Factory New)",
  "id": 39474
  },
  {
  "productName": "StatTrak™ SCAR-20 | Grotto (Field-Tested)",
  "id": 39475
  },
  {
  "productName": "StatTrak™ SCAR-20 | Grotto (Minimal Wear)",
  "id": 39476
  },
  {
  "productName": "StatTrak™ SCAR-20 | Jungle Slipstream (Factory New)",
  "id": 39479
  },
  {
  "productName": "StatTrak™ SCAR-20 | Jungle Slipstream (Field-Tested)",
  "id": 39480
  },
  {
  "productName": "StatTrak™ SCAR-20 | Jungle Slipstream (Minimal Wear)",
  "id": 39481
  },
  {
  "productName": "StatTrak™ SCAR-20 | Outbreak (Factory New)",
  "id": 39484
  },
  {
  "productName": "StatTrak™ SCAR-20 | Outbreak (Field-Tested)",
  "id": 39485
  },
  {
  "productName": "StatTrak™ SCAR-20 | Outbreak (Minimal Wear)",
  "id": 39486
  },
  {
  "productName": "StatTrak™ SCAR-20 | Powercore (Factory New)",
  "id": 39489
  },
  {
  "productName": "StatTrak™ SCAR-20 | Powercore (Field-Tested)",
  "id": 39490
  },
  {
  "productName": "StatTrak™ SCAR-20 | Powercore (Minimal Wear)",
  "id": 39491
  },
  {
  "productName": "StatTrak™ SG 553 | Aerial (Factory New)",
  "id": 39494
  },
  {
  "productName": "StatTrak™ SG 553 | Aerial (Field-Tested)",
  "id": 39495
  },
  {
  "productName": "StatTrak™ SG 553 | Aerial (Minimal Wear)",
  "id": 39496
  },
  {
  "productName": "StatTrak™ SG 553 | Atlas (Factory New)",
  "id": 39499
  },
  {
  "productName": "StatTrak™ SG 553 | Atlas (Field-Tested)",
  "id": 39500
  },
  {
  "productName": "StatTrak™ SG 553 | Atlas (Minimal Wear)",
  "id": 39501
  },
  {
  "productName": "StatTrak™ SG 553 | Cyrex (Factory New)",
  "id": 39504
  },
  {
  "productName": "StatTrak™ SG 553 | Cyrex (Field-Tested)",
  "id": 39505
  },
  {
  "productName": "StatTrak™ SG 553 | Phantom (Factory New)",
  "id": 39508
  },
  {
  "productName": "StatTrak™ SG 553 | Phantom (Field-Tested)",
  "id": 39509
  },
  {
  "productName": "StatTrak™ SG 553 | Phantom (Minimal Wear)",
  "id": 39510
  },
  {
  "productName": "StatTrak™ SG 553 | Pulse (Field-Tested)",
  "id": 39513
  },
  {
  "productName": "StatTrak™ SG 553 | Pulse (Minimal Wear)",
  "id": 39514
  },
  {
  "productName": "StatTrak™ SG 553 | Tiger Moth (Factory New)",
  "id": 39517
  },
  {
  "productName": "StatTrak™ SG 553 | Tiger Moth (Field-Tested)",
  "id": 39518
  },
  {
  "productName": "StatTrak™ SG 553 | Tiger Moth (Minimal Wear)",
  "id": 39519
  },
  {
  "productName": "StatTrak™ SG 553 | Triarch (Factory New)",
  "id": 39522
  },
  {
  "productName": "StatTrak™ SG 553 | Triarch (Field-Tested)",
  "id": 39523
  },
  {
  "productName": "StatTrak™ SG 553 | Triarch (Minimal Wear)",
  "id": 39524
  },
  {
  "productName": "StatTrak™ SG 553 | Ultraviolet (Factory New)",
  "id": 39527
  },
  {
  "productName": "StatTrak™ SG 553 | Ultraviolet (Field-Tested)",
  "id": 39528
  },
  {
  "productName": "StatTrak™ SG 553 | Ultraviolet (Minimal Wear)",
  "id": 39529
  },
  {
  "productName": "StatTrak™ SG 553 | Wave Spray (Factory New)",
  "id": 39532
  },
  {
  "productName": "StatTrak™ SG 553 | Wave Spray (Minimal Wear)",
  "id": 39533
  },
  {
  "productName": "StatTrak™ SSG 08 | Abyss (Factory New)",
  "id": 39536
  },
  {
  "productName": "StatTrak™ SSG 08 | Abyss (Field-Tested)",
  "id": 39537
  },
  {
  "productName": "StatTrak™ SSG 08 | Abyss (Minimal Wear)",
  "id": 39538
  },
  {
  "productName": "StatTrak™ SSG 08 | Big Iron (Factory New)",
  "id": 39541
  },
  {
  "productName": "StatTrak™ SSG 08 | Big Iron (Field-Tested)",
  "id": 39542
  },
  {
  "productName": "StatTrak™ SSG 08 | Big Iron (Minimal Wear)",
  "id": 39543
  },
  {
  "productName": "StatTrak™ SSG 08 | Blood in the Water (Factory New)",
  "id": 39545
  },
  {
  "productName": "StatTrak™ SSG 08 | Blood in the Water (Field-Tested)",
  "id": 39546
  },
  {
  "productName": "StatTrak™ SSG 08 | Blood in the Water (Minimal Wear)",
  "id": 39547
  },
  {
  "productName": "StatTrak™ SSG 08 | Dark Water (Field-Tested)",
  "id": 39548
  },
  {
  "productName": "StatTrak™ SSG 08 | Dark Water (Minimal Wear)",
  "id": 39549
  },
  {
  "productName": "StatTrak™ SSG 08 | Death's Head (Factory New)",
  "id": 39551
  },
  {
  "productName": "StatTrak™ SSG 08 | Death's Head (Field-Tested)",
  "id": 39552
  },
  {
  "productName": "StatTrak™ SSG 08 | Death's Head (Minimal Wear)",
  "id": 39553
  },
  {
  "productName": "StatTrak™ SSG 08 | Dragonfire (Factory New)",
  "id": 39556
  },
  {
  "productName": "StatTrak™ SSG 08 | Dragonfire (Field-Tested)",
  "id": 39557
  },
  {
  "productName": "StatTrak™ SSG 08 | Ghost Crusader (Factory New)",
  "id": 39560
  },
  {
  "productName": "StatTrak™ SSG 08 | Ghost Crusader (Field-Tested)",
  "id": 39561
  },
  {
  "productName": "StatTrak™ SSG 08 | Ghost Crusader (Minimal Wear)",
  "id": 39562
  },
  {
  "productName": "StatTrak™ SSG 08 | Necropos (Factory New)",
  "id": 39565
  },
  {
  "productName": "StatTrak™ SSG 08 | Necropos (Field-Tested)",
  "id": 39566
  },
  {
  "productName": "StatTrak™ SSG 08 | Necropos (Minimal Wear)",
  "id": 39567
  },
  {
  "productName": "StatTrak™ SSG 08 | Slashed (Field-Tested)",
  "id": 39570
  },
  {
  "productName": "StatTrak™ Tec-9 | Avalanche (Factory New)",
  "id": 39624
  },
  {
  "productName": "StatTrak™ Tec-9 | Avalanche (Field-Tested)",
  "id": 39625
  },
  {
  "productName": "StatTrak™ Tec-9 | Avalanche (Minimal Wear)",
  "id": 39626
  },
  {
  "productName": "StatTrak™ Tec-9 | Blue Titanium (Factory New)",
  "id": 39628
  },
  {
  "productName": "StatTrak™ Tec-9 | Cracked Opal (Factory New)",
  "id": 39630
  },
  {
  "productName": "StatTrak™ Tec-9 | Cracked Opal (Field-Tested)",
  "id": 39631
  },
  {
  "productName": "StatTrak™ Tec-9 | Cracked Opal (Minimal Wear)",
  "id": 39632
  },
  {
  "productName": "StatTrak™ Tec-9 | Cut Out (Factory New)",
  "id": 39635
  },
  {
  "productName": "StatTrak™ Tec-9 | Cut Out (Field-Tested)",
  "id": 39636
  },
  {
  "productName": "StatTrak™ Tec-9 | Cut Out (Minimal Wear)",
  "id": 39637
  },
  {
  "productName": "StatTrak™ Tec-9 | Fuel Injector (Factory New)",
  "id": 39640
  },
  {
  "productName": "StatTrak™ Tec-9 | Fuel Injector (Field-Tested)",
  "id": 39641
  },
  {
  "productName": "StatTrak™ Tec-9 | Fuel Injector (Minimal Wear)",
  "id": 39642
  },
  {
  "productName": "StatTrak™ Tec-9 | Ice Cap (Factory New)",
  "id": 39645
  },
  {
  "productName": "StatTrak™ Tec-9 | Ice Cap (Field-Tested)",
  "id": 39646
  },
  {
  "productName": "StatTrak™ Tec-9 | Ice Cap (Minimal Wear)",
  "id": 39647
  },
  {
  "productName": "StatTrak™ Tec-9 | Isaac (Factory New)",
  "id": 39650
  },
  {
  "productName": "StatTrak™ Tec-9 | Isaac (Field-Tested)",
  "id": 39651
  },
  {
  "productName": "StatTrak™ Tec-9 | Isaac (Minimal Wear)",
  "id": 39652
  },
  {
  "productName": "StatTrak™ Tec-9 | Jambiya (Factory New)",
  "id": 39655
  },
  {
  "productName": "StatTrak™ Tec-9 | Jambiya (Field-Tested)",
  "id": 39656
  },
  {
  "productName": "StatTrak™ Tec-9 | Jambiya (Minimal Wear)",
  "id": 39657
  },
  {
  "productName": "StatTrak™ Tec-9 | Re-Entry (Factory New)",
  "id": 39659
  },
  {
  "productName": "StatTrak™ Tec-9 | Re-Entry (Field-Tested)",
  "id": 39660
  },
  {
  "productName": "StatTrak™ Tec-9 | Re-Entry (Minimal Wear)",
  "id": 39661
  },
  {
  "productName": "StatTrak™ Tec-9 | Sandstorm (Field-Tested)",
  "id": 39664
  },
  {
  "productName": "StatTrak™ Tec-9 | Sandstorm (Minimal Wear)",
  "id": 39665
  },
  {
  "productName": "StatTrak™ Tec-9 | Titanium Bit (Factory New)",
  "id": 39667
  },
  {
  "productName": "StatTrak™ Tec-9 | Titanium Bit (Field-Tested)",
  "id": 39668
  },
  {
  "productName": "StatTrak™ Tec-9 | Titanium Bit (Minimal Wear)",
  "id": 39669
  },
  {
  "productName": "StatTrak™ UMP-45 | Bone Pile (Factory New)",
  "id": 39670
  },
  {
  "productName": "StatTrak™ UMP-45 | Bone Pile (Field-Tested)",
  "id": 39671
  },
  {
  "productName": "StatTrak™ UMP-45 | Bone Pile (Minimal Wear)",
  "id": 39672
  },
  {
  "productName": "StatTrak™ UMP-45 | Briefing (Factory New)",
  "id": 39674
  },
  {
  "productName": "StatTrak™ UMP-45 | Briefing (Field-Tested)",
  "id": 39675
  },
  {
  "productName": "StatTrak™ UMP-45 | Briefing (Minimal Wear)",
  "id": 39676
  },
  {
  "productName": "StatTrak™ UMP-45 | Corporal (Factory New)",
  "id": 39679
  },
  {
  "productName": "StatTrak™ UMP-45 | Corporal (Field-Tested)",
  "id": 39680
  },
  {
  "productName": "StatTrak™ UMP-45 | Corporal (Minimal Wear)",
  "id": 39681
  },
  {
  "productName": "StatTrak™ UMP-45 | Delusion (Factory New)",
  "id": 39683
  },
  {
  "productName": "StatTrak™ UMP-45 | Delusion (Field-Tested)",
  "id": 39684
  },
  {
  "productName": "StatTrak™ UMP-45 | Delusion (Minimal Wear)",
  "id": 39685
  },
  {
  "productName": "StatTrak™ UMP-45 | Exposure (Factory New)",
  "id": 39687
  },
  {
  "productName": "StatTrak™ UMP-45 | Exposure (Field-Tested)",
  "id": 39688
  },
  {
  "productName": "StatTrak™ UMP-45 | Exposure (Minimal Wear)",
  "id": 39689
  },
  {
  "productName": "StatTrak™ UMP-45 | Grand Prix (Field-Tested)",
  "id": 39691
  },
  {
  "productName": "StatTrak™ UMP-45 | Labyrinth (Factory New)",
  "id": 39692
  },
  {
  "productName": "StatTrak™ UMP-45 | Labyrinth (Field-Tested)",
  "id": 39693
  },
  {
  "productName": "StatTrak™ UMP-45 | Labyrinth (Minimal Wear)",
  "id": 39694
  },
  {
  "productName": "StatTrak™ UMP-45 | Metal Flowers (Factory New)",
  "id": 39697
  },
  {
  "productName": "StatTrak™ UMP-45 | Metal Flowers (Field-Tested)",
  "id": 39698
  },
  {
  "productName": "StatTrak™ UMP-45 | Metal Flowers (Minimal Wear)",
  "id": 39699
  },
  {
  "productName": "StatTrak™ UMP-45 | Primal Saber (Factory New)",
  "id": 39702
  },
  {
  "productName": "StatTrak™ UMP-45 | Primal Saber (Field-Tested)",
  "id": 39703
  },
  {
  "productName": "StatTrak™ UMP-45 | Primal Saber (Minimal Wear)",
  "id": 39704
  },
  {
  "productName": "StatTrak™ UMP-45 | Riot (Factory New)",
  "id": 39707
  },
  {
  "productName": "StatTrak™ UMP-45 | Riot (Field-Tested)",
  "id": 39708
  },
  {
  "productName": "StatTrak™ UMP-45 | Riot (Minimal Wear)",
  "id": 39709
  },
  {
  "productName": "StatTrak™ UMP-45 | Scaffold (Factory New)",
  "id": 39712
  },
  {
  "productName": "StatTrak™ UMP-45 | Scaffold (Field-Tested)",
  "id": 39713
  },
  {
  "productName": "StatTrak™ UMP-45 | Scaffold (Minimal Wear)",
  "id": 39714
  },
  {
  "productName": "StatTrak™ USP-S | Blood Tiger (Factory New)",
  "id": 39716
  },
  {
  "productName": "StatTrak™ USP-S | Blood Tiger (Field-Tested)",
  "id": 39717
  },
  {
  "productName": "StatTrak™ USP-S | Blood Tiger (Minimal Wear)",
  "id": 39718
  },
  {
  "productName": "StatTrak™ USP-S | Blueprint (Factory New)",
  "id": 39720
  },
  {
  "productName": "StatTrak™ USP-S | Blueprint (Field-Tested)",
  "id": 39721
  },
  {
  "productName": "StatTrak™ USP-S | Blueprint (Minimal Wear)",
  "id": 39722
  },
  {
  "productName": "StatTrak™ USP-S | Caiman (Factory New)",
  "id": 39724
  },
  {
  "productName": "StatTrak™ USP-S | Caiman (Field-Tested)",
  "id": 39725
  },
  {
  "productName": "StatTrak™ USP-S | Caiman (Minimal Wear)",
  "id": 39726
  },
  {
  "productName": "StatTrak™ USP-S | Cyrex (Factory New)",
  "id": 39729
  },
  {
  "productName": "StatTrak™ USP-S | Cyrex (Field-Tested)",
  "id": 39730
  },
  {
  "productName": "StatTrak™ USP-S | Cyrex (Minimal Wear)",
  "id": 39731
  },
  {
  "productName": "StatTrak™ USP-S | Dark Water (Field-Tested)",
  "id": 39733
  },
  {
  "productName": "StatTrak™ USP-S | Dark Water (Minimal Wear)",
  "id": 39734
  },
  {
  "productName": "StatTrak™ USP-S | Guardian (Factory New)",
  "id": 39735
  },
  {
  "productName": "StatTrak™ USP-S | Guardian (Field-Tested)",
  "id": 39736
  },
  {
  "productName": "StatTrak™ USP-S | Guardian (Minimal Wear)",
  "id": 39737
  },
  {
  "productName": "StatTrak™ USP-S | Kill Confirmed (Factory New)",
  "id": 39739
  },
  {
  "productName": "StatTrak™ USP-S | Kill Confirmed (Field-Tested)",
  "id": 39740
  },
  {
  "productName": "StatTrak™ USP-S | Kill Confirmed (Minimal Wear)",
  "id": 39741
  },
  {
  "productName": "StatTrak™ USP-S | Lead Conduit (Factory New)",
  "id": 39744
  },
  {
  "productName": "StatTrak™ USP-S | Lead Conduit (Field-Tested)",
  "id": 39745
  },
  {
  "productName": "StatTrak™ USP-S | Lead Conduit (Minimal Wear)",
  "id": 39746
  },
  {
  "productName": "StatTrak™ USP-S | Neo-Noir (Factory New)",
  "id": 39749
  },
  {
  "productName": "StatTrak™ USP-S | Neo-Noir (Field-Tested)",
  "id": 39750
  },
  {
  "productName": "StatTrak™ USP-S | Neo-Noir (Minimal Wear)",
  "id": 39751
  },
  {
  "productName": "StatTrak™ USP-S | Orion (Field-Tested)",
  "id": 39753
  },
  {
  "productName": "StatTrak™ USP-S | Orion (Minimal Wear)",
  "id": 39754
  },
  {
  "productName": "StatTrak™ USP-S | Overgrowth (Factory New)",
  "id": 39757
  },
  {
  "productName": "StatTrak™ USP-S | Overgrowth (Field-Tested)",
  "id": 39758
  },
  {
  "productName": "StatTrak™ USP-S | Overgrowth (Minimal Wear)",
  "id": 39759
  },
  {
  "productName": "StatTrak™ USP-S | Serum (Factory New)",
  "id": 39761
  },
  {
  "productName": "StatTrak™ USP-S | Serum (Field-Tested)",
  "id": 39762
  },
  {
  "productName": "StatTrak™ USP-S | Serum (Minimal Wear)",
  "id": 39763
  },
  {
  "productName": "StatTrak™ USP-S | Stainless (Factory New)",
  "id": 39765
  },
  {
  "productName": "StatTrak™ USP-S | Stainless (Field-Tested)",
  "id": 39766
  },
  {
  "productName": "StatTrak™ USP-S | Stainless (Minimal Wear)",
  "id": 39767
  },
  {
  "productName": "StatTrak™ USP-S | Torque (Factory New)",
  "id": 39770
  },
  {
  "productName": "StatTrak™ USP-S | Torque (Field-Tested)",
  "id": 39771
  },
  {
  "productName": "StatTrak™ USP-S | Torque (Minimal Wear)",
  "id": 39772
  },
  {
  "productName": "StatTrak™ XM1014 | Black Tie (Factory New)",
  "id": 39775
  },
  {
  "productName": "StatTrak™ XM1014 | Black Tie (Field-Tested)",
  "id": 39776
  },
  {
  "productName": "StatTrak™ XM1014 | Black Tie (Minimal Wear)",
  "id": 39777
  },
  {
  "productName": "StatTrak™ XM1014 | Heaven Guard (Factory New)",
  "id": 39780
  },
  {
  "productName": "StatTrak™ XM1014 | Heaven Guard (Field-Tested)",
  "id": 39781
  },
  {
  "productName": "StatTrak™ XM1014 | Heaven Guard (Minimal Wear)",
  "id": 39782
  },
  {
  "productName": "StatTrak™ XM1014 | Quicksilver (Factory New)",
  "id": 39785
  },
  {
  "productName": "StatTrak™ XM1014 | Quicksilver (Field-Tested)",
  "id": 39786
  },
  {
  "productName": "StatTrak™ XM1014 | Quicksilver (Minimal Wear)",
  "id": 39787
  },
  {
  "productName": "StatTrak™ XM1014 | Red Python (Field-Tested)",
  "id": 39790
  },
  {
  "productName": "StatTrak™ XM1014 | Red Python (Minimal Wear)",
  "id": 39791
  },
  {
  "productName": "StatTrak™ XM1014 | Scumbria (Factory New)",
  "id": 39794
  },
  {
  "productName": "StatTrak™ XM1014 | Scumbria (Field-Tested)",
  "id": 39795
  },
  {
  "productName": "StatTrak™ XM1014 | Scumbria (Minimal Wear)",
  "id": 39796
  },
  {
  "productName": "StatTrak™ XM1014 | Seasons (Factory New)",
  "id": 39799
  },
  {
  "productName": "StatTrak™ XM1014 | Seasons (Field-Tested)",
  "id": 39800
  },
  {
  "productName": "StatTrak™ XM1014 | Seasons (Minimal Wear)",
  "id": 39801
  },
  {
  "productName": "StatTrak™ XM1014 | Slipstream (Factory New)",
  "id": 39804
  },
  {
  "productName": "StatTrak™ XM1014 | Slipstream (Field-Tested)",
  "id": 39805
  },
  {
  "productName": "StatTrak™ XM1014 | Slipstream (Minimal Wear)",
  "id": 39806
  },
  {
  "productName": "StatTrak™ XM1014 | Teclu Burner (Factory New)",
  "id": 39809
  },
  {
  "productName": "StatTrak™ XM1014 | Teclu Burner (Field-Tested)",
  "id": 39810
  },
  {
  "productName": "StatTrak™ XM1014 | Teclu Burner (Minimal Wear)",
  "id": 39811
  },
  {
  "productName": "StatTrak™ XM1014 | Tranquility (Factory New)",
  "id": 39814
  },
  {
  "productName": "StatTrak™ XM1014 | Tranquility (Field-Tested)",
  "id": 39815
  },
  {
  "productName": "StatTrak™ XM1014 | Tranquility (Minimal Wear)",
  "id": 39816
  },
  {
  "productName": "StatTrak™ XM1014 | Ziggy (Factory New)",
  "id": 39819
  },
  {
  "productName": "StatTrak™ XM1014 | Ziggy (Field-Tested)",
  "id": 39820
  },
  {
  "productName": "StatTrak™ XM1014 | Ziggy (Minimal Wear)",
  "id": 39821
  },
  {
  "productName": "★ Bayonet | Autotronic (Factory New)",
  "id": 42351
  },
  {
  "productName": "★ Bayonet | Autotronic (Field-Tested)",
  "id": 42352
  },
  {
  "productName": "★ Bayonet | Autotronic (Minimal Wear)",
  "id": 42353
  },
  {
  "productName": "★ Bayonet | Black Laminate (Factory New)",
  "id": 42356
  },
  {
  "productName": "★ Bayonet | Black Laminate (Field-Tested)",
  "id": 42357
  },
  {
  "productName": "★ Bayonet | Black Laminate (Minimal Wear)",
  "id": 42358
  },
  {
  "productName": "★ Bayonet | Blue Steel (Factory New)",
  "id": 42361
  },
  {
  "productName": "★ Bayonet | Blue Steel (Field-Tested)",
  "id": 42362
  },
  {
  "productName": "★ Bayonet | Blue Steel (Minimal Wear)",
  "id": 42363
  },
  {
  "productName": "★ Bayonet | Boreal Forest (Factory New)",
  "id": 42366
  },
  {
  "productName": "★ Bayonet | Boreal Forest (Field-Tested)",
  "id": 42367
  },
  {
  "productName": "★ Bayonet | Boreal Forest (Minimal Wear)",
  "id": 42368
  },
  {
  "productName": "★ Bayonet | Bright Water (Factory New)",
  "id": 42371
  },
  {
  "productName": "★ Bayonet | Bright Water (Field-Tested)",
  "id": 42372
  },
  {
  "productName": "★ Bayonet | Bright Water (Minimal Wear)",
  "id": 42373
  },
  {
  "productName": "★ Bayonet | Case Hardened (Factory New)",
  "id": 42376
  },
  {
  "productName": "★ Bayonet | Case Hardened (Field-Tested)",
  "id": 42377
  },
  {
  "productName": "★ Bayonet | Case Hardened (Minimal Wear)",
  "id": 42378
  },
  {
  "productName": "★ Bayonet | Crimson Web (Field-Tested)",
  "id": 42381
  },
  {
  "productName": "★ Bayonet | Crimson Web (Minimal Wear)",
  "id": 42382
  },
  {
  "productName": "★ Bayonet | Damascus Steel (Factory New)",
  "id": 42385
  },
  {
  "productName": "★ Bayonet | Damascus Steel (Field-Tested)",
  "id": 42386
  },
  {
  "productName": "★ Bayonet | Damascus Steel (Minimal Wear)",
  "id": 42387
  },
  {
  "productName": "★ Bayonet | Doppler (Factory New)",
  "id": 42389
  },
  {
  "productName": "★ Bayonet | Doppler (Minimal Wear)",
  "id": 42390
  },
  {
  "productName": "★ Bayonet | Fade (Factory New)",
  "id": 42391
  },
  {
  "productName": "★ Bayonet | Fade (Minimal Wear)",
  "id": 42392
  },
  {
  "productName": "★ Bayonet | Forest DDPAT (Field-Tested)",
  "id": 42394
  },
  {
  "productName": "★ Bayonet | Forest DDPAT (Minimal Wear)",
  "id": 42395
  },
  {
  "productName": "★ Bayonet | Freehand (Factory New)",
  "id": 42398
  },
  {
  "productName": "★ Bayonet | Freehand (Field-Tested)",
  "id": 42399
  },
  {
  "productName": "★ Bayonet | Freehand (Minimal Wear)",
  "id": 42400
  },
  {
  "productName": "★ Bayonet | Gamma Doppler (Factory New)",
  "id": 42402
  },
  {
  "productName": "★ Bayonet | Gamma Doppler (Minimal Wear)",
  "id": 42403
  },
  {
  "productName": "★ Bayonet | Lore (Factory New)",
  "id": 42405
  },
  {
  "productName": "★ Bayonet | Lore (Field-Tested)",
  "id": 42406
  },
  {
  "productName": "★ Bayonet | Lore (Minimal Wear)",
  "id": 42407
  },
  {
  "productName": "★ Bayonet | Marble Fade (Factory New)",
  "id": 42409
  },
  {
  "productName": "★ Bayonet | Marble Fade (Minimal Wear)",
  "id": 42410
  },
  {
  "productName": "★ Bayonet | Night (Field-Tested)",
  "id": 42412
  },
  {
  "productName": "★ Bayonet | Night (Minimal Wear)",
  "id": 42413
  },
  {
  "productName": "★ Bayonet | Safari Mesh (Factory New)",
  "id": 42418
  },
  {
  "productName": "★ Bayonet | Safari Mesh (Field-Tested)",
  "id": 42419
  },
  {
  "productName": "★ Bayonet | Safari Mesh (Minimal Wear)",
  "id": 42420
  },
  {
  "productName": "★ Bayonet | Scorched (Field-Tested)",
  "id": 42423
  },
  {
  "productName": "★ Bayonet | Scorched (Minimal Wear)",
  "id": 42424
  },
  {
  "productName": "★ Bayonet | Slaughter (Factory New)",
  "id": 42426
  },
  {
  "productName": "★ Bayonet | Slaughter (Field-Tested)",
  "id": 42427
  },
  {
  "productName": "★ Bayonet | Slaughter (Minimal Wear)",
  "id": 42428
  },
  {
  "productName": "★ Bayonet | Stained (Factory New)",
  "id": 42430
  },
  {
  "productName": "★ Bayonet | Stained (Field-Tested)",
  "id": 42431
  },
  {
  "productName": "★ Bayonet | Stained (Minimal Wear)",
  "id": 42432
  },
  {
  "productName": "★ Bayonet | Tiger Tooth (Factory New)",
  "id": 42434
  },
  {
  "productName": "★ Bayonet | Tiger Tooth (Minimal Wear)",
  "id": 42435
  },
  {
  "productName": "★ Bayonet | Ultraviolet (Factory New)",
  "id": 42437
  },
  {
  "productName": "★ Bayonet | Ultraviolet (Field-Tested)",
  "id": 42438
  },
  {
  "productName": "★ Bayonet | Ultraviolet (Minimal Wear)",
  "id": 42439
  },
  {
  "productName": "★ Bayonet | Urban Masked (Factory New)",
  "id": 42442
  },
  {
  "productName": "★ Bayonet | Urban Masked (Field-Tested)",
  "id": 42443
  },
  {
  "productName": "★ Bayonet | Urban Masked (Minimal Wear)",
  "id": 42444
  },
  {
  "productName": "★ Bloodhound Gloves | Bronzed (Field-Tested)",
  "id": 42447
  },
  {
  "productName": "★ Bloodhound Gloves | Bronzed (Minimal Wear)",
  "id": 42448
  },
  {
  "productName": "★ Bloodhound Gloves | Charred (Field-Tested)",
  "id": 42451
  },
  {
  "productName": "★ Bloodhound Gloves | Charred (Minimal Wear)",
  "id": 42452
  },
  {
  "productName": "★ Bloodhound Gloves | Guerrilla (Field-Tested)",
  "id": 42455
  },
  {
  "productName": "★ Bloodhound Gloves | Guerrilla (Minimal Wear)",
  "id": 42456
  },
  {
  "productName": "★ Bloodhound Gloves | Snakebite (Field-Tested)",
  "id": 42458
  },
  {
  "productName": "★ Bloodhound Gloves | Snakebite (Minimal Wear)",
  "id": 42459
  },
  {
  "productName": "★ Bowie Knife | Blue Steel (Factory New)",
  "id": 42463
  },
  {
  "productName": "★ Bowie Knife | Blue Steel (Field-Tested)",
  "id": 42464
  },
  {
  "productName": "★ Bowie Knife | Blue Steel (Minimal Wear)",
  "id": 42465
  },
  {
  "productName": "★ Bowie Knife | Boreal Forest (Factory New)",
  "id": 42468
  },
  {
  "productName": "★ Bowie Knife | Boreal Forest (Field-Tested)",
  "id": 42469
  },
  {
  "productName": "★ Bowie Knife | Boreal Forest (Minimal Wear)",
  "id": 42470
  },
  {
  "productName": "★ Bowie Knife | Case Hardened (Factory New)",
  "id": 42473
  },
  {
  "productName": "★ Bowie Knife | Case Hardened (Field-Tested)",
  "id": 42474
  },
  {
  "productName": "★ Bowie Knife | Case Hardened (Minimal Wear)",
  "id": 42475
  },
  {
  "productName": "★ Bowie Knife | Crimson Web (Field-Tested)",
  "id": 42478
  },
  {
  "productName": "★ Bowie Knife | Crimson Web (Minimal Wear)",
  "id": 42479
  },
  {
  "productName": "★ Bowie Knife | Damascus Steel (Factory New)",
  "id": 42482
  },
  {
  "productName": "★ Bowie Knife | Damascus Steel (Field-Tested)",
  "id": 42483
  },
  {
  "productName": "★ Bowie Knife | Damascus Steel (Minimal Wear)",
  "id": 42484
  },
  {
  "productName": "★ Bowie Knife | Doppler (Factory New)",
  "id": 42486
  },
  {
  "productName": "★ Bowie Knife | Doppler (Minimal Wear)",
  "id": 42487
  },
  {
  "productName": "★ Bowie Knife | Fade (Factory New)",
  "id": 42488
  },
  {
  "productName": "★ Bowie Knife | Fade (Minimal Wear)",
  "id": 42489
  },
  {
  "productName": "★ Bowie Knife | Forest DDPAT (Factory New)",
  "id": 42491
  },
  {
  "productName": "★ Bowie Knife | Forest DDPAT (Field-Tested)",
  "id": 42492
  },
  {
  "productName": "★ Bowie Knife | Forest DDPAT (Minimal Wear)",
  "id": 42493
  },
  {
  "productName": "★ Bowie Knife | Marble Fade (Factory New)",
  "id": 42495
  },
  {
  "productName": "★ Bowie Knife | Marble Fade (Minimal Wear)",
  "id": 42496
  },
  {
  "productName": "★ Bowie Knife | Night (Field-Tested)",
  "id": 42498
  },
  {
  "productName": "★ Bowie Knife | Night (Minimal Wear)",
  "id": 42499
  },
  {
  "productName": "★ Bowie Knife | Safari Mesh (Field-Tested)",
  "id": 42504
  },
  {
  "productName": "★ Bowie Knife | Safari Mesh (Minimal Wear)",
  "id": 42505
  },
  {
  "productName": "★ Bowie Knife | Scorched (Factory New)",
  "id": 42508
  },
  {
  "productName": "★ Bowie Knife | Scorched (Field-Tested)",
  "id": 42509
  },
  {
  "productName": "★ Bowie Knife | Scorched (Minimal Wear)",
  "id": 42510
  },
  {
  "productName": "★ Bowie Knife | Slaughter (Factory New)",
  "id": 42512
  },
  {
  "productName": "★ Bowie Knife | Slaughter (Field-Tested)",
  "id": 42513
  },
  {
  "productName": "★ Bowie Knife | Slaughter (Minimal Wear)",
  "id": 42514
  },
  {
  "productName": "★ Bowie Knife | Stained (Factory New)",
  "id": 42516
  },
  {
  "productName": "★ Bowie Knife | Stained (Field-Tested)",
  "id": 42517
  },
  {
  "productName": "★ Bowie Knife | Stained (Minimal Wear)",
  "id": 42518
  },
  {
  "productName": "★ Bowie Knife | Tiger Tooth (Factory New)",
  "id": 42520
  },
  {
  "productName": "★ Bowie Knife | Tiger Tooth (Minimal Wear)",
  "id": 42521
  },
  {
  "productName": "★ Bowie Knife | Ultraviolet (Field-Tested)",
  "id": 42523
  },
  {
  "productName": "★ Bowie Knife | Ultraviolet (Minimal Wear)",
  "id": 42524
  },
  {
  "productName": "★ Bowie Knife | Urban Masked (Field-Tested)",
  "id": 42527
  },
  {
  "productName": "★ Bowie Knife | Urban Masked (Minimal Wear)",
  "id": 42528
  },
  {
  "productName": "★ Butterfly Knife | Blue Steel (Factory New)",
  "id": 42532
  },
  {
  "productName": "★ Butterfly Knife | Blue Steel (Field-Tested)",
  "id": 42533
  },
  {
  "productName": "★ Butterfly Knife | Blue Steel (Minimal Wear)",
  "id": 42534
  },
  {
  "productName": "★ Butterfly Knife | Boreal Forest (Factory New)",
  "id": 42537
  },
  {
  "productName": "★ Butterfly Knife | Boreal Forest (Field-Tested)",
  "id": 42538
  },
  {
  "productName": "★ Butterfly Knife | Boreal Forest (Minimal Wear)",
  "id": 42539
  },
  {
  "productName": "★ Butterfly Knife | Case Hardened (Factory New)",
  "id": 42542
  },
  {
  "productName": "★ Butterfly Knife | Case Hardened (Field-Tested)",
  "id": 42543
  },
  {
  "productName": "★ Butterfly Knife | Case Hardened (Minimal Wear)",
  "id": 42544
  },
  {
  "productName": "★ Butterfly Knife | Crimson Web (Field-Tested)",
  "id": 42547
  },
  {
  "productName": "★ Butterfly Knife | Crimson Web (Minimal Wear)",
  "id": 42548
  },
  {
  "productName": "★ Butterfly Knife | Damascus Steel (Factory New)",
  "id": 42551
  },
  {
  "productName": "★ Butterfly Knife | Damascus Steel (Field-Tested)",
  "id": 42552
  },
  {
  "productName": "★ Butterfly Knife | Damascus Steel (Minimal Wear)",
  "id": 42553
  },
  {
  "productName": "★ Butterfly Knife | Doppler (Factory New)",
  "id": 42555
  },
  {
  "productName": "★ Butterfly Knife | Fade (Factory New)",
  "id": 42556
  },
  {
  "productName": "★ Butterfly Knife | Fade (Minimal Wear)",
  "id": 42557
  },
  {
  "productName": "★ Butterfly Knife | Forest DDPAT (Factory New)",
  "id": 42559
  },
  {
  "productName": "★ Butterfly Knife | Forest DDPAT (Field-Tested)",
  "id": 42560
  },
  {
  "productName": "★ Butterfly Knife | Forest DDPAT (Minimal Wear)",
  "id": 42561
  },
  {
  "productName": "★ Butterfly Knife | Marble Fade (Factory New)",
  "id": 42563
  },
  {
  "productName": "★ Butterfly Knife | Marble Fade (Minimal Wear)",
  "id": 42564
  },
  {
  "productName": "★ Butterfly Knife | Night (Field-Tested)",
  "id": 42566
  },
  {
  "productName": "★ Butterfly Knife | Night (Minimal Wear)",
  "id": 42567
  },
  {
  "productName": "★ Butterfly Knife | Safari Mesh (Field-Tested)",
  "id": 42572
  },
  {
  "productName": "★ Butterfly Knife | Safari Mesh (Minimal Wear)",
  "id": 42573
  },
  {
  "productName": "★ Butterfly Knife | Scorched (Field-Tested)",
  "id": 42576
  },
  {
  "productName": "★ Butterfly Knife | Scorched (Minimal Wear)",
  "id": 42577
  },
  {
  "productName": "★ Butterfly Knife | Slaughter (Factory New)",
  "id": 42579
  },
  {
  "productName": "★ Butterfly Knife | Slaughter (Field-Tested)",
  "id": 42580
  },
  {
  "productName": "★ Butterfly Knife | Slaughter (Minimal Wear)",
  "id": 42581
  },
  {
  "productName": "★ Butterfly Knife | Stained (Factory New)",
  "id": 42583
  },
  {
  "productName": "★ Butterfly Knife | Stained (Field-Tested)",
  "id": 42584
  },
  {
  "productName": "★ Butterfly Knife | Stained (Minimal Wear)",
  "id": 42585
  },
  {
  "productName": "★ Butterfly Knife | Tiger Tooth (Factory New)",
  "id": 42587
  },
  {
  "productName": "★ Butterfly Knife | Tiger Tooth (Minimal Wear)",
  "id": 42588
  },
  {
  "productName": "★ Butterfly Knife | Ultraviolet (Field-Tested)",
  "id": 42590
  },
  {
  "productName": "★ Butterfly Knife | Ultraviolet (Minimal Wear)",
  "id": 42591
  },
  {
  "productName": "★ Butterfly Knife | Urban Masked (Field-Tested)",
  "id": 42594
  },
  {
  "productName": "★ Butterfly Knife | Urban Masked (Minimal Wear)",
  "id": 42595
  },
  {
  "productName": "★ Driver Gloves | Convoy (Field-Tested)",
  "id": 42598
  },
  {
  "productName": "★ Driver Gloves | Convoy (Minimal Wear)",
  "id": 42599
  },
  {
  "productName": "★ Driver Gloves | Crimson Weave (Field-Tested)",
  "id": 42602
  },
  {
  "productName": "★ Driver Gloves | Crimson Weave (Minimal Wear)",
  "id": 42603
  },
  {
  "productName": "★ Driver Gloves | Diamondback (Factory New)",
  "id": 42606
  },
  {
  "productName": "★ Driver Gloves | Diamondback (Field-Tested)",
  "id": 42607
  },
  {
  "productName": "★ Driver Gloves | Diamondback (Minimal Wear)",
  "id": 42608
  },
  {
  "productName": "★ Driver Gloves | Lunar Weave (Field-Tested)",
  "id": 42611
  },
  {
  "productName": "★ Driver Gloves | Lunar Weave (Minimal Wear)",
  "id": 42612
  },
  {
  "productName": "★ Falchion Knife | Blue Steel (Factory New)",
  "id": 42614
  },
  {
  "productName": "★ Falchion Knife | Blue Steel (Field-Tested)",
  "id": 42615
  },
  {
  "productName": "★ Falchion Knife | Blue Steel (Minimal Wear)",
  "id": 42616
  },
  {
  "productName": "★ Falchion Knife | Boreal Forest (Field-Tested)",
  "id": 42619
  },
  {
  "productName": "★ Falchion Knife | Boreal Forest (Minimal Wear)",
  "id": 42620
  },
  {
  "productName": "★ Falchion Knife | Case Hardened (Factory New)",
  "id": 42623
  },
  {
  "productName": "★ Falchion Knife | Case Hardened (Field-Tested)",
  "id": 42624
  },
  {
  "productName": "★ Falchion Knife | Case Hardened (Minimal Wear)",
  "id": 42625
  },
  {
  "productName": "★ Falchion Knife | Crimson Web (Field-Tested)",
  "id": 42628
  },
  {
  "productName": "★ Falchion Knife | Crimson Web (Minimal Wear)",
  "id": 42629
  },
  {
  "productName": "★ Falchion Knife | Damascus Steel (Factory New)",
  "id": 42632
  },
  {
  "productName": "★ Falchion Knife | Damascus Steel (Field-Tested)",
  "id": 42633
  },
  {
  "productName": "★ Falchion Knife | Damascus Steel (Minimal Wear)",
  "id": 42634
  },
  {
  "productName": "★ Falchion Knife | Doppler (Factory New)",
  "id": 42636
  },
  {
  "productName": "★ Falchion Knife | Doppler (Minimal Wear)",
  "id": 42637
  },
  {
  "productName": "★ Falchion Knife | Fade (Factory New)",
  "id": 42638
  },
  {
  "productName": "★ Falchion Knife | Fade (Minimal Wear)",
  "id": 42639
  },
  {
  "productName": "★ Falchion Knife | Forest DDPAT (Factory New)",
  "id": 42641
  },
  {
  "productName": "★ Falchion Knife | Forest DDPAT (Field-Tested)",
  "id": 42642
  },
  {
  "productName": "★ Falchion Knife | Forest DDPAT (Minimal Wear)",
  "id": 42643
  },
  {
  "productName": "★ Falchion Knife | Marble Fade (Factory New)",
  "id": 42645
  },
  {
  "productName": "★ Falchion Knife | Marble Fade (Minimal Wear)",
  "id": 42646
  },
  {
  "productName": "★ Falchion Knife | Night (Field-Tested)",
  "id": 42648
  },
  {
  "productName": "★ Falchion Knife | Safari Mesh (Factory New)",
  "id": 42653
  },
  {
  "productName": "★ Falchion Knife | Safari Mesh (Field-Tested)",
  "id": 42654
  },
  {
  "productName": "★ Falchion Knife | Safari Mesh (Minimal Wear)",
  "id": 42655
  },
  {
  "productName": "★ Falchion Knife | Scorched (Factory New)",
  "id": 42658
  },
  {
  "productName": "★ Falchion Knife | Scorched (Field-Tested)",
  "id": 42659
  },
  {
  "productName": "★ Falchion Knife | Scorched (Minimal Wear)",
  "id": 42660
  },
  {
  "productName": "★ Falchion Knife | Slaughter (Factory New)",
  "id": 42662
  },
  {
  "productName": "★ Falchion Knife | Slaughter (Field-Tested)",
  "id": 42663
  },
  {
  "productName": "★ Falchion Knife | Slaughter (Minimal Wear)",
  "id": 42664
  },
  {
  "productName": "★ Falchion Knife | Stained (Factory New)",
  "id": 42666
  },
  {
  "productName": "★ Falchion Knife | Stained (Field-Tested)",
  "id": 42667
  },
  {
  "productName": "★ Falchion Knife | Stained (Minimal Wear)",
  "id": 42668
  },
  {
  "productName": "★ Falchion Knife | Tiger Tooth (Factory New)",
  "id": 42670
  },
  {
  "productName": "★ Falchion Knife | Tiger Tooth (Minimal Wear)",
  "id": 42671
  },
  {
  "productName": "★ Falchion Knife | Ultraviolet (Factory New)",
  "id": 42673
  },
  {
  "productName": "★ Falchion Knife | Ultraviolet (Field-Tested)",
  "id": 42674
  },
  {
  "productName": "★ Falchion Knife | Ultraviolet (Minimal Wear)",
  "id": 42675
  },
  {
  "productName": "★ Falchion Knife | Urban Masked (Factory New)",
  "id": 42678
  },
  {
  "productName": "★ Falchion Knife | Urban Masked (Field-Tested)",
  "id": 42679
  },
  {
  "productName": "★ Falchion Knife | Urban Masked (Minimal Wear)",
  "id": 42680
  },
  {
  "productName": "★ Flip Knife | Autotronic (Factory New)",
  "id": 42684
  },
  {
  "productName": "★ Flip Knife | Autotronic (Field-Tested)",
  "id": 42685
  },
  {
  "productName": "★ Flip Knife | Autotronic (Minimal Wear)",
  "id": 42686
  },
  {
  "productName": "★ Flip Knife | Black Laminate (Factory New)",
  "id": 42689
  },
  {
  "productName": "★ Flip Knife | Black Laminate (Field-Tested)",
  "id": 42690
  },
  {
  "productName": "★ Flip Knife | Black Laminate (Minimal Wear)",
  "id": 42691
  },
  {
  "productName": "★ Flip Knife | Blue Steel (Factory New)",
  "id": 42694
  },
  {
  "productName": "★ Flip Knife | Blue Steel (Field-Tested)",
  "id": 42695
  },
  {
  "productName": "★ Flip Knife | Blue Steel (Minimal Wear)",
  "id": 42696
  },
  {
  "productName": "★ Flip Knife | Boreal Forest (Field-Tested)",
  "id": 42699
  },
  {
  "productName": "★ Flip Knife | Boreal Forest (Minimal Wear)",
  "id": 42700
  },
  {
  "productName": "★ Flip Knife | Bright Water (Factory New)",
  "id": 42702
  },
  {
  "productName": "★ Flip Knife | Bright Water (Field-Tested)",
  "id": 42703
  },
  {
  "productName": "★ Flip Knife | Bright Water (Minimal Wear)",
  "id": 42704
  },
  {
  "productName": "★ Flip Knife | Case Hardened (Factory New)",
  "id": 42707
  },
  {
  "productName": "★ Flip Knife | Case Hardened (Field-Tested)",
  "id": 42708
  },
  {
  "productName": "★ Flip Knife | Case Hardened (Minimal Wear)",
  "id": 42709
  },
  {
  "productName": "★ Flip Knife | Crimson Web (Field-Tested)",
  "id": 42712
  },
  {
  "productName": "★ Flip Knife | Crimson Web (Minimal Wear)",
  "id": 42713
  },
  {
  "productName": "★ Flip Knife | Damascus Steel (Factory New)",
  "id": 42716
  },
  {
  "productName": "★ Flip Knife | Damascus Steel (Field-Tested)",
  "id": 42717
  },
  {
  "productName": "★ Flip Knife | Damascus Steel (Minimal Wear)",
  "id": 42718
  },
  {
  "productName": "★ Flip Knife | Doppler (Factory New)",
  "id": 42720
  },
  {
  "productName": "★ Flip Knife | Doppler (Minimal Wear)",
  "id": 42721
  },
  {
  "productName": "★ Flip Knife | Fade (Factory New)",
  "id": 42722
  },
  {
  "productName": "★ Flip Knife | Fade (Minimal Wear)",
  "id": 42723
  },
  {
  "productName": "★ Flip Knife | Forest DDPAT (Factory New)",
  "id": 42725
  },
  {
  "productName": "★ Flip Knife | Forest DDPAT (Field-Tested)",
  "id": 42726
  },
  {
  "productName": "★ Flip Knife | Forest DDPAT (Minimal Wear)",
  "id": 42727
  },
  {
  "productName": "★ Flip Knife | Freehand (Factory New)",
  "id": 42730
  },
  {
  "productName": "★ Flip Knife | Freehand (Field-Tested)",
  "id": 42731
  },
  {
  "productName": "★ Flip Knife | Freehand (Minimal Wear)",
  "id": 42732
  },
  {
  "productName": "★ Flip Knife | Gamma Doppler (Factory New)",
  "id": 42734
  },
  {
  "productName": "★ Flip Knife | Gamma Doppler (Minimal Wear)",
  "id": 42735
  },
  {
  "productName": "★ Flip Knife | Lore (Factory New)",
  "id": 42737
  },
  {
  "productName": "★ Flip Knife | Lore (Field-Tested)",
  "id": 42738
  },
  {
  "productName": "★ Flip Knife | Lore (Minimal Wear)",
  "id": 42739
  },
  {
  "productName": "★ Flip Knife | Marble Fade (Factory New)",
  "id": 42741
  },
  {
  "productName": "★ Flip Knife | Marble Fade (Minimal Wear)",
  "id": 42742
  },
  {
  "productName": "★ Flip Knife | Night (Factory New)",
  "id": 42744
  },
  {
  "productName": "★ Flip Knife | Night (Field-Tested)",
  "id": 42745
  },
  {
  "productName": "★ Flip Knife | Night (Minimal Wear)",
  "id": 42746
  },
  {
  "productName": "★ Flip Knife | Safari Mesh (Factory New)",
  "id": 42751
  },
  {
  "productName": "★ Flip Knife | Safari Mesh (Field-Tested)",
  "id": 42752
  },
  {
  "productName": "★ Flip Knife | Safari Mesh (Minimal Wear)",
  "id": 42753
  },
  {
  "productName": "★ Flip Knife | Scorched (Factory New)",
  "id": 42756
  },
  {
  "productName": "★ Flip Knife | Scorched (Field-Tested)",
  "id": 42757
  },
  {
  "productName": "★ Flip Knife | Scorched (Minimal Wear)",
  "id": 42758
  },
  {
  "productName": "★ Flip Knife | Slaughter (Factory New)",
  "id": 42760
  },
  {
  "productName": "★ Flip Knife | Slaughter (Field-Tested)",
  "id": 42761
  },
  {
  "productName": "★ Flip Knife | Slaughter (Minimal Wear)",
  "id": 42762
  },
  {
  "productName": "★ Flip Knife | Stained (Factory New)",
  "id": 42764
  },
  {
  "productName": "★ Flip Knife | Stained (Field-Tested)",
  "id": 42765
  },
  {
  "productName": "★ Flip Knife | Stained (Minimal Wear)",
  "id": 42766
  },
  {
  "productName": "★ Flip Knife | Tiger Tooth (Factory New)",
  "id": 42768
  },
  {
  "productName": "★ Flip Knife | Tiger Tooth (Minimal Wear)",
  "id": 42769
  },
  {
  "productName": "★ Flip Knife | Ultraviolet (Factory New)",
  "id": 42771
  },
  {
  "productName": "★ Flip Knife | Ultraviolet (Field-Tested)",
  "id": 42772
  },
  {
  "productName": "★ Flip Knife | Ultraviolet (Minimal Wear)",
  "id": 42773
  },
  {
  "productName": "★ Flip Knife | Urban Masked (Field-Tested)",
  "id": 42776
  },
  {
  "productName": "★ Flip Knife | Urban Masked (Minimal Wear)",
  "id": 42777
  },
  {
  "productName": "★ Gut Knife | Autotronic (Factory New)",
  "id": 42781
  },
  {
  "productName": "★ Gut Knife | Autotronic (Field-Tested)",
  "id": 42782
  },
  {
  "productName": "★ Gut Knife | Autotronic (Minimal Wear)",
  "id": 42783
  },
  {
  "productName": "★ Gut Knife | Black Laminate (Factory New)",
  "id": 42786
  },
  {
  "productName": "★ Gut Knife | Black Laminate (Field-Tested)",
  "id": 42787
  },
  {
  "productName": "★ Gut Knife | Black Laminate (Minimal Wear)",
  "id": 42788
  },
  {
  "productName": "★ Gut Knife | Blue Steel (Factory New)",
  "id": 42791
  },
  {
  "productName": "★ Gut Knife | Blue Steel (Field-Tested)",
  "id": 42792
  },
  {
  "productName": "★ Gut Knife | Blue Steel (Minimal Wear)",
  "id": 42793
  },
  {
  "productName": "★ Gut Knife | Boreal Forest (Field-Tested)",
  "id": 42796
  },
  {
  "productName": "★ Gut Knife | Boreal Forest (Minimal Wear)",
  "id": 42797
  },
  {
  "productName": "★ Gut Knife | Bright Water (Factory New)",
  "id": 42799
  },
  {
  "productName": "★ Gut Knife | Bright Water (Field-Tested)",
  "id": 42800
  },
  {
  "productName": "★ Gut Knife | Bright Water (Minimal Wear)",
  "id": 42801
  },
  {
  "productName": "★ Gut Knife | Case Hardened (Factory New)",
  "id": 42804
  },
  {
  "productName": "★ Gut Knife | Case Hardened (Field-Tested)",
  "id": 42805
  },
  {
  "productName": "★ Gut Knife | Case Hardened (Minimal Wear)",
  "id": 42806
  },
  {
  "productName": "★ Gut Knife | Crimson Web (Factory New)",
  "id": 42809
  },
  {
  "productName": "★ Gut Knife | Crimson Web (Field-Tested)",
  "id": 42810
  },
  {
  "productName": "★ Gut Knife | Crimson Web (Minimal Wear)",
  "id": 42811
  },
  {
  "productName": "★ Gut Knife | Damascus Steel (Factory New)",
  "id": 42814
  },
  {
  "productName": "★ Gut Knife | Damascus Steel (Field-Tested)",
  "id": 42815
  },
  {
  "productName": "★ Gut Knife | Damascus Steel (Minimal Wear)",
  "id": 42816
  },
  {
  "productName": "★ Gut Knife | Doppler (Factory New)",
  "id": 42818
  },
  {
  "productName": "★ Gut Knife | Doppler (Minimal Wear)",
  "id": 42819
  },
  {
  "productName": "★ Gut Knife | Fade (Factory New)",
  "id": 42820
  },
  {
  "productName": "★ Gut Knife | Fade (Minimal Wear)",
  "id": 42821
  },
  {
  "productName": "★ Gut Knife | Forest DDPAT (Factory New)",
  "id": 42823
  },
  {
  "productName": "★ Gut Knife | Forest DDPAT (Field-Tested)",
  "id": 42824
  },
  {
  "productName": "★ Gut Knife | Forest DDPAT (Minimal Wear)",
  "id": 42825
  },
  {
  "productName": "★ Gut Knife | Freehand (Factory New)",
  "id": 42828
  },
  {
  "productName": "★ Gut Knife | Freehand (Field-Tested)",
  "id": 42829
  },
  {
  "productName": "★ Gut Knife | Freehand (Minimal Wear)",
  "id": 42830
  },
  {
  "productName": "★ Gut Knife | Gamma Doppler (Factory New)",
  "id": 42832
  },
  {
  "productName": "★ Gut Knife | Gamma Doppler (Minimal Wear)",
  "id": 42833
  },
  {
  "productName": "★ Gut Knife | Lore (Factory New)",
  "id": 42835
  },
  {
  "productName": "★ Gut Knife | Lore (Field-Tested)",
  "id": 42836
  },
  {
  "productName": "★ Gut Knife | Lore (Minimal Wear)",
  "id": 42837
  },
  {
  "productName": "★ Gut Knife | Marble Fade (Factory New)",
  "id": 42839
  },
  {
  "productName": "★ Gut Knife | Marble Fade (Minimal Wear)",
  "id": 42840
  },
  {
  "productName": "★ Gut Knife | Night (Field-Tested)",
  "id": 42842
  },
  {
  "productName": "★ Gut Knife | Night (Minimal Wear)",
  "id": 42843
  },
  {
  "productName": "★ Gut Knife | Safari Mesh (Factory New)",
  "id": 42848
  },
  {
  "productName": "★ Gut Knife | Safari Mesh (Field-Tested)",
  "id": 42849
  },
  {
  "productName": "★ Gut Knife | Safari Mesh (Minimal Wear)",
  "id": 42850
  },
  {
  "productName": "★ Gut Knife | Scorched (Field-Tested)",
  "id": 42853
  },
  {
  "productName": "★ Gut Knife | Scorched (Minimal Wear)",
  "id": 42854
  },
  {
  "productName": "★ Gut Knife | Slaughter (Factory New)",
  "id": 42856
  },
  {
  "productName": "★ Gut Knife | Slaughter (Field-Tested)",
  "id": 42857
  },
  {
  "productName": "★ Gut Knife | Slaughter (Minimal Wear)",
  "id": 42858
  },
  {
  "productName": "★ Gut Knife | Stained (Factory New)",
  "id": 42860
  },
  {
  "productName": "★ Gut Knife | Stained (Field-Tested)",
  "id": 42861
  },
  {
  "productName": "★ Gut Knife | Stained (Minimal Wear)",
  "id": 42862
  },
  {
  "productName": "★ Gut Knife | Tiger Tooth (Factory New)",
  "id": 42864
  },
  {
  "productName": "★ Gut Knife | Tiger Tooth (Minimal Wear)",
  "id": 42865
  },
  {
  "productName": "★ Gut Knife | Ultraviolet (Field-Tested)",
  "id": 42867
  },
  {
  "productName": "★ Gut Knife | Ultraviolet (Minimal Wear)",
  "id": 42868
  },
  {
  "productName": "★ Gut Knife | Urban Masked (Field-Tested)",
  "id": 42871
  },
  {
  "productName": "★ Gut Knife | Urban Masked (Minimal Wear)",
  "id": 42872
  },
  {
  "productName": "★ Hand Wraps | Badlands (Field-Tested)",
  "id": 42875
  },
  {
  "productName": "★ Hand Wraps | Badlands (Minimal Wear)",
  "id": 42876
  },
  {
  "productName": "★ Hand Wraps | Leather (Field-Tested)",
  "id": 42879
  },
  {
  "productName": "★ Hand Wraps | Leather (Minimal Wear)",
  "id": 42880
  },
  {
  "productName": "★ Hand Wraps | Slaughter (Field-Tested)",
  "id": 42883
  },
  {
  "productName": "★ Hand Wraps | Slaughter (Minimal Wear)",
  "id": 42884
  },
  {
  "productName": "★ Hand Wraps | Spruce DDPAT (Factory New)",
  "id": 42887
  },
  {
  "productName": "★ Hand Wraps | Spruce DDPAT (Field-Tested)",
  "id": 42888
  },
  {
  "productName": "★ Hand Wraps | Spruce DDPAT (Minimal Wear)",
  "id": 42889
  },
  {
  "productName": "★ Huntsman Knife | Blue Steel (Factory New)",
  "id": 42893
  },
  {
  "productName": "★ Huntsman Knife | Blue Steel (Field-Tested)",
  "id": 42894
  },
  {
  "productName": "★ Huntsman Knife | Blue Steel (Minimal Wear)",
  "id": 42895
  },
  {
  "productName": "★ Huntsman Knife | Boreal Forest (Factory New)",
  "id": 42898
  },
  {
  "productName": "★ Huntsman Knife | Boreal Forest (Field-Tested)",
  "id": 42899
  },
  {
  "productName": "★ Huntsman Knife | Boreal Forest (Minimal Wear)",
  "id": 42900
  },
  {
  "productName": "★ Huntsman Knife | Case Hardened (Factory New)",
  "id": 42903
  },
  {
  "productName": "★ Huntsman Knife | Case Hardened (Field-Tested)",
  "id": 42904
  },
  {
  "productName": "★ Huntsman Knife | Case Hardened (Minimal Wear)",
  "id": 42905
  },
  {
  "productName": "★ Huntsman Knife | Crimson Web (Factory New)",
  "id": 42908
  },
  {
  "productName": "★ Huntsman Knife | Crimson Web (Field-Tested)",
  "id": 42909
  },
  {
  "productName": "★ Huntsman Knife | Crimson Web (Minimal Wear)",
  "id": 42910
  },
  {
  "productName": "★ Huntsman Knife | Damascus Steel (Factory New)",
  "id": 42913
  },
  {
  "productName": "★ Huntsman Knife | Damascus Steel (Field-Tested)",
  "id": 42914
  },
  {
  "productName": "★ Huntsman Knife | Damascus Steel (Minimal Wear)",
  "id": 42915
  },
  {
  "productName": "★ Huntsman Knife | Doppler (Factory New)",
  "id": 42917
  },
  {
  "productName": "★ Huntsman Knife | Doppler (Minimal Wear)",
  "id": 42918
  },
  {
  "productName": "★ Huntsman Knife | Fade (Factory New)",
  "id": 42919
  },
  {
  "productName": "★ Huntsman Knife | Fade (Minimal Wear)",
  "id": 42920
  },
  {
  "productName": "★ Huntsman Knife | Forest DDPAT (Factory New)",
  "id": 42922
  },
  {
  "productName": "★ Huntsman Knife | Forest DDPAT (Field-Tested)",
  "id": 42923
  },
  {
  "productName": "★ Huntsman Knife | Forest DDPAT (Minimal Wear)",
  "id": 42924
  },
  {
  "productName": "★ Huntsman Knife | Marble Fade (Factory New)",
  "id": 42926
  },
  {
  "productName": "★ Huntsman Knife | Marble Fade (Minimal Wear)",
  "id": 42927
  },
  {
  "productName": "★ Huntsman Knife | Night (Field-Tested)",
  "id": 42929
  },
  {
  "productName": "★ Huntsman Knife | Night (Minimal Wear)",
  "id": 42930
  },
  {
  "productName": "★ Huntsman Knife | Safari Mesh (Factory New)",
  "id": 42934
  },
  {
  "productName": "★ Huntsman Knife | Safari Mesh (Field-Tested)",
  "id": 42935
  },
  {
  "productName": "★ Huntsman Knife | Safari Mesh (Minimal Wear)",
  "id": 42936
  },
  {
  "productName": "★ Huntsman Knife | Scorched (Field-Tested)",
  "id": 42939
  },
  {
  "productName": "★ Huntsman Knife | Scorched (Minimal Wear)",
  "id": 42940
  },
  {
  "productName": "★ Huntsman Knife | Slaughter (Factory New)",
  "id": 42942
  },
  {
  "productName": "★ Huntsman Knife | Slaughter (Field-Tested)",
  "id": 42943
  },
  {
  "productName": "★ Huntsman Knife | Slaughter (Minimal Wear)",
  "id": 42944
  },
  {
  "productName": "★ Huntsman Knife | Stained (Factory New)",
  "id": 42946
  },
  {
  "productName": "★ Huntsman Knife | Stained (Field-Tested)",
  "id": 42947
  },
  {
  "productName": "★ Huntsman Knife | Stained (Minimal Wear)",
  "id": 42948
  },
  {
  "productName": "★ Huntsman Knife | Tiger Tooth (Factory New)",
  "id": 42950
  },
  {
  "productName": "★ Huntsman Knife | Tiger Tooth (Minimal Wear)",
  "id": 42951
  },
  {
  "productName": "★ Huntsman Knife | Ultraviolet (Field-Tested)",
  "id": 42953
  },
  {
  "productName": "★ Huntsman Knife | Ultraviolet (Minimal Wear)",
  "id": 42954
  },
  {
  "productName": "★ Huntsman Knife | Urban Masked (Factory New)",
  "id": 42957
  },
  {
  "productName": "★ Huntsman Knife | Urban Masked (Field-Tested)",
  "id": 42958
  },
  {
  "productName": "★ Huntsman Knife | Urban Masked (Minimal Wear)",
  "id": 42959
  },
  {
  "productName": "★ Karambit | Autotronic (Factory New)",
  "id": 42963
  },
  {
  "productName": "★ Karambit | Autotronic (Field-Tested)",
  "id": 42964
  },
  {
  "productName": "★ Karambit | Autotronic (Minimal Wear)",
  "id": 42965
  },
  {
  "productName": "★ Karambit | Black Laminate (Factory New)",
  "id": 42967
  },
  {
  "productName": "★ Karambit | Black Laminate (Field-Tested)",
  "id": 42968
  },
  {
  "productName": "★ Karambit | Black Laminate (Minimal Wear)",
  "id": 42969
  },
  {
  "productName": "★ Karambit | Blue Steel (Field-Tested)",
  "id": 42972
  },
  {
  "productName": "★ Karambit | Blue Steel (Minimal Wear)",
  "id": 42973
  },
  {
  "productName": "★ Karambit | Boreal Forest (Factory New)",
  "id": 42976
  },
  {
  "productName": "★ Karambit | Boreal Forest (Field-Tested)",
  "id": 42977
  },
  {
  "productName": "★ Karambit | Boreal Forest (Minimal Wear)",
  "id": 42978
  },
  {
  "productName": "★ Karambit | Bright Water (Factory New)",
  "id": 42980
  },
  {
  "productName": "★ Karambit | Bright Water (Field-Tested)",
  "id": 42981
  },
  {
  "productName": "★ Karambit | Bright Water (Minimal Wear)",
  "id": 42982
  },
  {
  "productName": "★ Karambit | Case Hardened (Factory New)",
  "id": 42985
  },
  {
  "productName": "★ Karambit | Case Hardened (Field-Tested)",
  "id": 42986
  },
  {
  "productName": "★ Karambit | Case Hardened (Minimal Wear)",
  "id": 42987
  },
  {
  "productName": "★ Karambit | Crimson Web (Field-Tested)",
  "id": 42990
  },
  {
  "productName": "★ Karambit | Crimson Web (Minimal Wear)",
  "id": 42991
  },
  {
  "productName": "★ Karambit | Damascus Steel (Factory New)",
  "id": 42994
  },
  {
  "productName": "★ Karambit | Damascus Steel (Field-Tested)",
  "id": 42995
  },
  {
  "productName": "★ Karambit | Damascus Steel (Minimal Wear)",
  "id": 42996
  },
  {
  "productName": "★ Karambit | Doppler (Factory New)",
  "id": 42998
  },
  {
  "productName": "★ Karambit | Doppler (Minimal Wear)",
  "id": 42999
  },
  {
  "productName": "★ Karambit | Fade (Factory New)",
  "id": 43000
  },
  {
  "productName": "★ Karambit | Fade (Minimal Wear)",
  "id": 43001
  },
  {
  "productName": "★ Karambit | Forest DDPAT (Field-Tested)",
  "id": 43003
  },
  {
  "productName": "★ Karambit | Forest DDPAT (Minimal Wear)",
  "id": 43004
  },
  {
  "productName": "★ Karambit | Freehand (Factory New)",
  "id": 43007
  },
  {
  "productName": "★ Karambit | Freehand (Field-Tested)",
  "id": 43008
  },
  {
  "productName": "★ Karambit | Freehand (Minimal Wear)",
  "id": 43009
  },
  {
  "productName": "★ Karambit | Gamma Doppler (Factory New)",
  "id": 43011
  },
  {
  "productName": "★ Karambit | Gamma Doppler (Minimal Wear)",
  "id": 43012
  },
  {
  "productName": "★ Karambit | Lore (Field-Tested)",
  "id": 43014
  },
  {
  "productName": "★ Karambit | Lore (Minimal Wear)",
  "id": 43015
  },
  {
  "productName": "★ Karambit | Marble Fade (Factory New)",
  "id": 43017
  },
  {
  "productName": "★ Karambit | Marble Fade (Minimal Wear)",
  "id": 43018
  },
  {
  "productName": "★ Karambit | Night (Field-Tested)",
  "id": 43020
  },
  {
  "productName": "★ Karambit | Night (Minimal Wear)",
  "id": 43021
  },
  {
  "productName": "★ Karambit | Safari Mesh (Field-Tested)",
  "id": 43026
  },
  {
  "productName": "★ Karambit | Safari Mesh (Minimal Wear)",
  "id": 43027
  },
  {
  "productName": "★ Karambit | Scorched (Field-Tested)",
  "id": 43030
  },
  {
  "productName": "★ Karambit | Scorched (Minimal Wear)",
  "id": 43031
  },
  {
  "productName": "★ Karambit | Slaughter (Factory New)",
  "id": 43033
  },
  {
  "productName": "★ Karambit | Slaughter (Field-Tested)",
  "id": 43034
  },
  {
  "productName": "★ Karambit | Slaughter (Minimal Wear)",
  "id": 43035
  },
  {
  "productName": "★ Karambit | Stained (Factory New)",
  "id": 43037
  },
  {
  "productName": "★ Karambit | Stained (Field-Tested)",
  "id": 43038
  },
  {
  "productName": "★ Karambit | Stained (Minimal Wear)",
  "id": 43039
  },
  {
  "productName": "★ Karambit | Tiger Tooth (Factory New)",
  "id": 43041
  },
  {
  "productName": "★ Karambit | Tiger Tooth (Minimal Wear)",
  "id": 43042
  },
  {
  "productName": "★ Karambit | Ultraviolet (Factory New)",
  "id": 43044
  },
  {
  "productName": "★ Karambit | Ultraviolet (Field-Tested)",
  "id": 43045
  },
  {
  "productName": "★ Karambit | Ultraviolet (Minimal Wear)",
  "id": 43046
  },
  {
  "productName": "★ Karambit | Urban Masked (Field-Tested)",
  "id": 43049
  },
  {
  "productName": "★ Karambit | Urban Masked (Minimal Wear)",
  "id": 43050
  },
  {
  "productName": "★ M9 Bayonet | Autotronic (Factory New)",
  "id": 43054
  },
  {
  "productName": "★ M9 Bayonet | Autotronic (Field-Tested)",
  "id": 43055
  },
  {
  "productName": "★ M9 Bayonet | Autotronic (Minimal Wear)",
  "id": 43056
  },
  {
  "productName": "★ M9 Bayonet | Black Laminate (Factory New)",
  "id": 43059
  },
  {
  "productName": "★ M9 Bayonet | Black Laminate (Field-Tested)",
  "id": 43060
  },
  {
  "productName": "★ M9 Bayonet | Black Laminate (Minimal Wear)",
  "id": 43061
  },
  {
  "productName": "★ M9 Bayonet | Blue Steel (Factory New)",
  "id": 43064
  },
  {
  "productName": "★ M9 Bayonet | Blue Steel (Field-Tested)",
  "id": 43065
  },
  {
  "productName": "★ M9 Bayonet | Blue Steel (Minimal Wear)",
  "id": 43066
  },
  {
  "productName": "★ M9 Bayonet | Boreal Forest (Field-Tested)",
  "id": 43069
  },
  {
  "productName": "★ M9 Bayonet | Boreal Forest (Minimal Wear)",
  "id": 43070
  },
  {
  "productName": "★ M9 Bayonet | Bright Water (Factory New)",
  "id": 43073
  },
  {
  "productName": "★ M9 Bayonet | Bright Water (Field-Tested)",
  "id": 43074
  },
  {
  "productName": "★ M9 Bayonet | Bright Water (Minimal Wear)",
  "id": 43075
  },
  {
  "productName": "★ M9 Bayonet | Case Hardened (Factory New)",
  "id": 43078
  },
  {
  "productName": "★ M9 Bayonet | Case Hardened (Field-Tested)",
  "id": 43079
  },
  {
  "productName": "★ M9 Bayonet | Case Hardened (Minimal Wear)",
  "id": 43080
  },
  {
  "productName": "★ M9 Bayonet | Crimson Web (Field-Tested)",
  "id": 43083
  },
  {
  "productName": "★ M9 Bayonet | Crimson Web (Minimal Wear)",
  "id": 43084
  },
  {
  "productName": "★ M9 Bayonet | Damascus Steel (Factory New)",
  "id": 43087
  },
  {
  "productName": "★ M9 Bayonet | Damascus Steel (Field-Tested)",
  "id": 43088
  },
  {
  "productName": "★ M9 Bayonet | Damascus Steel (Minimal Wear)",
  "id": 43089
  },
  {
  "productName": "★ M9 Bayonet | Doppler (Factory New)",
  "id": 43091
  },
  {
  "productName": "★ M9 Bayonet | Doppler (Minimal Wear)",
  "id": 43092
  },
  {
  "productName": "★ M9 Bayonet | Fade (Minimal Wear)",
  "id": 43093
  },
  {
  "productName": "★ M9 Bayonet | Forest DDPAT (Factory New)",
  "id": 43095
  },
  {
  "productName": "★ M9 Bayonet | Forest DDPAT (Field-Tested)",
  "id": 43096
  },
  {
  "productName": "★ M9 Bayonet | Forest DDPAT (Minimal Wear)",
  "id": 43097
  },
  {
  "productName": "★ M9 Bayonet | Freehand (Factory New)",
  "id": 43100
  },
  {
  "productName": "★ M9 Bayonet | Freehand (Field-Tested)",
  "id": 43101
  },
  {
  "productName": "★ M9 Bayonet | Freehand (Minimal Wear)",
  "id": 43102
  },
  {
  "productName": "★ M9 Bayonet | Gamma Doppler (Factory New)",
  "id": 43104
  },
  {
  "productName": "★ M9 Bayonet | Lore (Factory New)",
  "id": 43106
  },
  {
  "productName": "★ M9 Bayonet | Lore (Field-Tested)",
  "id": 43107
  },
  {
  "productName": "★ M9 Bayonet | Lore (Minimal Wear)",
  "id": 43108
  },
  {
  "productName": "★ M9 Bayonet | Marble Fade (Factory New)",
  "id": 43110
  },
  {
  "productName": "★ M9 Bayonet | Marble Fade (Minimal Wear)",
  "id": 43111
  },
  {
  "productName": "★ M9 Bayonet | Night (Factory New)",
  "id": 43113
  },
  {
  "productName": "★ M9 Bayonet | Night (Field-Tested)",
  "id": 43114
  },
  {
  "productName": "★ M9 Bayonet | Night (Minimal Wear)",
  "id": 43115
  },
  {
  "productName": "★ M9 Bayonet | Safari Mesh (Field-Tested)",
  "id": 43120
  },
  {
  "productName": "★ M9 Bayonet | Safari Mesh (Minimal Wear)",
  "id": 43121
  },
  {
  "productName": "★ M9 Bayonet | Scorched (Factory New)",
  "id": 43124
  },
  {
  "productName": "★ M9 Bayonet | Scorched (Field-Tested)",
  "id": 43125
  },
  {
  "productName": "★ M9 Bayonet | Scorched (Minimal Wear)",
  "id": 43126
  },
  {
  "productName": "★ M9 Bayonet | Slaughter (Factory New)",
  "id": 43128
  },
  {
  "productName": "★ M9 Bayonet | Slaughter (Field-Tested)",
  "id": 43129
  },
  {
  "productName": "★ M9 Bayonet | Slaughter (Minimal Wear)",
  "id": 43130
  },
  {
  "productName": "★ M9 Bayonet | Stained (Factory New)",
  "id": 43132
  },
  {
  "productName": "★ M9 Bayonet | Stained (Field-Tested)",
  "id": 43133
  },
  {
  "productName": "★ M9 Bayonet | Stained (Minimal Wear)",
  "id": 43134
  },
  {
  "productName": "★ M9 Bayonet | Tiger Tooth (Factory New)",
  "id": 43136
  },
  {
  "productName": "★ M9 Bayonet | Tiger Tooth (Minimal Wear)",
  "id": 43137
  },
  {
  "productName": "★ M9 Bayonet | Ultraviolet (Field-Tested)",
  "id": 43139
  },
  {
  "productName": "★ M9 Bayonet | Ultraviolet (Minimal Wear)",
  "id": 43140
  },
  {
  "productName": "★ M9 Bayonet | Urban Masked (Field-Tested)",
  "id": 43143
  },
  {
  "productName": "★ M9 Bayonet | Urban Masked (Minimal Wear)",
  "id": 43144
  },
  {
  "productName": "★ Moto Gloves | Boom! (Field-Tested)",
  "id": 43147
  },
  {
  "productName": "★ Moto Gloves | Boom! (Minimal Wear)",
  "id": 43148
  },
  {
  "productName": "★ Moto Gloves | Cool Mint (Field-Tested)",
  "id": 43151
  },
  {
  "productName": "★ Moto Gloves | Cool Mint (Minimal Wear)",
  "id": 43152
  },
  {
  "productName": "★ Moto Gloves | Eclipse (Field-Tested)",
  "id": 43155
  },
  {
  "productName": "★ Moto Gloves | Eclipse (Minimal Wear)",
  "id": 43156
  },
  {
  "productName": "★ Moto Gloves | Spearmint (Field-Tested)",
  "id": 43159
  },
  {
  "productName": "★ Moto Gloves | Spearmint (Minimal Wear)",
  "id": 43160
  },
  {
  "productName": "★ Shadow Daggers | Blue Steel (Factory New)",
  "id": 43164
  },
  {
  "productName": "★ Shadow Daggers | Blue Steel (Field-Tested)",
  "id": 43165
  },
  {
  "productName": "★ Shadow Daggers | Blue Steel (Minimal Wear)",
  "id": 43166
  },
  {
  "productName": "★ Shadow Daggers | Boreal Forest (Field-Tested)",
  "id": 43169
  },
  {
  "productName": "★ Shadow Daggers | Boreal Forest (Minimal Wear)",
  "id": 43170
  },
  {
  "productName": "★ Shadow Daggers | Case Hardened (Factory New)",
  "id": 43173
  },
  {
  "productName": "★ Shadow Daggers | Case Hardened (Field-Tested)",
  "id": 43174
  },
  {
  "productName": "★ Shadow Daggers | Case Hardened (Minimal Wear)",
  "id": 43175
  },
  {
  "productName": "★ Shadow Daggers | Crimson Web (Field-Tested)",
  "id": 43178
  },
  {
  "productName": "★ Shadow Daggers | Crimson Web (Minimal Wear)",
  "id": 43179
  },
  {
  "productName": "★ Shadow Daggers | Damascus Steel (Factory New)",
  "id": 43182
  },
  {
  "productName": "★ Shadow Daggers | Damascus Steel (Field-Tested)",
  "id": 43183
  },
  {
  "productName": "★ Shadow Daggers | Damascus Steel (Minimal Wear)",
  "id": 43184
  },
  {
  "productName": "★ Shadow Daggers | Doppler (Factory New)",
  "id": 43186
  },
  {
  "productName": "★ Shadow Daggers | Doppler (Minimal Wear)",
  "id": 43187
  },
  {
  "productName": "★ Shadow Daggers | Fade (Factory New)",
  "id": 43188
  },
  {
  "productName": "★ Shadow Daggers | Fade (Minimal Wear)",
  "id": 43189
  },
  {
  "productName": "★ Shadow Daggers | Forest DDPAT (Factory New)",
  "id": 43191
  },
  {
  "productName": "★ Shadow Daggers | Forest DDPAT (Field-Tested)",
  "id": 43192
  },
  {
  "productName": "★ Shadow Daggers | Forest DDPAT (Minimal Wear)",
  "id": 43193
  },
  {
  "productName": "★ Shadow Daggers | Marble Fade (Factory New)",
  "id": 43195
  },
  {
  "productName": "★ Shadow Daggers | Marble Fade (Minimal Wear)",
  "id": 43196
  },
  {
  "productName": "★ Shadow Daggers | Night (Field-Tested)",
  "id": 43198
  },
  {
  "productName": "★ Shadow Daggers | Night (Minimal Wear)",
  "id": 43199
  },
  {
  "productName": "★ Shadow Daggers | Safari Mesh (Factory New)",
  "id": 43204
  },
  {
  "productName": "★ Shadow Daggers | Safari Mesh (Field-Tested)",
  "id": 43205
  },
  {
  "productName": "★ Shadow Daggers | Safari Mesh (Minimal Wear)",
  "id": 43206
  },
  {
  "productName": "★ Shadow Daggers | Scorched (Field-Tested)",
  "id": 43209
  },
  {
  "productName": "★ Shadow Daggers | Scorched (Minimal Wear)",
  "id": 43210
  },
  {
  "productName": "★ Shadow Daggers | Slaughter (Factory New)",
  "id": 43212
  },
  {
  "productName": "★ Shadow Daggers | Slaughter (Field-Tested)",
  "id": 43213
  },
  {
  "productName": "★ Shadow Daggers | Slaughter (Minimal Wear)",
  "id": 43214
  },
  {
  "productName": "★ Shadow Daggers | Stained (Factory New)",
  "id": 43216
  },
  {
  "productName": "★ Shadow Daggers | Stained (Field-Tested)",
  "id": 43217
  },
  {
  "productName": "★ Shadow Daggers | Stained (Minimal Wear)",
  "id": 43218
  },
  {
  "productName": "★ Shadow Daggers | Tiger Tooth (Factory New)",
  "id": 43220
  },
  {
  "productName": "★ Shadow Daggers | Tiger Tooth (Minimal Wear)",
  "id": 43221
  },
  {
  "productName": "★ Shadow Daggers | Ultraviolet (Factory New)",
  "id": 43223
  },
  {
  "productName": "★ Shadow Daggers | Ultraviolet (Field-Tested)",
  "id": 43224
  },
  {
  "productName": "★ Shadow Daggers | Ultraviolet (Minimal Wear)",
  "id": 43225
  },
  {
  "productName": "★ Shadow Daggers | Urban Masked (Factory New)",
  "id": 43228
  },
  {
  "productName": "★ Shadow Daggers | Urban Masked (Field-Tested)",
  "id": 43229
  },
  {
  "productName": "★ Shadow Daggers | Urban Masked (Minimal Wear)",
  "id": 43230
  },
  {
  "productName": "★ Specialist Gloves | Crimson Kimono (Field-Tested)",
  "id": 43233
  },
  {
  "productName": "★ Specialist Gloves | Crimson Kimono (Minimal Wear)",
  "id": 43234
  },
  {
  "productName": "★ Specialist Gloves | Emerald Web (Field-Tested)",
  "id": 43237
  },
  {
  "productName": "★ Specialist Gloves | Emerald Web (Minimal Wear)",
  "id": 43238
  },
  {
  "productName": "★ Specialist Gloves | Forest DDPAT (Factory New)",
  "id": 43241
  },
  {
  "productName": "★ Specialist Gloves | Forest DDPAT (Field-Tested)",
  "id": 43242
  },
  {
  "productName": "★ Specialist Gloves | Forest DDPAT (Minimal Wear)",
  "id": 43243
  },
  {
  "productName": "★ Specialist Gloves | Foundation (Field-Tested)",
  "id": 43246
  },
  {
  "productName": "★ Specialist Gloves | Foundation (Minimal Wear)",
  "id": 43247
  },
  {
  "productName": "★ Sport Gloves | Arid (Factory New)",
  "id": 43250
  },
  {
  "productName": "★ Sport Gloves | Arid (Field-Tested)",
  "id": 43251
  },
  {
  "productName": "★ Sport Gloves | Arid (Minimal Wear)",
  "id": 43252
  },
  {
  "productName": "★ Sport Gloves | Hedge Maze (Field-Tested)",
  "id": 43255
  },
  {
  "productName": "★ Sport Gloves | Hedge Maze (Minimal Wear)",
  "id": 43256
  },
  {
  "productName": "★ Sport Gloves | Pandora's Box (Field-Tested)",
  "id": 43259
  },
  {
  "productName": "★ Sport Gloves | Superconductor (Field-Tested)",
  "id": 43262
  },
  {
  "productName": "★ Sport Gloves | Superconductor (Minimal Wear)",
  "id": 43263
  },
  {
  "productName": "★ StatTrak™ Bayonet | Autotronic (Field-Tested)",
  "id": 43266
  },
  {
  "productName": "★ StatTrak™ Bayonet | Autotronic (Minimal Wear)",
  "id": 43267
  },
  {
  "productName": "★ StatTrak™ Bayonet | Black Laminate (Field-Tested)",
  "id": 43269
  },
  {
  "productName": "★ StatTrak™ Bayonet | Black Laminate (Minimal Wear)",
  "id": 43270
  },
  {
  "productName": "★ StatTrak™ Bayonet | Blue Steel (Field-Tested)",
  "id": 43273
  },
  {
  "productName": "★ StatTrak™ Bayonet | Blue Steel (Minimal Wear)",
  "id": 43274
  },
  {
  "productName": "★ StatTrak™ Bayonet | Boreal Forest (Minimal Wear)",
  "id": 43277
  },
  {
  "productName": "★ StatTrak™ Bayonet | Bright Water (Factory New)",
  "id": 43279
  },
  {
  "productName": "★ StatTrak™ Bayonet | Bright Water (Field-Tested)",
  "id": 43280
  },
  {
  "productName": "★ StatTrak™ Bayonet | Bright Water (Minimal Wear)",
  "id": 43281
  },
  {
  "productName": "★ StatTrak™ Bayonet | Case Hardened (Field-Tested)",
  "id": 43282
  },
  {
  "productName": "★ StatTrak™ Bayonet | Case Hardened (Minimal Wear)",
  "id": 43283
  },
  {
  "productName": "★ StatTrak™ Bayonet | Crimson Web (Field-Tested)",
  "id": 43286
  },
  {
  "productName": "★ StatTrak™ Bayonet | Crimson Web (Minimal Wear)",
  "id": 43287
  },
  {
  "productName": "★ StatTrak™ Bayonet | Damascus Steel (Factory New)",
  "id": 43289
  },
  {
  "productName": "★ StatTrak™ Bayonet | Damascus Steel (Field-Tested)",
  "id": 43290
  },
  {
  "productName": "★ StatTrak™ Bayonet | Damascus Steel (Minimal Wear)",
  "id": 43291
  },
  {
  "productName": "★ StatTrak™ Bayonet | Doppler (Factory New)",
  "id": 43292
  },
  {
  "productName": "★ StatTrak™ Bayonet | Doppler (Minimal Wear)",
  "id": 43293
  },
  {
  "productName": "★ StatTrak™ Bayonet | Fade (Factory New)",
  "id": 43294
  },
  {
  "productName": "★ StatTrak™ Bayonet | Forest DDPAT (Field-Tested)",
  "id": 43296
  },
  {
  "productName": "★ StatTrak™ Bayonet | Forest DDPAT (Minimal Wear)",
  "id": 43297
  },
  {
  "productName": "★ StatTrak™ Bayonet | Freehand (Factory New)",
  "id": 43298
  },
  {
  "productName": "★ StatTrak™ Bayonet | Freehand (Field-Tested)",
  "id": 43299
  },
  {
  "productName": "★ StatTrak™ Bayonet | Freehand (Minimal Wear)",
  "id": 43300
  },
  {
  "productName": "★ StatTrak™ Bayonet | Gamma Doppler (Factory New)",
  "id": 43302
  },
  {
  "productName": "★ StatTrak™ Bayonet | Gamma Doppler (Minimal Wear)",
  "id": 43303
  },
  {
  "productName": "★ StatTrak™ Bayonet | Lore (Field-Tested)",
  "id": 43305
  },
  {
  "productName": "★ StatTrak™ Bayonet | Lore (Minimal Wear)",
  "id": 43306
  },
  {
  "productName": "★ StatTrak™ Bayonet | Marble Fade (Factory New)",
  "id": 43307
  },
  {
  "productName": "★ StatTrak™ Bayonet | Marble Fade (Minimal Wear)",
  "id": 43308
  },
  {
  "productName": "★ StatTrak™ Bayonet | Night (Field-Tested)",
  "id": 43310
  },
  {
  "productName": "★ StatTrak™ Bayonet | Night (Minimal Wear)",
  "id": 43311
  },
  {
  "productName": "★ StatTrak™ Bayonet | Safari Mesh (Field-Tested)",
  "id": 43315
  },
  {
  "productName": "★ StatTrak™ Bayonet | Safari Mesh (Minimal Wear)",
  "id": 43316
  },
  {
  "productName": "★ StatTrak™ Bayonet | Scorched (Field-Tested)",
  "id": 43318
  },
  {
  "productName": "★ StatTrak™ Bayonet | Scorched (Minimal Wear)",
  "id": 43319
  },
  {
  "productName": "★ StatTrak™ Bayonet | Slaughter (Factory New)",
  "id": 43320
  },
  {
  "productName": "★ StatTrak™ Bayonet | Slaughter (Field-Tested)",
  "id": 43321
  },
  {
  "productName": "★ StatTrak™ Bayonet | Slaughter (Minimal Wear)",
  "id": 43322
  },
  {
  "productName": "★ StatTrak™ Bayonet | Stained (Factory New)",
  "id": 43323
  },
  {
  "productName": "★ StatTrak™ Bayonet | Stained (Field-Tested)",
  "id": 43324
  },
  {
  "productName": "★ StatTrak™ Bayonet | Stained (Minimal Wear)",
  "id": 43325
  },
  {
  "productName": "★ StatTrak™ Bayonet | Tiger Tooth (Factory New)",
  "id": 43327
  },
  {
  "productName": "★ StatTrak™ Bayonet | Tiger Tooth (Minimal Wear)",
  "id": 43328
  },
  {
  "productName": "★ StatTrak™ Bayonet | Ultraviolet (Field-Tested)",
  "id": 43329
  },
  {
  "productName": "★ StatTrak™ Bayonet | Ultraviolet (Minimal Wear)",
  "id": 43330
  },
  {
  "productName": "★ StatTrak™ Bayonet | Urban Masked (Field-Tested)",
  "id": 43332
  },
  {
  "productName": "★ StatTrak™ Bayonet | Urban Masked (Minimal Wear)",
  "id": 43333
  },
  {
  "productName": "★ StatTrak™ Bowie Knife | Blue Steel (Field-Tested)",
  "id": 43336
  },
  {
  "productName": "★ StatTrak™ Bowie Knife | Blue Steel (Minimal Wear)",
  "id": 43337
  },
  {
  "productName": "★ StatTrak™ Bowie Knife | Boreal Forest (Field-Tested)",
  "id": 43338
  },
  {
  "productName": "★ StatTrak™ Bowie Knife | Case Hardened (Factory New)",
  "id": 43339
  },
  {
  "productName": "★ StatTrak™ Bowie Knife | Case Hardened (Field-Tested)",
  "id": 43340
  },
  {
  "productName": "★ StatTrak™ Bowie Knife | Case Hardened (Minimal Wear)",
  "id": 43341
  },
  {
  "productName": "★ StatTrak™ Bowie Knife | Crimson Web (Field-Tested)",
  "id": 43344
  },
  {
  "productName": "★ StatTrak™ Bowie Knife | Crimson Web (Minimal Wear)",
  "id": 43345
  },
  {
  "productName": "★ StatTrak™ Bowie Knife | Damascus Steel (Factory New)",
  "id": 43347
  },
  {
  "productName": "★ StatTrak™ Bowie Knife | Damascus Steel (Field-Tested)",
  "id": 43348
  },
  {
  "productName": "★ StatTrak™ Bowie Knife | Damascus Steel (Minimal Wear)",
  "id": 43349
  },
  {
  "productName": "★ StatTrak™ Bowie Knife | Doppler (Factory New)",
  "id": 43351
  },
  {
  "productName": "★ StatTrak™ Bowie Knife | Doppler (Minimal Wear)",
  "id": 43352
  },
  {
  "productName": "★ StatTrak™ Bowie Knife | Fade (Factory New)",
  "id": 43353
  },
  {
  "productName": "★ StatTrak™ Bowie Knife | Fade (Minimal Wear)",
  "id": 43354
  },
  {
  "productName": "★ StatTrak™ Bowie Knife | Forest DDPAT (Field-Tested)",
  "id": 43355
  },
  {
  "productName": "★ StatTrak™ Bowie Knife | Forest DDPAT (Minimal Wear)",
  "id": 43356
  },
  {
  "productName": "★ StatTrak™ Bowie Knife | Marble Fade (Factory New)",
  "id": 43357
  },
  {
  "productName": "★ StatTrak™ Bowie Knife | Marble Fade (Minimal Wear)",
  "id": 43358
  },
  {
  "productName": "★ StatTrak™ Bowie Knife | Night (Field-Tested)",
  "id": 43360
  },
  {
  "productName": "★ StatTrak™ Bowie Knife | Night (Minimal Wear)",
  "id": 43361
  },
  {
  "productName": "★ StatTrak™ Bowie Knife | Safari Mesh (Field-Tested)",
  "id": 43365
  },
  {
  "productName": "★ StatTrak™ Bowie Knife | Safari Mesh (Minimal Wear)",
  "id": 43366
  },
  {
  "productName": "★ StatTrak™ Bowie Knife | Scorched (Field-Tested)",
  "id": 43369
  },
  {
  "productName": "★ StatTrak™ Bowie Knife | Scorched (Minimal Wear)",
  "id": 43370
  },
  {
  "productName": "★ StatTrak™ Bowie Knife | Slaughter (Factory New)",
  "id": 43372
  },
  {
  "productName": "★ StatTrak™ Bowie Knife | Slaughter (Field-Tested)",
  "id": 43373
  },
  {
  "productName": "★ StatTrak™ Bowie Knife | Slaughter (Minimal Wear)",
  "id": 43374
  },
  {
  "productName": "★ StatTrak™ Bowie Knife | Stained (Factory New)",
  "id": 43376
  },
  {
  "productName": "★ StatTrak™ Bowie Knife | Stained (Field-Tested)",
  "id": 43377
  },
  {
  "productName": "★ StatTrak™ Bowie Knife | Stained (Minimal Wear)",
  "id": 43378
  },
  {
  "productName": "★ StatTrak™ Bowie Knife | Tiger Tooth (Factory New)",
  "id": 43379
  },
  {
  "productName": "★ StatTrak™ Bowie Knife | Tiger Tooth (Minimal Wear)",
  "id": 43380
  },
  {
  "productName": "★ StatTrak™ Bowie Knife | Ultraviolet (Field-Tested)",
  "id": 43381
  },
  {
  "productName": "★ StatTrak™ Bowie Knife | Ultraviolet (Minimal Wear)",
  "id": 43382
  },
  {
  "productName": "★ StatTrak™ Bowie Knife | Urban Masked (Factory New)",
  "id": 43385
  },
  {
  "productName": "★ StatTrak™ Bowie Knife | Urban Masked (Field-Tested)",
  "id": 43386
  },
  {
  "productName": "★ StatTrak™ Bowie Knife | Urban Masked (Minimal Wear)",
  "id": 43387
  },
  {
  "productName": "★ StatTrak™ Butterfly Knife | Blue Steel (Factory New)",
  "id": 43391
  },
  {
  "productName": "★ StatTrak™ Butterfly Knife | Blue Steel (Field-Tested)",
  "id": 43392
  },
  {
  "productName": "★ StatTrak™ Butterfly Knife | Blue Steel (Minimal Wear)",
  "id": 43393
  },
  {
  "productName": "★ StatTrak™ Butterfly Knife | Boreal Forest (Factory New)",
  "id": 43395
  },
  {
  "productName": "★ StatTrak™ Butterfly Knife | Boreal Forest (Field-Tested)",
  "id": 43396
  },
  {
  "productName": "★ StatTrak™ Butterfly Knife | Boreal Forest (Minimal Wear)",
  "id": 43397
  },
  {
  "productName": "★ StatTrak™ Butterfly Knife | Case Hardened (Field-Tested)",
  "id": 43399
  },
  {
  "productName": "★ StatTrak™ Butterfly Knife | Case Hardened (Minimal Wear)",
  "id": 43400
  },
  {
  "productName": "★ StatTrak™ Butterfly Knife | Crimson Web (Field-Tested)",
  "id": 43403
  },
  {
  "productName": "★ StatTrak™ Butterfly Knife | Crimson Web (Minimal Wear)",
  "id": 43404
  },
  {
  "productName": "★ StatTrak™ Butterfly Knife | Damascus Steel (Factory New)",
  "id": 43407
  },
  {
  "productName": "★ StatTrak™ Butterfly Knife | Damascus Steel (Field-Tested)",
  "id": 43408
  },
  {
  "productName": "★ StatTrak™ Butterfly Knife | Damascus Steel (Minimal Wear)",
  "id": 43409
  },
  {
  "productName": "★ StatTrak™ Butterfly Knife | Doppler (Factory New)",
  "id": 43411
  },
  {
  "productName": "★ StatTrak™ Butterfly Knife | Fade (Factory New)",
  "id": 43412
  },
  {
  "productName": "★ StatTrak™ Butterfly Knife | Fade (Minimal Wear)",
  "id": 43413
  },
  {
  "productName": "★ StatTrak™ Butterfly Knife | Forest DDPAT (Field-Tested)",
  "id": 43415
  },
  {
  "productName": "★ StatTrak™ Butterfly Knife | Forest DDPAT (Minimal Wear)",
  "id": 43416
  },
  {
  "productName": "★ StatTrak™ Butterfly Knife | Marble Fade (Factory New)",
  "id": 43418
  },
  {
  "productName": "★ StatTrak™ Butterfly Knife | Marble Fade (Minimal Wear)",
  "id": 43419
  },
  {
  "productName": "★ StatTrak™ Butterfly Knife | Night (Field-Tested)",
  "id": 43421
  },
  {
  "productName": "★ StatTrak™ Butterfly Knife | Night (Minimal Wear)",
  "id": 43422
  },
  {
  "productName": "★ StatTrak™ Butterfly Knife | Safari Mesh (Field-Tested)",
  "id": 43425
  },
  {
  "productName": "★ StatTrak™ Butterfly Knife | Safari Mesh (Minimal Wear)",
  "id": 43426
  },
  {
  "productName": "★ StatTrak™ Butterfly Knife | Scorched (Field-Tested)",
  "id": 43427
  },
  {
  "productName": "★ StatTrak™ Butterfly Knife | Scorched (Minimal Wear)",
  "id": 43428
  },
  {
  "productName": "★ StatTrak™ Butterfly Knife | Slaughter (Factory New)",
  "id": 43429
  },
  {
  "productName": "★ StatTrak™ Butterfly Knife | Slaughter (Field-Tested)",
  "id": 43430
  },
  {
  "productName": "★ StatTrak™ Butterfly Knife | Slaughter (Minimal Wear)",
  "id": 43431
  },
  {
  "productName": "★ StatTrak™ Butterfly Knife | Stained (Field-Tested)",
  "id": 43432
  },
  {
  "productName": "★ StatTrak™ Butterfly Knife | Stained (Minimal Wear)",
  "id": 43433
  },
  {
  "productName": "★ StatTrak™ Butterfly Knife | Tiger Tooth (Factory New)",
  "id": 43435
  },
  {
  "productName": "★ StatTrak™ Butterfly Knife | Ultraviolet (Field-Tested)",
  "id": 43437
  },
  {
  "productName": "★ StatTrak™ Butterfly Knife | Ultraviolet (Minimal Wear)",
  "id": 43438
  },
  {
  "productName": "★ StatTrak™ Butterfly Knife | Urban Masked (Field-Tested)",
  "id": 43440
  },
  {
  "productName": "★ StatTrak™ Butterfly Knife | Urban Masked (Minimal Wear)",
  "id": 43441
  },
  {
  "productName": "★ StatTrak™ Falchion Knife | Blue Steel (Field-Tested)",
  "id": 43445
  },
  {
  "productName": "★ StatTrak™ Falchion Knife | Blue Steel (Minimal Wear)",
  "id": 43446
  },
  {
  "productName": "★ StatTrak™ Falchion Knife | Boreal Forest (Field-Tested)",
  "id": 43449
  },
  {
  "productName": "★ StatTrak™ Falchion Knife | Boreal Forest (Minimal Wear)",
  "id": 43450
  },
  {
  "productName": "★ StatTrak™ Falchion Knife | Case Hardened (Factory New)",
  "id": 43453
  },
  {
  "productName": "★ StatTrak™ Falchion Knife | Case Hardened (Field-Tested)",
  "id": 43454
  },
  {
  "productName": "★ StatTrak™ Falchion Knife | Case Hardened (Minimal Wear)",
  "id": 43455
  },
  {
  "productName": "★ StatTrak™ Falchion Knife | Crimson Web (Field-Tested)",
  "id": 43458
  },
  {
  "productName": "★ StatTrak™ Falchion Knife | Crimson Web (Minimal Wear)",
  "id": 43459
  },
  {
  "productName": "★ StatTrak™ Falchion Knife | Damascus Steel (Factory New)",
  "id": 43461
  },
  {
  "productName": "★ StatTrak™ Falchion Knife | Damascus Steel (Field-Tested)",
  "id": 43462
  },
  {
  "productName": "★ StatTrak™ Falchion Knife | Damascus Steel (Minimal Wear)",
  "id": 43463
  },
  {
  "productName": "★ StatTrak™ Falchion Knife | Doppler (Factory New)",
  "id": 43464
  },
  {
  "productName": "★ StatTrak™ Falchion Knife | Fade (Factory New)",
  "id": 43465
  },
  {
  "productName": "★ StatTrak™ Falchion Knife | Fade (Minimal Wear)",
  "id": 43466
  },
  {
  "productName": "★ StatTrak™ Falchion Knife | Forest DDPAT (Field-Tested)",
  "id": 43468
  },
  {
  "productName": "★ StatTrak™ Falchion Knife | Forest DDPAT (Minimal Wear)",
  "id": 43469
  },
  {
  "productName": "★ StatTrak™ Falchion Knife | Marble Fade (Factory New)",
  "id": 43470
  },
  {
  "productName": "★ StatTrak™ Falchion Knife | Night (Field-Tested)",
  "id": 43472
  },
  {
  "productName": "★ StatTrak™ Falchion Knife | Safari Mesh (Field-Tested)",
  "id": 43476
  },
  {
  "productName": "★ StatTrak™ Falchion Knife | Safari Mesh (Minimal Wear)",
  "id": 43477
  },
  {
  "productName": "★ StatTrak™ Falchion Knife | Scorched (Field-Tested)",
  "id": 43480
  },
  {
  "productName": "★ StatTrak™ Falchion Knife | Scorched (Minimal Wear)",
  "id": 43481
  },
  {
  "productName": "★ StatTrak™ Falchion Knife | Slaughter (Factory New)",
  "id": 43483
  },
  {
  "productName": "★ StatTrak™ Falchion Knife | Slaughter (Field-Tested)",
  "id": 43484
  },
  {
  "productName": "★ StatTrak™ Falchion Knife | Slaughter (Minimal Wear)",
  "id": 43485
  },
  {
  "productName": "★ StatTrak™ Falchion Knife | Stained (Factory New)",
  "id": 43487
  },
  {
  "productName": "★ StatTrak™ Falchion Knife | Stained (Field-Tested)",
  "id": 43488
  },
  {
  "productName": "★ StatTrak™ Falchion Knife | Stained (Minimal Wear)",
  "id": 43489
  },
  {
  "productName": "★ StatTrak™ Falchion Knife | Tiger Tooth (Factory New)",
  "id": 43491
  },
  {
  "productName": "★ StatTrak™ Falchion Knife | Ultraviolet (Field-Tested)",
  "id": 43492
  },
  {
  "productName": "★ StatTrak™ Falchion Knife | Urban Masked (Field-Tested)",
  "id": 43494
  },
  {
  "productName": "★ StatTrak™ Falchion Knife | Urban Masked (Minimal Wear)",
  "id": 43495
  },
  {
  "productName": "★ StatTrak™ Flip Knife | Autotronic (Field-Tested)",
  "id": 43498
  },
  {
  "productName": "★ StatTrak™ Flip Knife | Autotronic (Minimal Wear)",
  "id": 43499
  },
  {
  "productName": "★ StatTrak™ Flip Knife | Black Laminate (Field-Tested)",
  "id": 43501
  },
  {
  "productName": "★ StatTrak™ Flip Knife | Black Laminate (Minimal Wear)",
  "id": 43502
  },
  {
  "productName": "★ StatTrak™ Flip Knife | Blue Steel (Factory New)",
  "id": 43505
  },
  {
  "productName": "★ StatTrak™ Flip Knife | Blue Steel (Field-Tested)",
  "id": 43506
  },
  {
  "productName": "★ StatTrak™ Flip Knife | Blue Steel (Minimal Wear)",
  "id": 43507
  },
  {
  "productName": "★ StatTrak™ Flip Knife | Boreal Forest (Field-Tested)",
  "id": 43510
  },
  {
  "productName": "★ StatTrak™ Flip Knife | Bright Water (Factory New)",
  "id": 43512
  },
  {
  "productName": "★ StatTrak™ Flip Knife | Bright Water (Field-Tested)",
  "id": 43513
  },
  {
  "productName": "★ StatTrak™ Flip Knife | Bright Water (Minimal Wear)",
  "id": 43514
  },
  {
  "productName": "★ StatTrak™ Flip Knife | Case Hardened (Factory New)",
  "id": 43517
  },
  {
  "productName": "★ StatTrak™ Flip Knife | Case Hardened (Field-Tested)",
  "id": 43518
  },
  {
  "productName": "★ StatTrak™ Flip Knife | Case Hardened (Minimal Wear)",
  "id": 43519
  },
  {
  "productName": "★ StatTrak™ Flip Knife | Crimson Web (Field-Tested)",
  "id": 43522
  },
  {
  "productName": "★ StatTrak™ Flip Knife | Crimson Web (Minimal Wear)",
  "id": 43523
  },
  {
  "productName": "★ StatTrak™ Flip Knife | Damascus Steel (Factory New)",
  "id": 43524
  },
  {
  "productName": "★ StatTrak™ Flip Knife | Damascus Steel (Field-Tested)",
  "id": 43525
  },
  {
  "productName": "★ StatTrak™ Flip Knife | Damascus Steel (Minimal Wear)",
  "id": 43526
  },
  {
  "productName": "★ StatTrak™ Flip Knife | Doppler (Factory New)",
  "id": 43528
  },
  {
  "productName": "★ StatTrak™ Flip Knife | Doppler (Minimal Wear)",
  "id": 43529
  },
  {
  "productName": "★ StatTrak™ Flip Knife | Fade (Factory New)",
  "id": 43530
  },
  {
  "productName": "★ StatTrak™ Flip Knife | Fade (Minimal Wear)",
  "id": 43531
  },
  {
  "productName": "★ StatTrak™ Flip Knife | Forest DDPAT (Field-Tested)",
  "id": 43533
  },
  {
  "productName": "★ StatTrak™ Flip Knife | Forest DDPAT (Minimal Wear)",
  "id": 43534
  },
  {
  "productName": "★ StatTrak™ Flip Knife | Freehand (Factory New)",
  "id": 43536
  },
  {
  "productName": "★ StatTrak™ Flip Knife | Freehand (Field-Tested)",
  "id": 43537
  },
  {
  "productName": "★ StatTrak™ Flip Knife | Gamma Doppler (Factory New)",
  "id": 43538
  },
  {
  "productName": "★ StatTrak™ Flip Knife | Gamma Doppler (Minimal Wear)",
  "id": 43539
  },
  {
  "productName": "★ StatTrak™ Flip Knife | Lore (Factory New)",
  "id": 43541
  },
  {
  "productName": "★ StatTrak™ Flip Knife | Lore (Field-Tested)",
  "id": 43542
  },
  {
  "productName": "★ StatTrak™ Flip Knife | Lore (Minimal Wear)",
  "id": 43543
  },
  {
  "productName": "★ StatTrak™ Flip Knife | Marble Fade (Factory New)",
  "id": 43544
  },
  {
  "productName": "★ StatTrak™ Flip Knife | Marble Fade (Minimal Wear)",
  "id": 43545
  },
  {
  "productName": "★ StatTrak™ Flip Knife | Night (Field-Tested)",
  "id": 43547
  },
  {
  "productName": "★ StatTrak™ Flip Knife | Night (Minimal Wear)",
  "id": 43548
  },
  {
  "productName": "★ StatTrak™ Flip Knife | Safari Mesh (Field-Tested)",
  "id": 43551
  },
  {
  "productName": "★ StatTrak™ Flip Knife | Safari Mesh (Minimal Wear)",
  "id": 43552
  },
  {
  "productName": "★ StatTrak™ Flip Knife | Scorched (Field-Tested)",
  "id": 43555
  },
  {
  "productName": "★ StatTrak™ Flip Knife | Scorched (Minimal Wear)",
  "id": 43556
  },
  {
  "productName": "★ StatTrak™ Flip Knife | Slaughter (Factory New)",
  "id": 43558
  },
  {
  "productName": "★ StatTrak™ Flip Knife | Slaughter (Field-Tested)",
  "id": 43559
  },
  {
  "productName": "★ StatTrak™ Flip Knife | Slaughter (Minimal Wear)",
  "id": 43560
  },
  {
  "productName": "★ StatTrak™ Flip Knife | Stained (Factory New)",
  "id": 43562
  },
  {
  "productName": "★ StatTrak™ Flip Knife | Stained (Field-Tested)",
  "id": 43563
  },
  {
  "productName": "★ StatTrak™ Flip Knife | Tiger Tooth (Factory New)",
  "id": 43565
  },
  {
  "productName": "★ StatTrak™ Flip Knife | Tiger Tooth (Minimal Wear)",
  "id": 43566
  },
  {
  "productName": "★ StatTrak™ Flip Knife | Ultraviolet (Field-Tested)",
  "id": 43568
  },
  {
  "productName": "★ StatTrak™ Flip Knife | Ultraviolet (Minimal Wear)",
  "id": 43569
  },
  {
  "productName": "★ StatTrak™ Flip Knife | Urban Masked (Field-Tested)",
  "id": 43572
  },
  {
  "productName": "★ StatTrak™ Flip Knife | Urban Masked (Minimal Wear)",
  "id": 43573
  },
  {
  "productName": "★ StatTrak™ Gut Knife | Autotronic (Factory New)",
  "id": 43577
  },
  {
  "productName": "★ StatTrak™ Gut Knife | Autotronic (Field-Tested)",
  "id": 43578
  },
  {
  "productName": "★ StatTrak™ Gut Knife | Autotronic (Minimal Wear)",
  "id": 43579
  },
  {
  "productName": "★ StatTrak™ Gut Knife | Black Laminate (Factory New)",
  "id": 43581
  },
  {
  "productName": "★ StatTrak™ Gut Knife | Black Laminate (Field-Tested)",
  "id": 43582
  },
  {
  "productName": "★ StatTrak™ Gut Knife | Black Laminate (Minimal Wear)",
  "id": 43583
  },
  {
  "productName": "★ StatTrak™ Gut Knife | Blue Steel (Factory New)",
  "id": 43586
  },
  {
  "productName": "★ StatTrak™ Gut Knife | Blue Steel (Field-Tested)",
  "id": 43587
  },
  {
  "productName": "★ StatTrak™ Gut Knife | Blue Steel (Minimal Wear)",
  "id": 43588
  },
  {
  "productName": "★ StatTrak™ Gut Knife | Boreal Forest (Field-Tested)",
  "id": 43590
  },
  {
  "productName": "★ StatTrak™ Gut Knife | Boreal Forest (Minimal Wear)",
  "id": 43591
  },
  {
  "productName": "★ StatTrak™ Gut Knife | Bright Water (Factory New)",
  "id": 43592
  },
  {
  "productName": "★ StatTrak™ Gut Knife | Bright Water (Minimal Wear)",
  "id": 43593
  },
  {
  "productName": "★ StatTrak™ Gut Knife | Case Hardened (Factory New)",
  "id": 43595
  },
  {
  "productName": "★ StatTrak™ Gut Knife | Case Hardened (Field-Tested)",
  "id": 43596
  },
  {
  "productName": "★ StatTrak™ Gut Knife | Case Hardened (Minimal Wear)",
  "id": 43597
  },
  {
  "productName": "★ StatTrak™ Gut Knife | Crimson Web (Field-Tested)",
  "id": 43600
  },
  {
  "productName": "★ StatTrak™ Gut Knife | Crimson Web (Minimal Wear)",
  "id": 43601
  },
  {
  "productName": "★ StatTrak™ Gut Knife | Damascus Steel (Factory New)",
  "id": 43604
  },
  {
  "productName": "★ StatTrak™ Gut Knife | Damascus Steel (Field-Tested)",
  "id": 43605
  },
  {
  "productName": "★ StatTrak™ Gut Knife | Damascus Steel (Minimal Wear)",
  "id": 43606
  },
  {
  "productName": "★ StatTrak™ Gut Knife | Doppler (Factory New)",
  "id": 43607
  },
  {
  "productName": "★ StatTrak™ Gut Knife | Doppler (Minimal Wear)",
  "id": 43608
  },
  {
  "productName": "★ StatTrak™ Gut Knife | Fade (Factory New)",
  "id": 43609
  },
  {
  "productName": "★ StatTrak™ Gut Knife | Forest DDPAT (Field-Tested)",
  "id": 43611
  },
  {
  "productName": "★ StatTrak™ Gut Knife | Forest DDPAT (Minimal Wear)",
  "id": 43612
  },
  {
  "productName": "★ StatTrak™ Gut Knife | Freehand (Factory New)",
  "id": 43614
  },
  {
  "productName": "★ StatTrak™ Gut Knife | Freehand (Field-Tested)",
  "id": 43615
  },
  {
  "productName": "★ StatTrak™ Gut Knife | Freehand (Minimal Wear)",
  "id": 43616
  },
  {
  "productName": "★ StatTrak™ Gut Knife | Gamma Doppler (Factory New)",
  "id": 43617
  },
  {
  "productName": "★ StatTrak™ Gut Knife | Lore (Factory New)",
  "id": 43619
  },
  {
  "productName": "★ StatTrak™ Gut Knife | Lore (Field-Tested)",
  "id": 43620
  },
  {
  "productName": "★ StatTrak™ Gut Knife | Marble Fade (Factory New)",
  "id": 43621
  },
  {
  "productName": "★ StatTrak™ Gut Knife | Marble Fade (Minimal Wear)",
  "id": 43622
  },
  {
  "productName": "★ StatTrak™ Gut Knife | Night (Field-Tested)",
  "id": 43624
  },
  {
  "productName": "★ StatTrak™ Gut Knife | Night (Minimal Wear)",
  "id": 43625
  },
  {
  "productName": "★ StatTrak™ Gut Knife | Safari Mesh (Field-Tested)",
  "id": 43628
  },
  {
  "productName": "★ StatTrak™ Gut Knife | Safari Mesh (Minimal Wear)",
  "id": 43629
  },
  {
  "productName": "★ StatTrak™ Gut Knife | Scorched (Field-Tested)",
  "id": 43632
  },
  {
  "productName": "★ StatTrak™ Gut Knife | Scorched (Minimal Wear)",
  "id": 43633
  },
  {
  "productName": "★ StatTrak™ Gut Knife | Slaughter (Factory New)",
  "id": 43635
  },
  {
  "productName": "★ StatTrak™ Gut Knife | Slaughter (Field-Tested)",
  "id": 43636
  },
  {
  "productName": "★ StatTrak™ Gut Knife | Slaughter (Minimal Wear)",
  "id": 43637
  },
  {
  "productName": "★ StatTrak™ Gut Knife | Stained (Factory New)",
  "id": 43639
  },
  {
  "productName": "★ StatTrak™ Gut Knife | Stained (Field-Tested)",
  "id": 43640
  },
  {
  "productName": "★ StatTrak™ Gut Knife | Stained (Minimal Wear)",
  "id": 43641
  },
  {
  "productName": "★ StatTrak™ Gut Knife | Tiger Tooth (Factory New)",
  "id": 43643
  },
  {
  "productName": "★ StatTrak™ Gut Knife | Ultraviolet (Field-Tested)",
  "id": 43644
  },
  {
  "productName": "★ StatTrak™ Gut Knife | Ultraviolet (Minimal Wear)",
  "id": 43645
  },
  {
  "productName": "★ StatTrak™ Gut Knife | Urban Masked (Field-Tested)",
  "id": 43646
  },
  {
  "productName": "★ StatTrak™ Gut Knife | Urban Masked (Minimal Wear)",
  "id": 43647
  },
  {
  "productName": "★ StatTrak™ Huntsman Knife | Blue Steel (Field-Tested)",
  "id": 43650
  },
  {
  "productName": "★ StatTrak™ Huntsman Knife | Blue Steel (Minimal Wear)",
  "id": 43651
  },
  {
  "productName": "★ StatTrak™ Huntsman Knife | Boreal Forest (Field-Tested)",
  "id": 43654
  },
  {
  "productName": "★ StatTrak™ Huntsman Knife | Boreal Forest (Minimal Wear)",
  "id": 43655
  },
  {
  "productName": "★ StatTrak™ Huntsman Knife | Case Hardened (Field-Tested)",
  "id": 43658
  },
  {
  "productName": "★ StatTrak™ Huntsman Knife | Case Hardened (Minimal Wear)",
  "id": 43659
  },
  {
  "productName": "★ StatTrak™ Huntsman Knife | Crimson Web (Field-Tested)",
  "id": 43662
  },
  {
  "productName": "★ StatTrak™ Huntsman Knife | Crimson Web (Minimal Wear)",
  "id": 43663
  },
  {
  "productName": "★ StatTrak™ Huntsman Knife | Damascus Steel (Factory New)",
  "id": 43664
  },
  {
  "productName": "★ StatTrak™ Huntsman Knife | Damascus Steel (Field-Tested)",
  "id": 43665
  },
  {
  "productName": "★ StatTrak™ Huntsman Knife | Damascus Steel (Minimal Wear)",
  "id": 43666
  },
  {
  "productName": "★ StatTrak™ Huntsman Knife | Doppler (Factory New)",
  "id": 43667
  },
  {
  "productName": "★ StatTrak™ Huntsman Knife | Doppler (Minimal Wear)",
  "id": 43668
  },
  {
  "productName": "★ StatTrak™ Huntsman Knife | Fade (Factory New)",
  "id": 43669
  },
  {
  "productName": "★ StatTrak™ Huntsman Knife | Fade (Minimal Wear)",
  "id": 43670
  },
  {
  "productName": "★ StatTrak™ Huntsman Knife | Forest DDPAT (Field-Tested)",
  "id": 43672
  },
  {
  "productName": "★ StatTrak™ Huntsman Knife | Marble Fade (Factory New)",
  "id": 43674
  },
  {
  "productName": "★ StatTrak™ Huntsman Knife | Marble Fade (Minimal Wear)",
  "id": 43675
  },
  {
  "productName": "★ StatTrak™ Huntsman Knife | Night (Factory New)",
  "id": 43677
  },
  {
  "productName": "★ StatTrak™ Huntsman Knife | Night (Field-Tested)",
  "id": 43678
  },
  {
  "productName": "★ StatTrak™ Huntsman Knife | Night (Minimal Wear)",
  "id": 43679
  },
  {
  "productName": "★ StatTrak™ Huntsman Knife | Safari Mesh (Field-Tested)",
  "id": 43682
  },
  {
  "productName": "★ StatTrak™ Huntsman Knife | Safari Mesh (Minimal Wear)",
  "id": 43683
  },
  {
  "productName": "★ StatTrak™ Huntsman Knife | Scorched (Field-Tested)",
  "id": 43685
  },
  {
  "productName": "★ StatTrak™ Huntsman Knife | Scorched (Minimal Wear)",
  "id": 43686
  },
  {
  "productName": "★ StatTrak™ Huntsman Knife | Slaughter (Factory New)",
  "id": 43688
  },
  {
  "productName": "★ StatTrak™ Huntsman Knife | Slaughter (Minimal Wear)",
  "id": 43689
  },
  {
  "productName": "★ StatTrak™ Huntsman Knife | Stained (Field-Tested)",
  "id": 43691
  },
  {
  "productName": "★ StatTrak™ Huntsman Knife | Stained (Minimal Wear)",
  "id": 43692
  },
  {
  "productName": "★ StatTrak™ Huntsman Knife | Tiger Tooth (Factory New)",
  "id": 43694
  },
  {
  "productName": "★ StatTrak™ Huntsman Knife | Ultraviolet (Field-Tested)",
  "id": 43695
  },
  {
  "productName": "★ StatTrak™ Huntsman Knife | Urban Masked (Field-Tested)",
  "id": 43698
  },
  {
  "productName": "★ StatTrak™ Huntsman Knife | Urban Masked (Minimal Wear)",
  "id": 43699
  },
  {
  "productName": "★ StatTrak™ Karambit | Autotronic (Field-Tested)",
  "id": 43703
  },
  {
  "productName": "★ StatTrak™ Karambit | Autotronic (Minimal Wear)",
  "id": 43704
  },
  {
  "productName": "★ StatTrak™ Karambit | Black Laminate (Field-Tested)",
  "id": 43706
  },
  {
  "productName": "★ StatTrak™ Karambit | Black Laminate (Minimal Wear)",
  "id": 43707
  },
  {
  "productName": "★ StatTrak™ Karambit | Blue Steel (Field-Tested)",
  "id": 43710
  },
  {
  "productName": "★ StatTrak™ Karambit | Blue Steel (Minimal Wear)",
  "id": 43711
  },
  {
  "productName": "★ StatTrak™ Karambit | Boreal Forest (Field-Tested)",
  "id": 43714
  },
  {
  "productName": "★ StatTrak™ Karambit | Boreal Forest (Minimal Wear)",
  "id": 43715
  },
  {
  "productName": "★ StatTrak™ Karambit | Bright Water (Factory New)",
  "id": 43718
  },
  {
  "productName": "★ StatTrak™ Karambit | Bright Water (Field-Tested)",
  "id": 43719
  },
  {
  "productName": "★ StatTrak™ Karambit | Bright Water (Minimal Wear)",
  "id": 43720
  },
  {
  "productName": "★ StatTrak™ Karambit | Case Hardened (Field-Tested)",
  "id": 43722
  },
  {
  "productName": "★ StatTrak™ Karambit | Case Hardened (Minimal Wear)",
  "id": 43723
  },
  {
  "productName": "★ StatTrak™ Karambit | Crimson Web (Field-Tested)",
  "id": 43726
  },
  {
  "productName": "★ StatTrak™ Karambit | Crimson Web (Minimal Wear)",
  "id": 43727
  },
  {
  "productName": "★ StatTrak™ Karambit | Damascus Steel (Factory New)",
  "id": 43728
  },
  {
  "productName": "★ StatTrak™ Karambit | Damascus Steel (Field-Tested)",
  "id": 43729
  },
  {
  "productName": "★ StatTrak™ Karambit | Damascus Steel (Minimal Wear)",
  "id": 43730
  },
  {
  "productName": "★ StatTrak™ Karambit | Doppler (Factory New)",
  "id": 43731
  },
  {
  "productName": "★ StatTrak™ Karambit | Doppler (Minimal Wear)",
  "id": 43732
  },
  {
  "productName": "★ StatTrak™ Karambit | Fade (Factory New)",
  "id": 43733
  },
  {
  "productName": "★ StatTrak™ Karambit | Fade (Minimal Wear)",
  "id": 43734
  },
  {
  "productName": "★ StatTrak™ Karambit | Forest DDPAT (Field-Tested)",
  "id": 43736
  },
  {
  "productName": "★ StatTrak™ Karambit | Forest DDPAT (Minimal Wear)",
  "id": 43737
  },
  {
  "productName": "★ StatTrak™ Karambit | Freehand (Factory New)",
  "id": 43739
  },
  {
  "productName": "★ StatTrak™ Karambit | Freehand (Field-Tested)",
  "id": 43740
  },
  {
  "productName": "★ StatTrak™ Karambit | Freehand (Minimal Wear)",
  "id": 43741
  },
  {
  "productName": "★ StatTrak™ Karambit | Gamma Doppler (Factory New)",
  "id": 43742
  },
  {
  "productName": "★ StatTrak™ Karambit | Lore (Factory New)",
  "id": 43743
  },
  {
  "productName": "★ StatTrak™ Karambit | Lore (Field-Tested)",
  "id": 43744
  },
  {
  "productName": "★ StatTrak™ Karambit | Lore (Minimal Wear)",
  "id": 43745
  },
  {
  "productName": "★ StatTrak™ Karambit | Marble Fade (Factory New)",
  "id": 43746
  },
  {
  "productName": "★ StatTrak™ Karambit | Night (Field-Tested)",
  "id": 43748
  },
  {
  "productName": "★ StatTrak™ Karambit | Night (Minimal Wear)",
  "id": 43749
  },
  {
  "productName": "★ StatTrak™ Karambit | Safari Mesh (Field-Tested)",
  "id": 43752
  },
  {
  "productName": "★ StatTrak™ Karambit | Safari Mesh (Minimal Wear)",
  "id": 43753
  },
  {
  "productName": "★ StatTrak™ Karambit | Scorched (Field-Tested)",
  "id": 43756
  },
  {
  "productName": "★ StatTrak™ Karambit | Scorched (Minimal Wear)",
  "id": 43757
  },
  {
  "productName": "★ StatTrak™ Karambit | Slaughter (Factory New)",
  "id": 43758
  },
  {
  "productName": "★ StatTrak™ Karambit | Slaughter (Field-Tested)",
  "id": 43759
  },
  {
  "productName": "★ StatTrak™ Karambit | Slaughter (Minimal Wear)",
  "id": 43760
  },
  {
  "productName": "★ StatTrak™ Karambit | Stained (Factory New)",
  "id": 43762
  },
  {
  "productName": "★ StatTrak™ Karambit | Stained (Field-Tested)",
  "id": 43763
  },
  {
  "productName": "★ StatTrak™ Karambit | Stained (Minimal Wear)",
  "id": 43764
  },
  {
  "productName": "★ StatTrak™ Karambit | Tiger Tooth (Factory New)",
  "id": 43766
  },
  {
  "productName": "★ StatTrak™ Karambit | Ultraviolet (Field-Tested)",
  "id": 43768
  },
  {
  "productName": "★ StatTrak™ Karambit | Ultraviolet (Minimal Wear)",
  "id": 43769
  },
  {
  "productName": "★ StatTrak™ Karambit | Urban Masked (Field-Tested)",
  "id": 43772
  },
  {
  "productName": "★ StatTrak™ Karambit | Urban Masked (Minimal Wear)",
  "id": 43773
  },
  {
  "productName": "★ StatTrak™ M9 Bayonet | Autotronic (Factory New)",
  "id": 43776
  },
  {
  "productName": "★ StatTrak™ M9 Bayonet | Autotronic (Field-Tested)",
  "id": 43777
  },
  {
  "productName": "★ StatTrak™ M9 Bayonet | Autotronic (Minimal Wear)",
  "id": 43778
  },
  {
  "productName": "★ StatTrak™ M9 Bayonet | Black Laminate (Field-Tested)",
  "id": 43780
  },
  {
  "productName": "★ StatTrak™ M9 Bayonet | Black Laminate (Minimal Wear)",
  "id": 43781
  },
  {
  "productName": "★ StatTrak™ M9 Bayonet | Blue Steel (Field-Tested)",
  "id": 43784
  },
  {
  "productName": "★ StatTrak™ M9 Bayonet | Blue Steel (Minimal Wear)",
  "id": 43785
  },
  {
  "productName": "★ StatTrak™ M9 Bayonet | Boreal Forest (Minimal Wear)",
  "id": 43788
  },
  {
  "productName": "★ StatTrak™ M9 Bayonet | Bright Water (Factory New)",
  "id": 43789
  },
  {
  "productName": "★ StatTrak™ M9 Bayonet | Bright Water (Field-Tested)",
  "id": 43790
  },
  {
  "productName": "★ StatTrak™ M9 Bayonet | Bright Water (Minimal Wear)",
  "id": 43791
  },
  {
  "productName": "★ StatTrak™ M9 Bayonet | Case Hardened (Field-Tested)",
  "id": 43793
  },
  {
  "productName": "★ StatTrak™ M9 Bayonet | Case Hardened (Minimal Wear)",
  "id": 43794
  },
  {
  "productName": "★ StatTrak™ M9 Bayonet | Crimson Web (Field-Tested)",
  "id": 43796
  },
  {
  "productName": "★ StatTrak™ M9 Bayonet | Damascus Steel (Factory New)",
  "id": 43799
  },
  {
  "productName": "★ StatTrak™ M9 Bayonet | Damascus Steel (Field-Tested)",
  "id": 43800
  },
  {
  "productName": "★ StatTrak™ M9 Bayonet | Damascus Steel (Minimal Wear)",
  "id": 43801
  },
  {
  "productName": "★ StatTrak™ M9 Bayonet | Doppler (Factory New)",
  "id": 43803
  },
  {
  "productName": "★ StatTrak™ M9 Bayonet | Doppler (Minimal Wear)",
  "id": 43804
  },
  {
  "productName": "★ StatTrak™ M9 Bayonet | Fade (Factory New)",
  "id": 43805
  },
  {
  "productName": "★ StatTrak™ M9 Bayonet | Forest DDPAT (Field-Tested)",
  "id": 43806
  },
  {
  "productName": "★ StatTrak™ M9 Bayonet | Forest DDPAT (Minimal Wear)",
  "id": 43807
  },
  {
  "productName": "★ StatTrak™ M9 Bayonet | Freehand (Factory New)",
  "id": 43809
  },
  {
  "productName": "★ StatTrak™ M9 Bayonet | Freehand (Field-Tested)",
  "id": 43810
  },
  {
  "productName": "★ StatTrak™ M9 Bayonet | Freehand (Minimal Wear)",
  "id": 43811
  },
  {
  "productName": "★ StatTrak™ M9 Bayonet | Gamma Doppler (Factory New)",
  "id": 43812
  },
  {
  "productName": "★ StatTrak™ M9 Bayonet | Gamma Doppler (Minimal Wear)",
  "id": 43813
  },
  {
  "productName": "★ StatTrak™ M9 Bayonet | Lore (Factory New)",
  "id": 43815
  },
  {
  "productName": "★ StatTrak™ M9 Bayonet | Lore (Field-Tested)",
  "id": 43816
  },
  {
  "productName": "★ StatTrak™ M9 Bayonet | Marble Fade (Factory New)",
  "id": 43818
  },
  {
  "productName": "★ StatTrak™ M9 Bayonet | Marble Fade (Minimal Wear)",
  "id": 43819
  },
  {
  "productName": "★ StatTrak™ M9 Bayonet | Night (Field-Tested)",
  "id": 43821
  },
  {
  "productName": "★ StatTrak™ M9 Bayonet | Night (Minimal Wear)",
  "id": 43822
  },
  {
  "productName": "★ StatTrak™ M9 Bayonet | Safari Mesh (Field-Tested)",
  "id": 43826
  },
  {
  "productName": "★ StatTrak™ M9 Bayonet | Safari Mesh (Minimal Wear)",
  "id": 43827
  },
  {
  "productName": "★ StatTrak™ M9 Bayonet | Scorched (Field-Tested)",
  "id": 43829
  },
  {
  "productName": "★ StatTrak™ M9 Bayonet | Scorched (Minimal Wear)",
  "id": 43830
  },
  {
  "productName": "★ StatTrak™ M9 Bayonet | Slaughter (Factory New)",
  "id": 43832
  },
  {
  "productName": "★ StatTrak™ M9 Bayonet | Slaughter (Field-Tested)",
  "id": 43833
  },
  {
  "productName": "★ StatTrak™ M9 Bayonet | Slaughter (Minimal Wear)",
  "id": 43834
  },
  {
  "productName": "★ StatTrak™ M9 Bayonet | Stained (Factory New)",
  "id": 43835
  },
  {
  "productName": "★ StatTrak™ M9 Bayonet | Stained (Field-Tested)",
  "id": 43836
  },
  {
  "productName": "★ StatTrak™ M9 Bayonet | Stained (Minimal Wear)",
  "id": 43837
  },
  {
  "productName": "★ StatTrak™ M9 Bayonet | Tiger Tooth (Factory New)",
  "id": 43839
  },
  {
  "productName": "★ StatTrak™ M9 Bayonet | Ultraviolet (Field-Tested)",
  "id": 43841
  },
  {
  "productName": "★ StatTrak™ M9 Bayonet | Ultraviolet (Minimal Wear)",
  "id": 43842
  },
  {
  "productName": "★ StatTrak™ M9 Bayonet | Urban Masked (Field-Tested)",
  "id": 43844
  },
  {
  "productName": "★ StatTrak™ M9 Bayonet | Urban Masked (Minimal Wear)",
  "id": 43845
  },
  {
  "productName": "★ StatTrak™ Shadow Daggers | Blue Steel (Factory New)",
  "id": 43849
  },
  {
  "productName": "★ StatTrak™ Shadow Daggers | Blue Steel (Field-Tested)",
  "id": 43850
  },
  {
  "productName": "★ StatTrak™ Shadow Daggers | Blue Steel (Minimal Wear)",
  "id": 43851
  },
  {
  "productName": "★ StatTrak™ Shadow Daggers | Boreal Forest (Field-Tested)",
  "id": 43854
  },
  {
  "productName": "★ StatTrak™ Shadow Daggers | Boreal Forest (Minimal Wear)",
  "id": 43855
  },
  {
  "productName": "★ StatTrak™ Shadow Daggers | Case Hardened (Field-Tested)",
  "id": 43858
  },
  {
  "productName": "★ StatTrak™ Shadow Daggers | Case Hardened (Minimal Wear)",
  "id": 43859
  },
  {
  "productName": "★ StatTrak™ Shadow Daggers | Crimson Web (Field-Tested)",
  "id": 43862
  },
  {
  "productName": "Tec-9 | Army Mesh (Factory New)",
  "id": 41961
  },
  {
  "productName": "Tec-9 | Army Mesh (Field-Tested)",
  "id": 41962
  },
  {
  "productName": "Tec-9 | Army Mesh (Minimal Wear)",
  "id": 41963
  },
  {
  "productName": "Tec-9 | Avalanche (Factory New)",
  "id": 41966
  },
  {
  "productName": "Tec-9 | Avalanche (Field-Tested)",
  "id": 41967
  },
  {
  "productName": "Tec-9 | Avalanche (Minimal Wear)",
  "id": 41968
  },
  {
  "productName": "Tec-9 | Bamboo Forest (Factory New)",
  "id": 41971
  },
  {
  "productName": "Tec-9 | Bamboo Forest (Field-Tested)",
  "id": 41972
  },
  {
  "productName": "Tec-9 | Bamboo Forest (Minimal Wear)",
  "id": 41973
  },
  {
  "productName": "Tec-9 | Blue Titanium (Factory New)",
  "id": 41975
  },
  {
  "productName": "Tec-9 | Brass (Factory New)",
  "id": 41977
  },
  {
  "productName": "Tec-9 | Brass (Field-Tested)",
  "id": 41978
  },
  {
  "productName": "Tec-9 | Brass (Minimal Wear)",
  "id": 41979
  },
  {
  "productName": "Tec-9 | Cracked Opal (Factory New)",
  "id": 41982
  },
  {
  "productName": "Tec-9 | Cracked Opal (Field-Tested)",
  "id": 41983
  },
  {
  "productName": "Tec-9 | Cracked Opal (Minimal Wear)",
  "id": 41984
  },
  {
  "productName": "Tec-9 | Cut Out (Factory New)",
  "id": 41987
  },
  {
  "productName": "Tec-9 | Cut Out (Field-Tested)",
  "id": 41988
  },
  {
  "productName": "Tec-9 | Cut Out (Minimal Wear)",
  "id": 41989
  },
  {
  "productName": "Tec-9 | Fuel Injector (Factory New)",
  "id": 41992
  },
  {
  "productName": "Tec-9 | Fuel Injector (Field-Tested)",
  "id": 41993
  },
  {
  "productName": "Tec-9 | Fuel Injector (Minimal Wear)",
  "id": 41994
  },
  {
  "productName": "Tec-9 | Groundwater (Factory New)",
  "id": 41997
  },
  {
  "productName": "Tec-9 | Groundwater (Field-Tested)",
  "id": 41998
  },
  {
  "productName": "Tec-9 | Groundwater (Minimal Wear)",
  "id": 41999
  },
  {
  "productName": "Tec-9 | Hades (Factory New)",
  "id": 42002
  },
  {
  "productName": "Tec-9 | Hades (Field-Tested)",
  "id": 42003
  },
  {
  "productName": "Tec-9 | Hades (Minimal Wear)",
  "id": 42004
  },
  {
  "productName": "Tec-9 | Ice Cap (Field-Tested)",
  "id": 42007
  },
  {
  "productName": "Tec-9 | Ice Cap (Minimal Wear)",
  "id": 42008
  },
  {
  "productName": "Tec-9 | Isaac (Factory New)",
  "id": 42011
  },
  {
  "productName": "Tec-9 | Isaac (Minimal Wear)",
  "id": 42012
  },
  {
  "productName": "Tec-9 | Jambiya (Factory New)",
  "id": 42015
  },
  {
  "productName": "Tec-9 | Jambiya (Field-Tested)",
  "id": 42016
  },
  {
  "productName": "Tec-9 | Jambiya (Minimal Wear)",
  "id": 42017
  },
  {
  "productName": "Tec-9 | Nuclear Threat (Factory New)",
  "id": 42020
  },
  {
  "productName": "Tec-9 | Nuclear Threat (Field-Tested)",
  "id": 42021
  },
  {
  "productName": "Tec-9 | Nuclear Threat (Minimal Wear)",
  "id": 42022
  },
  {
  "productName": "Tec-9 | Ossified (Factory New)",
  "id": 42024
  },
  {
  "productName": "Tec-9 | Ossified (Minimal Wear)",
  "id": 42025
  },
  {
  "productName": "Tec-9 | Re-Entry (Factory New)",
  "id": 42026
  },
  {
  "productName": "Tec-9 | Re-Entry (Field-Tested)",
  "id": 42027
  },
  {
  "productName": "Tec-9 | Re-Entry (Minimal Wear)",
  "id": 42028
  },
  {
  "productName": "Tec-9 | Red Quartz (Factory New)",
  "id": 42030
  },
  {
  "productName": "Tec-9 | Red Quartz (Field-Tested)",
  "id": 42031
  },
  {
  "productName": "Tec-9 | Red Quartz (Minimal Wear)",
  "id": 42032
  },
  {
  "productName": "Tec-9 | Sandstorm (Field-Tested)",
  "id": 42034
  },
  {
  "productName": "Tec-9 | Sandstorm (Minimal Wear)",
  "id": 42035
  },
  {
  "productName": "Tec-9 | Terrace (Factory New)",
  "id": 42037
  },
  {
  "productName": "Tec-9 | Terrace (Field-Tested)",
  "id": 42038
  },
  {
  "productName": "Tec-9 | Terrace (Minimal Wear)",
  "id": 42039
  },
  {
  "productName": "Tec-9 | Titanium Bit (Factory New)",
  "id": 42041
  },
  {
  "productName": "Tec-9 | Titanium Bit (Field-Tested)",
  "id": 42042
  },
  {
  "productName": "Tec-9 | Titanium Bit (Minimal Wear)",
  "id": 42043
  },
  {
  "productName": "Tec-9 | Tornado (Factory New)",
  "id": 42045
  },
  {
  "productName": "Tec-9 | Tornado (Field-Tested)",
  "id": 42046
  },
  {
  "productName": "Tec-9 | Tornado (Minimal Wear)",
  "id": 42047
  },
  {
  "productName": "Tec-9 | Toxic (Factory New)",
  "id": 42050
  },
  {
  "productName": "Tec-9 | Toxic (Field-Tested)",
  "id": 42051
  },
  {
  "productName": "Tec-9 | Toxic (Minimal Wear)",
  "id": 42052
  },
  {
  "productName": "Tec-9 | Urban DDPAT (Factory New)",
  "id": 42055
  },
  {
  "productName": "Tec-9 | Urban DDPAT (Field-Tested)",
  "id": 42056
  },
  {
  "productName": "Tec-9 | Urban DDPAT (Minimal Wear)",
  "id": 42057
  },
  {
  "productName": "Tec-9 | VariCamo (Factory New)",
  "id": 42060
  },
  {
  "productName": "Tec-9 | VariCamo (Field-Tested)",
  "id": 42061
  },
  {
  "productName": "Tec-9 | VariCamo (Minimal Wear)",
  "id": 42062
  },
  {
  "productName": "UMP-45 | Blaze (Factory New)",
  "id": 42065
  },
  {
  "productName": "UMP-45 | Blaze (Minimal Wear)",
  "id": 42066
  },
  {
  "productName": "UMP-45 | Bone Pile (Factory New)",
  "id": 42067
  },
  {
  "productName": "UMP-45 | Bone Pile (Field-Tested)",
  "id": 42068
  },
  {
  "productName": "UMP-45 | Bone Pile (Minimal Wear)",
  "id": 42069
  },
  {
  "productName": "UMP-45 | Briefing (Factory New)",
  "id": 42071
  },
  {
  "productName": "UMP-45 | Briefing (Field-Tested)",
  "id": 42072
  },
  {
  "productName": "UMP-45 | Briefing (Minimal Wear)",
  "id": 42073
  },
  {
  "productName": "UMP-45 | Caramel (Factory New)",
  "id": 42076
  },
  {
  "productName": "UMP-45 | Caramel (Field-Tested)",
  "id": 42077
  },
  {
  "productName": "UMP-45 | Caramel (Minimal Wear)",
  "id": 42078
  },
  {
  "productName": "UMP-45 | Carbon Fiber (Factory New)",
  "id": 42080
  },
  {
  "productName": "UMP-45 | Carbon Fiber (Minimal Wear)",
  "id": 42081
  },
  {
  "productName": "UMP-45 | Corporal (Factory New)",
  "id": 42083
  },
  {
  "productName": "UMP-45 | Corporal (Field-Tested)",
  "id": 42084
  },
  {
  "productName": "UMP-45 | Corporal (Minimal Wear)",
  "id": 42085
  },
  {
  "productName": "UMP-45 | Delusion (Factory New)",
  "id": 42087
  },
  {
  "productName": "UMP-45 | Delusion (Field-Tested)",
  "id": 42088
  },
  {
  "productName": "UMP-45 | Delusion (Minimal Wear)",
  "id": 42089
  },
  {
  "productName": "UMP-45 | Exposure (Factory New)",
  "id": 42091
  },
  {
  "productName": "UMP-45 | Exposure (Field-Tested)",
  "id": 42092
  },
  {
  "productName": "UMP-45 | Exposure (Minimal Wear)",
  "id": 42093
  },
  {
  "productName": "UMP-45 | Fallout Warning (Factory New)",
  "id": 42096
  },
  {
  "productName": "UMP-45 | Fallout Warning (Field-Tested)",
  "id": 42097
  },
  {
  "productName": "UMP-45 | Fallout Warning (Minimal Wear)",
  "id": 42098
  },
  {
  "productName": "UMP-45 | Grand Prix (Field-Tested)",
  "id": 42100
  },
  {
  "productName": "UMP-45 | Gunsmoke (Factory New)",
  "id": 42102
  },
  {
  "productName": "UMP-45 | Gunsmoke (Field-Tested)",
  "id": 42103
  },
  {
  "productName": "UMP-45 | Gunsmoke (Minimal Wear)",
  "id": 42104
  },
  {
  "productName": "UMP-45 | Indigo (Factory New)",
  "id": 42107
  },
  {
  "productName": "UMP-45 | Indigo (Field-Tested)",
  "id": 42108
  },
  {
  "productName": "UMP-45 | Indigo (Minimal Wear)",
  "id": 42109
  },
  {
  "productName": "UMP-45 | Labyrinth (Factory New)",
  "id": 42111
  },
  {
  "productName": "UMP-45 | Labyrinth (Field-Tested)",
  "id": 42112
  },
  {
  "productName": "UMP-45 | Labyrinth (Minimal Wear)",
  "id": 42113
  },
  {
  "productName": "UMP-45 | Metal Flowers (Factory New)",
  "id": 42116
  },
  {
  "productName": "UMP-45 | Metal Flowers (Field-Tested)",
  "id": 42117
  },
  {
  "productName": "UMP-45 | Metal Flowers (Minimal Wear)",
  "id": 42118
  },
  {
  "productName": "UMP-45 | Minotaur's Labyrinth (Factory New)",
  "id": 42120
  },
  {
  "productName": "UMP-45 | Minotaur's Labyrinth (Field-Tested)",
  "id": 42121
  },
  {
  "productName": "UMP-45 | Minotaur's Labyrinth (Minimal Wear)",
  "id": 42122
  },
  {
  "productName": "UMP-45 | Primal Saber (Factory New)",
  "id": 42125
  },
  {
  "productName": "UMP-45 | Primal Saber (Field-Tested)",
  "id": 42126
  },
  {
  "productName": "UMP-45 | Primal Saber (Minimal Wear)",
  "id": 42127
  },
  {
  "productName": "UMP-45 | Riot (Factory New)",
  "id": 42130
  },
  {
  "productName": "UMP-45 | Riot (Field-Tested)",
  "id": 42131
  },
  {
  "productName": "UMP-45 | Riot (Minimal Wear)",
  "id": 42132
  },
  {
  "productName": "UMP-45 | Scaffold (Factory New)",
  "id": 42135
  },
  {
  "productName": "UMP-45 | Scaffold (Field-Tested)",
  "id": 42136
  },
  {
  "productName": "UMP-45 | Scaffold (Minimal Wear)",
  "id": 42137
  },
  {
  "productName": "UMP-45 | Scorched (Factory New)",
  "id": 42140
  },
  {
  "productName": "UMP-45 | Scorched (Field-Tested)",
  "id": 42141
  },
  {
  "productName": "UMP-45 | Scorched (Minimal Wear)",
  "id": 42142
  },
  {
  "productName": "UMP-45 | Urban DDPAT (Factory New)",
  "id": 42145
  },
  {
  "productName": "UMP-45 | Urban DDPAT (Field-Tested)",
  "id": 42146
  },
  {
  "productName": "UMP-45 | Urban DDPAT (Minimal Wear)",
  "id": 42147
  },
  {
  "productName": "USP-S | Blood Tiger (Factory New)",
  "id": 42149
  },
  {
  "productName": "USP-S | Blood Tiger (Field-Tested)",
  "id": 42150
  },
  {
  "productName": "USP-S | Blood Tiger (Minimal Wear)",
  "id": 42151
  },
  {
  "productName": "USP-S | Blueprint (Factory New)",
  "id": 42153
  },
  {
  "productName": "USP-S | Blueprint (Minimal Wear)",
  "id": 42154
  },
  {
  "productName": "USP-S | Business Class (Factory New)",
  "id": 42157
  },
  {
  "productName": "USP-S | Business Class (Field-Tested)",
  "id": 42158
  },
  {
  "productName": "USP-S | Business Class (Minimal Wear)",
  "id": 42159
  },
  {
  "productName": "USP-S | Caiman (Factory New)",
  "id": 42161
  },
  {
  "productName": "USP-S | Caiman (Field-Tested)",
  "id": 42162
  },
  {
  "productName": "USP-S | Caiman (Minimal Wear)",
  "id": 42163
  },
  {
  "productName": "USP-S | Cyrex (Factory New)",
  "id": 42166
  },
  {
  "productName": "USP-S | Cyrex (Field-Tested)",
  "id": 42167
  },
  {
  "productName": "USP-S | Cyrex (Minimal Wear)",
  "id": 42168
  },
  {
  "productName": "USP-S | Dark Water (Field-Tested)",
  "id": 42170
  },
  {
  "productName": "USP-S | Dark Water (Minimal Wear)",
  "id": 42171
  },
  {
  "productName": "USP-S | Forest Leaves (Factory New)",
  "id": 42173
  },
  {
  "productName": "USP-S | Forest Leaves (Field-Tested)",
  "id": 42174
  },
  {
  "productName": "USP-S | Forest Leaves (Minimal Wear)",
  "id": 42175
  },
  {
  "productName": "USP-S | Guardian (Factory New)",
  "id": 42177
  },
  {
  "productName": "USP-S | Guardian (Field-Tested)",
  "id": 42178
  },
  {
  "productName": "USP-S | Guardian (Minimal Wear)",
  "id": 42179
  },
  {
  "productName": "USP-S | Kill Confirmed (Factory New)",
  "id": 42181
  },
  {
  "productName": "USP-S | Kill Confirmed (Field-Tested)",
  "id": 42182
  },
  {
  "productName": "USP-S | Kill Confirmed (Minimal Wear)",
  "id": 42183
  },
  {
  "productName": "USP-S | Lead Conduit (Factory New)",
  "id": 42186
  },
  {
  "productName": "USP-S | Lead Conduit (Field-Tested)",
  "id": 42187
  },
  {
  "productName": "USP-S | Lead Conduit (Minimal Wear)",
  "id": 42188
  },
  {
  "productName": "USP-S | Neo-Noir (Factory New)",
  "id": 42191
  },
  {
  "productName": "USP-S | Neo-Noir (Field-Tested)",
  "id": 42192
  },
  {
  "productName": "USP-S | Neo-Noir (Minimal Wear)",
  "id": 42193
  },
  {
  "productName": "USP-S | Night Ops (Factory New)",
  "id": 42196
  },
  {
  "productName": "USP-S | Night Ops (Field-Tested)",
  "id": 42197
  },
  {
  "productName": "USP-S | Night Ops (Minimal Wear)",
  "id": 42198
  },
  {
  "productName": "USP-S | Orion (Factory New)",
  "id": 42201
  },
  {
  "productName": "USP-S | Orion (Field-Tested)",
  "id": 42202
  },
  {
  "productName": "USP-S | Orion (Minimal Wear)",
  "id": 42203
  },
  {
  "productName": "USP-S | Overgrowth (Factory New)",
  "id": 42206
  },
  {
  "productName": "USP-S | Overgrowth (Field-Tested)",
  "id": 42207
  },
  {
  "productName": "USP-S | Overgrowth (Minimal Wear)",
  "id": 42208
  },
  {
  "productName": "USP-S | Para Green (Factory New)",
  "id": 42211
  },
  {
  "productName": "USP-S | Para Green (Field-Tested)",
  "id": 42212
  },
  {
  "productName": "USP-S | Para Green (Minimal Wear)",
  "id": 42213
  },
  {
  "productName": "USP-S | Road Rash (Factory New)",
  "id": 42216
  },
  {
  "productName": "USP-S | Road Rash (Field-Tested)",
  "id": 42217
  },
  {
  "productName": "USP-S | Road Rash (Minimal Wear)",
  "id": 42218
  },
  {
  "productName": "USP-S | Royal Blue (Factory New)",
  "id": 42221
  },
  {
  "productName": "USP-S | Royal Blue (Field-Tested)",
  "id": 42222
  },
  {
  "productName": "USP-S | Royal Blue (Minimal Wear)",
  "id": 42223
  },
  {
  "productName": "USP-S | Serum (Factory New)",
  "id": 42225
  },
  {
  "productName": "USP-S | Serum (Field-Tested)",
  "id": 42226
  },
  {
  "productName": "USP-S | Serum (Minimal Wear)",
  "id": 42227
  },
  {
  "productName": "USP-S | Stainless (Factory New)",
  "id": 42229
  },
  {
  "productName": "USP-S | Stainless (Field-Tested)",
  "id": 42230
  },
  {
  "productName": "USP-S | Stainless (Minimal Wear)",
  "id": 42231
  },
  {
  "productName": "USP-S | Torque (Field-Tested)",
  "id": 42234
  },
  {
  "productName": "USP-S | Torque (Minimal Wear)",
  "id": 42235
  },
  {
  "productName": "XM1014 | Black Tie (Factory New)",
  "id": 42242
  },
  {
  "productName": "XM1014 | Black Tie (Field-Tested)",
  "id": 42243
  },
  {
  "productName": "XM1014 | Black Tie (Minimal Wear)",
  "id": 42244
  },
  {
  "productName": "XM1014 | Blaze Orange (Factory New)",
  "id": 42247
  },
  {
  "productName": "XM1014 | Blaze Orange (Field-Tested)",
  "id": 42248
  },
  {
  "productName": "XM1014 | Blaze Orange (Minimal Wear)",
  "id": 42249
  },
  {
  "productName": "XM1014 | Blue Spruce (Factory New)",
  "id": 42252
  },
  {
  "productName": "XM1014 | Blue Spruce (Field-Tested)",
  "id": 42253
  },
  {
  "productName": "XM1014 | Blue Spruce (Minimal Wear)",
  "id": 42254
  },
  {
  "productName": "XM1014 | Blue Steel (Factory New)",
  "id": 42257
  },
  {
  "productName": "XM1014 | Blue Steel (Field-Tested)",
  "id": 42258
  },
  {
  "productName": "XM1014 | Blue Steel (Minimal Wear)",
  "id": 42259
  },
  {
  "productName": "XM1014 | Bone Machine (Factory New)",
  "id": 42262
  },
  {
  "productName": "XM1014 | Bone Machine (Field-Tested)",
  "id": 42263
  },
  {
  "productName": "XM1014 | Bone Machine (Minimal Wear)",
  "id": 42264
  },
  {
  "productName": "XM1014 | CaliCamo (Factory New)",
  "id": 42267
  },
  {
  "productName": "XM1014 | CaliCamo (Field-Tested)",
  "id": 42268
  },
  {
  "productName": "XM1014 | CaliCamo (Minimal Wear)",
  "id": 42269
  },
  {
  "productName": "XM1014 | Fallout Warning (Factory New)",
  "id": 42272
  },
  {
  "productName": "XM1014 | Fallout Warning (Field-Tested)",
  "id": 42273
  },
  {
  "productName": "XM1014 | Fallout Warning (Minimal Wear)",
  "id": 42274
  },
  {
  "productName": "XM1014 | Grassland (Factory New)",
  "id": 42277
  },
  {
  "productName": "XM1014 | Grassland (Field-Tested)",
  "id": 42278
  },
  {
  "productName": "XM1014 | Grassland (Minimal Wear)",
  "id": 42279
  },
  {
  "productName": "XM1014 | Heaven Guard (Factory New)",
  "id": 42282
  },
  {
  "productName": "XM1014 | Heaven Guard (Field-Tested)",
  "id": 42283
  },
  {
  "productName": "XM1014 | Heaven Guard (Minimal Wear)",
  "id": 42284
  },
  {
  "productName": "XM1014 | Jungle (Factory New)",
  "id": 42287
  },
  {
  "productName": "XM1014 | Jungle (Field-Tested)",
  "id": 42288
  },
  {
  "productName": "XM1014 | Jungle (Minimal Wear)",
  "id": 42289
  },
  {
  "productName": "XM1014 | Quicksilver (Factory New)",
  "id": 42292
  },
  {
  "productName": "XM1014 | Quicksilver (Field-Tested)",
  "id": 42293
  },
  {
  "productName": "XM1014 | Quicksilver (Minimal Wear)",
  "id": 42294
  },
  {
  "productName": "XM1014 | Red Leather (Factory New)",
  "id": 42297
  },
  {
  "productName": "XM1014 | Red Leather (Field-Tested)",
  "id": 42298
  },
  {
  "productName": "XM1014 | Red Leather (Minimal Wear)",
  "id": 42299
  },
  {
  "productName": "XM1014 | Red Python (Field-Tested)",
  "id": 42302
  },
  {
  "productName": "XM1014 | Red Python (Minimal Wear)",
  "id": 42303
  },
  {
  "productName": "XM1014 | Scumbria (Factory New)",
  "id": 42306
  },
  {
  "productName": "XM1014 | Scumbria (Field-Tested)",
  "id": 42307
  },
  {
  "productName": "XM1014 | Scumbria (Minimal Wear)",
  "id": 42308
  },
  {
  "productName": "XM1014 | Seasons (Factory New)",
  "id": 42311
  },
  {
  "productName": "XM1014 | Seasons (Field-Tested)",
  "id": 42312
  },
  {
  "productName": "XM1014 | Seasons (Minimal Wear)",
  "id": 42313
  },
  {
  "productName": "XM1014 | Slipstream (Factory New)",
  "id": 42316
  },
  {
  "productName": "XM1014 | Slipstream (Field-Tested)",
  "id": 42317
  },
  {
  "productName": "XM1014 | Slipstream (Minimal Wear)",
  "id": 42318
  },
  {
  "productName": "XM1014 | Teclu Burner (Factory New)",
  "id": 42321
  },
  {
  "productName": "XM1014 | Teclu Burner (Field-Tested)",
  "id": 42322
  },
  {
  "productName": "XM1014 | Teclu Burner (Minimal Wear)",
  "id": 42323
  },
  {
  "productName": "XM1014 | Tranquility (Factory New)",
  "id": 42326
  },
  {
  "productName": "XM1014 | Tranquility (Field-Tested)",
  "id": 42327
  },
  {
  "productName": "XM1014 | Tranquility (Minimal Wear)",
  "id": 42328
  },
  {
  "productName": "XM1014 | Urban Perforated (Factory New)",
  "id": 42331
  },
  {
  "productName": "XM1014 | Urban Perforated (Field-Tested)",
  "id": 42332
  },
  {
  "productName": "XM1014 | Urban Perforated (Minimal Wear)",
  "id": 42333
  },
  {
  "productName": "XM1014 | VariCamo Blue (Factory New)",
  "id": 42336
  },
  {
  "productName": "XM1014 | VariCamo Blue (Field-Tested)",
  "id": 42337
  },
  {
  "productName": "XM1014 | VariCamo Blue (Minimal Wear)",
  "id": 42338
  },
  {
  "productName": "XM1014 | Ziggy (Factory New)",
  "id": 42341
  },
  {
  "productName": "XM1014 | Ziggy (Field-Tested)",
  "id": 42342
  },
  {
  "productName": "XM1014 | Ziggy (Minimal Wear)",
  "id": 42343
  }
]